﻿var $content;
var dataTableElement;
var validatorFormModify;
var arrayDataList	= [];
var objDataModify	= {};
var objDataBack		= {};
var targetCode		= tockenCode();
var listFunction = {
  click: function(data) {return false;},
  dbClick: function(data) {return false;}
}
$(document).ready(function() {
	$content = $('#contentModule');
	/*----------------------------------------------------------------------*/
	/* Modal Element
	/*----------------------------------------------------------------------*/
	$content.append("	<div id=\"myModal" + targetCode + "\" class=\"modal fade\">" +
					"	<div class=\"modal-dialog\">" +
					"	<div class=\"modal-content\">" +
					"	<div class=\"modal-header\"><button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button><h4 class=\"modal-title\" id=\"myModalLabel\"></h4></div>" +
					"	<div class=\"modal-body\"></div>" +
					"	<div class=\"modal-footer\"><button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cerrar</button></div>" +
					"	</div>" +
					"	</div>" +
					"	</div>");
	/*----------------------------------------------------------------------*/
	/* datatable plugin Listas
	/*----------------------------------------------------------------------*/
	dataTableElement = $content.find("table.dataTableList").dataTable({
		"aaData": arrayDataList,
		"bRetrieve": true,
		"sPaginationType": "full_numbers",
		"aaSortingFixed": [[ 0, 'desc' ]],
		"bFilter": false,
		"bInfo": false,
		"scrollX": false,
		"bSortCellsTop": true,
		"bProcessing": true,
        "aoColumns": getColumnAttr(this),
		"fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			$_self = $(this);
			$("td",nRow).each(function(idx, tdObj) {
				$(tdObj).html("");
				for(aDataItem in aData) {
					if($(tdObj).hasClass(aDataItem)) {
						$(tdObj).html(aData[aDataItem]);
					}
				}
			});
			$btnDiv = $("<div class=\"btn-group\"></div>");
			if($(this).data("update")) {
				$("<button type=\"button\" id=\"trash_" + iDisplayIndex + "_\" class=\"btn btn-info\"></button>").tooltip({title: "Editar", placement : "left"}).on("click",function() {
                    aData.accion = "upd";
                    listFunction.dbClick(aData);
				}).append("<i class=\"glyphicon glyphicon-edit\"></i>").appendTo($btnDiv);
			}
			if($(this).data("delete")) {
				$("<button type=\"button\" id=\"trash_" + iDisplayIndex + "_\" class=\"btn btn-danger\"></button>").tooltip({title: "Borrar", placement : "right"}).on("click",function() {
					aData.accion = "del";
                    $__self = $(this);
					$__self.attr({"disabled":"disabled"}).find("i").removeClass("glyphicon-trash").addClass("throbber-loader").css({"font-size":"9px"});
					$.ajax({
						async: true,
						type: "POST",
						dataType: "json",
						url: "json.php",
						data: {
							module: $_self.data("module"),
							vista: $_self.data("del-view"),
							datos: JSON.stringify(aData),
						},
						success: function(respJson) {
							if(respJson.status != "ERROR") {
								$(nRow).remove();
							} else {
								$("#myModal" + targetCode + " div.modal-header > h4.modal-title").html(respJson.status);
								$("#myModal" + targetCode + " div.modal-body").html(respJson.text);
								$("#myModal" + targetCode + "").modal("show");
							}
							$__self.removeAttr("disabled").find("i").removeAttr("style").removeClass("throbber-loader").addClass("glyphicon-trash");
						},
						error: function(jqXHR, textStatus, errorThrown) {
							$("#myModal" + targetCode + " div.modal-header > h4.modal-title").html(textStatus);
							$("#myModal" + targetCode + " div.modal-body").html("<p>" + errorThrown + "</p>");
							$("#myModal" + targetCode + "").modal("show");
						}
					});
				}).append("<i class=\"glyphicon glyphicon-trash\"></i>").appendTo($btnDiv);
			}
			$("td.accion",nRow).html($btnDiv);
			return nRow;
		},
	});
	$(".dataTables_scrollBody").css({"overflow": "hidden"});
	$("#" + $(dataTableElement.get(0)).attr("id") + "_length").find("select").data({"width":"75px"}).selectpicker();
	function getColumnAttr(_self) {
		var column = [];
			$("thead > tr > th",_self).each(function(idx, thObj) {
				var tdData = $(thObj).data();
					item = {};
					for(thItem in tdData) {
						item[thItem] = tdData[thItem];
					}
				column.push(item);
			});
		return column;
	}
	/*---------------------------------------*/
	/* Jquery Form
	/*---------------------------------------*/
	//Formulario List
	function sendFormAjax(formData, $form, opt) {
		$.ajax({
			async: true,
			type: "POST",
			dataType: "json",
			url: "json.php",
			data: {
				module: $form.data("module"),
				vista: $form.data("vista"),
				datos: JSON.stringify($form.serializeObject()),
			},
			success: function(respJson, statusText, xhr, $form) {
				dataTableElement.fnClearTable();
				if(respJson.length > 0) {
					dataTableElement.fnAddData(respJson);
					dataTableElement.fnDraw();
					arrayDataList = respJson;
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				$("#myModal" + targetCode + " div.modal-header > h4.modal-title").html(textStatus);
				$("#myModal" + targetCode + " div.modal-body").html("<p>" + errorThrown + "</p>");
				$("#myModal" + targetCode + "").modal("show");
			}
		});
	}
	if(objDataBack != null && typeof objDataBack[0] != 'undefined') {
		objDataBack = objDataBack[0];
	}
	$content.find('form.formList').autofill(objDataBack);
	$content.find('form.formList').ajaxForm({
		beforeSubmit: function(formData, jqForm, options) {
			sendFormAjax(formData, jqForm, options);
			return false;
		}
	});
	$content.find("form.formList button[type='reset']").on("click", function(event) {
		event.preventDefault();
		$(this).closest('form').get(0).reset();
		$(this).closest('form').find('select').selectpicker('render');
		$(this).closest('form').submit();
	});
	$content.find("form.formList button.pdf").on("click", function(event) {
		event.preventDefault();
		var data = {
			ids : arrayDataList.map(function(item) {
				return item.id
			}).join(",")
		};
		var url = "pdf.php?rnd=" + targetCode + "&module=" + $(this).data("module") + "&vista=" + $(this).data("view") + "&title=" + $(this).data("title") + "&datos=" + JSON.stringify(data); 
		window.open(url, '_blank');
	});
	$content.find("form.formList button.excel").on("click", function(event) {
		event.preventDefault();
		var data = {
			ids : arrayDataList.map(function(item) {
				return item.id
			}).join(",")
		};
		var url = "xls.php?rnd=" + targetCode + "&module=" + $(this).data("module") + "&vista=" + $(this).data("view") + "&title=" + $(this).data("title") + "&datos=" + JSON.stringify(data); 
		window.open(url, '_blank');
	});
	//Formulario Modify
	if(objDataModify == null) {
		objDataModify = {};
	}
	$content.find('form.formModify').autofill(objDataModify);
    validatorFormModify = $content.find('form.formModify').validate({
		submitHandler: function(form) {
			$(form).submit(function() {
				$_self = $(this);
				$(this).ajaxSubmit({
					async: true,
					type: "POST",
					dataType: "json",
					url: "json.php",
					data: {
						module: $_self.data("module"),
						vista: $_self.data("vista"),
						datos: JSON.stringify($_self.serializeObject()),
					},
					beforeSubmit: function(formData, jqForm, options) {
						$_self.find("button[type=submit]").attr({"disabled":"disabled"}).find("i").removeClass("glyphicon-ok").addClass("throbber-loader").css({"font-size":"9px"});
						return true;
					},
					success: function(respJson, statusText, xhr, $form) {
						if(respJson.status != "ERROR") {
							$("#" + $_self.data("id")).val(respJson.id);
							$("#accion").val("upd");
                            resAlert = "\t<div class=\"alert alert-dismissible " + respJson.type + "\" role=\"alert\">\n";
                            resAlert+= "\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\n";
                            resAlert+= "\t\t\t<span aria-hidden=\"true\">&times;</span>\n";
                            resAlert+= "\t\t</button>\n";
                            resAlert+= "\t" + respJson.text + "</div>\n";
                            $_self.find("div.alertDiv").html(resAlert);
                            $_self.find("div.alertDiv").fadeIn();
                            if(typeof callBackFormSend != "undefined") {
                                callBackFormSend(respJson);
                            }
						} else {
							$("#myModal" + targetCode + " div.modal-header > h4.modal-title").html(respJson.status);
							$("#myModal" + targetCode + " div.modal-body").html(respJson.text);
							$("#myModal" + targetCode + "").modal("show");
						}
						$_self.find("button[type=submit]").removeAttr("disabled").find("i").removeAttr("style").removeClass("throbber-loader").addClass("glyphicon-ok");
					},
					error: function(jqXHR, textStatus, errorThrown) {
						$("#myModal" + targetCode + " div.modal-header > h4.modal-title").html(textStatus);
						$("#myModal" + targetCode + " div.modal-body").html(errorThrown);
						$("#myModal" + targetCode + "").modal("show");
					}
				});
				return false;
			});
		},
		// showErrors: function(errorMap, errorList) {
			// $.each(this.successList, function(index, value) {
				// $(value).text('').addClass('valid').closest('.form-group').removeClass('error').addClass('success');
				// return $(value).popover("hide");
			// });
			// return $.each(errorList, function(index, value) {
				// var _popover;
				// $(value.element).closest('.form-group').removeClass('success').addClass('error');
				// _popover = $(value.element).popover({
					// trigger: "focus",
					// placement: "right",
					// content: value.message,
					// template: "<div class=\"popover\" style=\"width:50%;font-size:10px;\"><div class=\"arrow\"></div><div class=\"popover-inner\"><div class=\"popover-content\"><p></p></div></div></div>"
				// });
				// _popover.data("bs.popover").options.content = value.message;
				// // return $(value.element).popover("show");
			// });
		// },
        highlight: function (element) {
			$(element).closest('.form-group').removeClass('success').addClass('error');
        },
		success: function(element) {
            element.text('').addClass('valid').closest('.form-group').removeClass('error').addClass('success');
		}
	});
	
	/*----------------------------------------------------------------------*/
	/* Checkbox y Radio Bootstrap Pluguin
	/*----------------------------------------------------------------------*/
	$content.find("input[type=checkbox], input[type=radio]").bootstrapSwitch();
	
	/*----------------------------------------------------------------------*/
	/* Select Bootstrap Pluguin
	/*----------------------------------------------------------------------*/
	$content.find("select").selectpicker();

	/*----------------------------------------------------------------------*/
	/* DateTime Picker Bootstrap Pluguin
	/*----------------------------------------------------------------------*/
	$content.find("input.datetimepicker").datetimepicker();
    $content.find("input.timepicker").datetimepicker({
        format: 'LT'
    });
    $content.find("input.datepicker").datetimepicker({
        format: 'DD/MM/YYYY'
    });
});