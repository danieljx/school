<?php
	error_reporting(E_ALL);
	session_start();
	ob_start();
	define( '_JEXEC', 1 );
	define('JPATH_BASE', dirname(__FILE__) );
	define( 'DS', DIRECTORY_SEPARATOR );
	require_once ( JPATH_BASE .DS.'libraries'.DS.'defines.php' );
	echo "<?xml version=\"1.0\" encoding=\"".JFactory::getValueConf('config.charset')."\"?>";
	$user = JFactory::getUser();
?>
<html>
	<head>
		<?php JFactory::getHead(); ?>
		<style>
			<?php JFactory::getStyle(); ?>
		</style>
		<script type="text/javascript"  language="JavaScript">
			<?php JFactory::getJavaScript(); ?>
		</script>
	</head>
	<body>
		<?php JFactory::getShowComp("nav-top"); ?>
		<div class="container-fluid">
			<div class="row">
					<?php JFactory::getShowComp("nav-left"); 
					if($user->getId() == -1) { ?>
					<div id="contentModule" class="col-md-8 col-md-offset-2">
					<?php } else { ?>
					<div id="contentModule" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
					<?php JFactory::getShowComp("profile"); ?>
					<?php JFactory::getShowComp("breadcrumbs"); ?>
					<?php } ?>
					<?php JFactory::getShowMod(); ?>
					</div>
					</div>
					<?php JFactory::getShowComp("footer"); ?>
			</div>
		</div>
	</body>
</html>  