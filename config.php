<?php
class JConfig {
	var $dbtype   = 'mysql';
	var $host     = 'localhost';
	var $db       = 'school';
    var $user     = 'root';
    var $password = '';
	var $dbprefix = '';
	var $sitename = 'OnSchool';
	var $MetaDesc = 'Sistema Gestion Incripciones Escolar';
	var $MetaKeys = 'sistema, gestion, inscripciones, control, escuela';
	var $lang 	  = 'spanish';
	var $charset  = 'UTF-8';
	var $title    = 'Sistema Gestion Incripciones Escolar';    
	var $desc     = 'Sistema Gestion Incripciones Escolar';
	var $key      = 'Sistema Gestion Incripciones Escolar';
}