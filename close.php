<?php
	session_start();
	ob_start();
	if(!isset($_SESSION['user_id'])) {
		$_SESSION['user_id'] = "";
	}
	$_SESSION['user_id'] = -1;
	header("Location: index.php");       