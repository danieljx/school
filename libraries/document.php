<?php
class JDocument extends JObject
{
	var $title = '';
	var $description = '';
	var $_mime = '';
	var $_scripts = array();
	var $_script = array();
	var $_styleSheets = array();
	var $_style = array();
	var $_metaTags = array();
	var $_generator = 'MANGO RP';
	function __construct( $options = array())
	{
	}
	function setTitle($title) {
		$this->title = $title;
	}
	function getTitle() {
		return $this->title;
	}
	function setDescription($description) {
		$this->description = $description;
	}
	function setGenerator($generator) {
		$this->_generator = $generator;
	}
	function getGenerator() {
		return $this->_generator;
	}
	function setMimeEncoding($type = 'text/html') {
		$this->_mime = strtolower($type);
	}			
	function getDescription() {
		return $this->description;
	}				
	function addScript($url, $type="text/javascript") {
		$this->_scripts[$url] = $type;
	}
	function addScriptDeclaration($content, $type = 'text/javascript')
	{
		if (!isset($this->_script[strtolower($type)])) {
			$this->_script[strtolower($type)] = $content;
		} else {
			$this->_script[strtolower($type)] .= chr(13).$content;
		}
	}
	function addStyleSheet($url, $type = 'text/css', $media = null, $attribs = array())
	{
		$this->_styleSheets[$url]['mime']		= $type;
		$this->_styleSheets[$url]['media']		= $media;
		$this->_styleSheets[$url]['attribs']	= $attribs;
	}
	function addStyleDeclaration($content, $type = 'text/css')
	{
		if (!isset($this->_style[strtolower($type)])) {
			$this->_style[strtolower($type)] = $content;
		} else {
			$this->_style[strtolower($type)] .= chr(13).$content;
		}
	}
	function setCharset($type = 'utf-8') {
		$this->_charset = $type;
	}
	function getCharset() {
		return $this->_charset;
	}			
	function setMetaData($name, $content, $http_equiv = false)
	{
		$name = strtolower($name);
		if($name == 'generator') { 
			$this->setGenerator($content);
		} elseif($name == 'description') {
			$this->setDescription($content);
		} else {
			if ($http_equiv == true) {
				$this->_metaTags['http-equiv'][$name] = $content;
			} else {
				$this->_metaTags['standard'][$name] = $content;
			}
		}
	}
	function getMetaData($name, $http_equiv = false)
	{
		$result = '';
		$name = strtolower($name);
		if($name == 'generator') { 
			$result = $this->getGenerator();
		} elseif($name == 'description') {
			$result = $this->getDescription();
		} else {
			if ($http_equiv == true) {
				$result = @$this->_metaTags['http-equiv'][$name];
			} else {
				$result = @$this->_metaTags['standard'][$name];
			}
		}
		return $result;
	}	
			
}
