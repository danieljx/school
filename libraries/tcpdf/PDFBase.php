<?php
class PDFBase extends TCPDF {
    const _orientation_portrait  	= "P";
    const _orientation_landscape 	= "L";
    const _unit_point 				= "pt";
    const _unit_millimeter 			= "mm";
    const _unit_centimeter 			= "cm";
    const _unit_inch 				= "in";
    public $creator    				= "TCPDF";
    public $autor      				= "Autor del Reporte";
    public $subject    				= "Asunto del Reporte";
    public $title      				= "Titulo del Reporte";
    public $desc       				= "Descripcion";
    public $logo        			= "front/images/logo-transparente.png";
    public $logo_width  			= 30;
    public $margin_left    			= 15;
    public $margin_right   			= 15;
    public $margin_top     			= 27;
    public $margin_bottom  			= 25;
    public $margin_header  			= 5;
    public $margin_footer  			= 10;
    public $image_scale_ratio 		= 1.25;
    public $header_font 			= 'times';
    public $header_size 			= 10;
    public $footer_font 			= 'times';
    public $footer_size 			= 10;
    public $font 					= 'times';
    public $size 					= 10;
    public function __construct($orientation=PDFBase::_orientation_portrait, $unit=PDFBase::_unit_millimeter, $format='Letter', $unicode=true, $encoding='UTF-8', $diskcache=false, $pdfa=false) {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa);
    }
    public function setInicio($headerfoot = true) {
        $this->SetCreator($this->creator);
        $this->SetAuthor($this->autor);
        $this->SetTitle($this->title);
        $this->SetSubject($this->subject);
        if($headerfoot) {
            $this->setFooterData($tc=array(64,64,64), $lc=array(0,64,128));        
        }
        $this->SetMargins($this->margin_left, $this->margin_top, $this->margin_right);
        $this->SetHeaderMargin($this->margin_header);
        $this->SetFooterMargin($this->margin_footer);
        $this->SetAutoPageBreak(TRUE, $this->margin_bottom);       
        $this->setImageScale($this->image_scale_ratio);
        $this->AddPage();
    }
}
$tagdir 	= dirname(__FILE__) . DS . "template";
$taglist	= scandir($tagdir);
foreach($taglist as $tagitem) {
    if(!is_dir($tagdir.DS.$tagitem)) {
		require_once($tagdir.DS.$tagitem);
	} 
}