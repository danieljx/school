<?php
class PDFList extends PDFBase {
	
    private $footer_text 	= "";
	
    public function __construct($orientation=PDFBase::_orientation_landscape, $unit=PDFBase::_unit_millimeter, $format='Letter', $unicode=true, $encoding='UTF-8', $diskcache=false, $pdfa=false) {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa);
    }
    public function setInicio() {
        parent::setInicio();
    }
	public function setLogo($logo) {
		$this->logo = $logo;
	}
	private function getLogo() {
		return $this->logo;
	}
	public function setTitle($text) {
		$this->title = $text;
	}
	private function getTitle() {
		return $this->title;
	}
	public function setFooterText($text) {
		$this->footer_text = $text;
	}
	private function getFooterText() {
		return $this->footer_text;
	}
	public function Header() {
		$this->Image("front/images/logo.png", 15, 6, 25, '', 'PNG', '', 'T', false, 600, '', false, false, 0, false, false, false);
		$this->SetY(12);
		$this->SetFont('helvetica', 'B', 15);
		$this->Cell(0, 15, html_entity_decode($this->getTitle()), 0, false, 'C', 0, '', 0, false, 'M', 'M');		
	}
    public function Footer() {
		$this->Line(20, 300, 196, 300, array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(200, 200, 200)));
		$this->SetY(-17.4);
		$this->Cell(0, 8, html_entity_decode($this->getFooterText()), 0, 0, 'C', 0, '', 0);
		$this->SetY(-15);
		$this->SetFont('helvetica', 'I', 8);
		$this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
	}
}