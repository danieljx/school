<?php
class JModule {
	private $xml		= null;    
	private $path		= null;
	private $mod_id		= null;
	private $mod_name	= null;
	private $mod_title	= null;
	private $error		= null;
	private $view		= false;
	private $insert		= false;
	private $update		= false;
	private $delete		= false;
	private $prints		= false;
	private $export		= false;
	private $status		= false;

	function __construct() {
		$db   = JFactory::getDBO();
		$user = JFactory::getUser();
		$mod  = JRequest::getVar("module");
		$rol  = $user->getRolId();
		$sql  = "";
		if(empty($mod) && $user->getId() == -1) {
			$sql = "Select m.mod_name\n";
			$sql.= "	  ,m.mod_title\n";
			$sql.= "	  ,m.mod_id\n";
			$sql.= "	  ,'V' as accion\n";
			$sql.= "From sys_module m\n";
			$sql.= "Where m.mod_sta = 'S'\n";
			$sql.= "  and m.mod_lock = 'N'\n";
			$sql.= "  and m.mod_type = 'P'\n";
		} else if(empty($mod) && $user->getId() == -1) {
			$sql = "Select m.mod_name\n";
			$sql.= "	  ,m.mod_title\n";
			$sql.= "	  ,m.mod_id\n";
			$sql.= "	  ,'V' as accion\n";
			$sql.= "From sys_module m\n";
			$sql.= "Where m.mod_sta = 'S'\n";
			$sql.= "  and m.mod_lock = 'N'\n";
			$sql.= "  and m.mod_name = " . $db->Quote($mod) . "\n";
		} else if(empty($mod)) {
			$sql = "Select m.mod_name\n";
			$sql.= "	  ,m.mod_title\n";
			$sql.= "	  ,m.mod_id\n";
			$sql.= "	  ,'V' as accion\n";
			$sql.= "From sys_module m\n";
			$sql.= "Where m.mod_sta = 'S'\n";
			$sql.= "  and m.mod_lock = 'S'\n";
			$sql.= "  and m.mod_type = 'P'\n";
		} else if($user->getId() == -1) {
			$sql = "Select m.mod_name\n";
			$sql.= "	  ,m.mod_title\n";
			$sql.= "	  ,m.mod_id\n";
			$sql.= "	  ,'V' as accion\n";
			$sql.= "From sys_module m\n";
			$sql.= "Where m.mod_sta = 'S'\n";
			$sql.= "  and m.mod_lock = 'N'\n";
			$sql.= "  and m.mod_name = " . $db->Quote($mod) . "\n";
		} else {
			$sql = "Select m.mod_name\n";
			$sql.= "	  ,m.mod_title\n";
			$sql.= "	  ,m.mod_id\n";
			$sql.= "	  ,r.accion\n";
			$sql.= "From sys_module m\n";
			$sql.= "Left Join sys_rule_accion r on r.accion_item = m.mod_id and r.accion_type = 'MOD' and r.rule_id = " . $rol . "\n";
			$sql.= "Where m.mod_sta = 'S'\n";
			$sql.= "  and m.mod_name = " . $db->Quote($mod) . "\n";
		}
		$db->setQuery( $sql );
		$rows = $db->loadObjectList();
		if(count($rows) > 0 &&  strpos($rows[0]->accion,"V") > -1 && (empty($mod) || file_exists(JPATH_MODULES . DS . $mod . DS . "views.xml"))) {
			$this->mod_name 	= $rows[0]->mod_name;
			$this->mod_title 	= $rows[0]->mod_title;
			$this->mod_id 		= $rows[0]->mod_id;
			$this->path     	= JPATH_MODULES . DS . $this->mod_name . DS;
			$this->xml      	= simplexml_load_file($this->path . "views.xml");
			if(file_exists( $this->path. "dataClass.php")) {
				require_once($this->path. "dataClass.php");
			}
			if(file_exists( $this->path. "lang" .DS . JFactory::getValueConf('config.lang') . ".php")) {
				require_once($this->path. "lang" .DS . JFactory::getValueConf('config.lang') . ".php");
			}
			if(file_exists( $this->path. "lang" .DS . "default" . ".php")) {
				require_once($this->path. "lang" .DS . "default" . ".php");
			}
			$meta = JFactory::getMetasObj();
			$this->view		= (strpos($rows[0]->accion,"V") > -1);
			$this->insert	= (strpos($rows[0]->accion,"I") > -1);
			$this->update	= (strpos($rows[0]->accion,"U") > -1);
			$this->delete 	= (strpos($rows[0]->accion,"D") > -1);
			$this->prints 	= (strpos($rows[0]->accion,"P") > -1);
			$this->export 	= (strpos($rows[0]->accion,"X") > -1);
			$this->status 	= (strpos($rows[0]->accion,"S") > -1);
		} else {
			if(count($rows) > 0 && file_exists(JPATH_MODULES . DS . $mod . DS . "views.xml")) {
				$this->error = 1;
			} else {
				$this->error = 2;
				$datos = "error=1&regresar=" . urlencode(JFunc::DRequest());
				header('Location: index.php?' . $datos); 					  
				exit();
			}
			$this->mod_name = "error";
			$this->path     = JPATH_MODULES . DS . $this->mod_name . DS;
			$this->xml      = simplexml_load_file($this->path . "views.xml");
			if(file_exists( $this->path. "lang" .DS . JFactory::getValueConf('config.lang') . ".php")) {
				require_once($this->path. "lang" .DS . JFactory::getValueConf('config.lang') . ".php");
			}
			if(file_exists( $this->path. "lang" .DS . "default" . ".php")) {
				require_once($this->path. "lang" .DS . "default" . ".php");
			}
			$meta = JFactory::getMetasObj();
			$meta->setTitle($meta->getTitle() . ": " . $this->getTitle());                     	
		}
	}
	function getModXml() {
		return $this->xml;
	}
	function getModPath() {
		return $this->path;
	}
	function getModId() {
		return $this->mod_id;
	}
	function getName() {
		return $this->mod_name;
	}
	function getTitle() {
		return $this->mod_title;
	}
	function getInsert() {
		return $this->insert;
	}
	function getUpdate() {
		return $this->update;
	}
	function getView() {
		return $this->view;
	}
	function getDelete() {
		return $this->delete;
	}
	function getPrint() {
		return $this->prints;
	}
	function getExport() {
		return $this->export;
	}
	function getStatus() {
		return $this->status;
	}
	function getCode() {
		$vista = JRequest::getVar("vista");
		if(!empty($this->xml)) {
			if(empty($vista)) {
				$vista = "default";
				$vista = $this->xml->$vista;
			}
			if(isset($this->xml->$vista)) {
				if(isset($this->xml->$vista->code) && file_exists($this->path.$vista .DS.$this->xml->$vista->code)) {
					require_once( $this->path.$vista .DS.$this->xml->$vista->code);
				}
			}
		}
	}
	function getError() {
		return $this->error;
	}
	function getStyles() {
		$vista = JRequest::getVar("vista");
		if(!empty($this->xml)) {
			if(empty($vista)) {
				$vista = "default";
				$vista = $this->xml->$vista;
			}
			if(isset($this->xml->$vista)) {
				if(isset($this->xml->$vista->style) && file_exists($this->path.$vista .DS.$this->xml->$vista->style)) {
					require_once( $this->path.$vista .DS.$this->xml->$vista->style);
				}
			}
		}
	}
	function getClases() {
		$vista = JRequest::getVar("vista");
		if(!empty($this->xml)) {
			if(empty($vista)) {
				$vista = "default";
				$vista = $this->xml->$vista;
			}
			if(isset($this->xml->$vista)) {
				if(isset($this->xml->$vista->clase) && file_exists($this->path.$vista .DS.$this->xml->$vista->clase)) {
					require_once( $this->path.$vista .DS.$this->xml->$vista->clase);
				}
			}
		}
	}
	function getJavaScript() {
		$vista = JRequest::getVar("vista");
		if(!empty($this->xml)) {
			if(empty($vista)) {
				$vista = "default";
				$vista = $this->xml->$vista;
			}
			if(isset($this->xml->$vista)) {
				if(isset($this->xml->$vista->java)  && file_exists($this->path.$vista .DS.$this->xml->$vista->java)) {
					require_once( $this->path.$vista .DS.$this->xml->$vista->java);
				}
			}
		}
	}
}