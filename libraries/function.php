<?php
class JFunc
{
  // Arma con en una variable todo los datos post y get 
  function DRequest()
  {
     $url = "";
	 $bok = false;
	 foreach($_REQUEST as $key=>$val)
     {
	    if(!empty($val))
		{
		  if(!$bok)
		  {$bok = true;}
		  else
		  {$url .= "&";}
		  $url .= $key . "=" . urlencode($val);
	    }
	 }	
	 return $url;
  }
  
  // Convierte Un Double a un string con formato
  function DString($P_VALOR)
    { $num = $P_VALOR;
      if(is_numeric($num))
        {$num = number_format($num,2,SEPDEC,SEPMIL);}
      return $num;
    }
  // Convierte un string con formato a double  
  function SDouble($P_VALOR)
    { $num = $P_VALOR;
      $num = str_replace(SEPMIL,"",$num);
      $num = str_replace(SEPDEC,"XX",$num);
      $num = str_replace("XX"  ,".",$num);
      return $num; 
    }  
    
  function Fhoy()
    {$hoy = date("d/m/Y");                           
     return $hoy;
    }    
    
	public static function FTarget()
    {
	date_default_timezone_set('America/Caracas');
	$hoy = date("ymdH_i_s");                           
     return $hoy;
    }
    
  function FSQL_S($P_VALOR)
    {$Valor = $P_VALOR;
     if(is_null($Valor) || $Valor =="")
       {$Valor = "null";}
     else
       {$Valor = "'".str_replace("'","''",$Valor)."'" ;}
     return $Valor;   
    }   
  Function SqlTime($P_VALOR)
  {
     $test = "null";
	 if(!empty($P_VALOR))
	 {
	  $dec  = $P_VALOR;
	  $dec  = str_ireplace("DATE_TIME_XLS","",$dec);
	  if(!empty($dec))
	    {$spd  = 86400;
         $secs = $spd * $dec;
         $test = strtotime('midnight') + $secs;
         $test = date ('H:i:s',$test);
	     $test = "Convert('" . $test . "',time)";
	    }
	  }
	  return $test;
  }  
  Function SqlDate($P_VALOR)
    {$Valor = $P_VALOR;
	 if(!empty($Valor))
	   $Valor = str_ireplace("DATE_FORMAT","",$Valor);
     if(!empty($Valor))
       {
	    $arr = explode("/",$Valor);
		if($arr[1] > 12)
	     $Valor = "STR_TO_DATE('".$Valor."','%m/%d/%Y')" ;
		else
	     $Valor = "STR_TO_DATE('".$Valor."','%d/%m/%Y')" ;
	   
	   }
     else
       {$Valor = "null";}	 
	 return $Valor;
	}  
  Function Acentos($P_VALOR)
    {$Valor = $P_VALOR;
     $Valor = str_replace("�" ,"&aacute;",$Valor);
     $Valor = str_replace("�" ,"&eacute;",$Valor);
     $Valor = str_replace("�" ,"&iacute;",$Valor);
     $Valor = str_replace("�" ,"&oacute;",$Valor);
     $Valor = str_replace("�" ,"&uacute;",$Valor);
     $Valor = str_replace("�" ,"&ntilde;",$Valor);
     $Valor = str_replace("�" ,"&Aacute;",$Valor);
     $Valor = str_replace("�" ,"&Eacute;",$Valor);
     $Valor = str_replace("�" ,"&Iacute;",$Valor);
     $Valor = str_replace("�" ,"&Oacute;",$Valor);
     $Valor = str_replace("�" ,"&Uacute;",$Valor);
     $Valor = str_replace("�" ,"&�tilde;",$Valor);
     $Valor = str_replace("�" ,"&Uuml;"  ,$Valor);
     $Valor = str_replace("�" ,"&uuml;"  ,$Valor);
     $Valor = str_replace("\"", "&quot;" ,$Valor);
     $Valor = str_replace("\'", "&quot;" ,$Valor);
     return $Valor;   
    }
    
      Function Imprimir($P_VALOR,$LEN)
    {$Valor = "";
     if(!empty($P_VALOR))
       {
         $array = explode(" ", $P_VALOR);
         $count = count($array);
         for($i = 0; $i < count($array); $i++)
          {$tem = $Valor;
           if(!empty($array[$i]))
             {if($i > 0)
                {$tem .= " ";}
              $tem .= $array[$i];
              if(strlen($tem) <= $LEN)
                {$Valor = $tem;}
              else
                {if(empty($Valor))
                   {$Valor = substr($tem,0,$LEN);}
                 $Valor .="...";
                 break;  
                }
            }   
          }
       }
     return $Valor;   
    }
}