<?php
class JComponent {
	var $component = array();
	function __construct() {
		jimport('component.detalle');
		$db  = JFactory::getDBO();
		$sql = "Select comp_id\n";
		$sql.= "	  ,comp_name\n";
		$sql.= "	  ,comp_mod\n";
		$sql.= "From sys_components\n";
		$sql.= "Where comp_sta = 'S'\n";
		$sql.= "Order by 1\n";
		$db->setQuery( $sql );
		$rows = $db->loadObjectList();
		for($i = 0 ; $i < count($rows); $i++) {
			$temp = new JCompdetall();
			$temp->setId($rows[$i]->comp_id);
			$temp->setName($rows[$i]->comp_name);
			$temp->setModulos($rows[$i]->comp_mod);
			$temp->load();
			$this->component[$rows[$i]->comp_name] = $temp;
		}
	}
	function getId($name) {
		$obj = $this->component[$name];
		return $obj->getId();
	}  
	function getName($name) {
		$obj = $this->component[$name];
		return $obj->getName();
	}  
	function getModulos($name) {
		$obj = $this->component[$name];
		return $obj->getModulos();
	}  
	function getShow($name) {
		$obj = $this->component[$name];
		$tem = JRequest::getVar("module");
		$dos = $obj->getModulos();
		if(stripos($dos,"all") > -1 ||  stripos($dos,$tem) > -1) {
			$obj->getCode();
		}
	}
	function getJavaScript() {
		foreach($this->component as $obj) {
			if(stripos($obj->getModulos(),"all") > -1 ||  stripos($obj->getModulos(),JRequest::getVar("module")) > -1) {
				$obj->getJavaScript();
			}
		}
		
	}
	function getStyles() {
		foreach($this->component as $obj) {
			if(stripos($obj->getModulos(),"all") > -1 ||  stripos($obj->getModulos(),JRequest::getVar("module")) > -1) {
				$obj->getStyles();
			}
		}
		
	}
}