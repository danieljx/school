<?php
class JDatabase extends JObject {

	var $query_result;
	var $row = array();
	var $rowset = array();
	var $num_queries = 0;
	
	var $name			= '';
	var $_sql			= '';
	var $_errorNum		= 0;
	var $_errorMsg		= '';
	var $_table_prefix	= 'ontv_';
	var $_resource		= '';
	var $_cursor		= null;
	var $_debug			= 0;
	var $_limit			= 0;
	var $_offset		= 0;
	var $_ticker		= 0;
	var $_log			= null;
	var $_nullDate		= null;
	var $_nameQuote		= null;
	var $_utf			= 0;
	var $_quoted	= null;
	var $_hasQuoted	= null;
	var $ObjUpd 	= null;
	var $ListIem	= null;
	function conectar($options	= array()) {
		$err_desc ="";
		$persistency = true;
		$this->persistency = $persistency;
		$this->user = $options['user'];
		$this->password = $options['password'];
		$this->server = $options['host'];
		$this->dbname = $options['database'];
		$this->_table_prefix =  $options['prefix'];
		$database	= array_key_exists('database', $options)	? $options['database']	: null;
		if($this->persistency) {
			$this->_resource = mysqli_connect($this->server, $this->user, $this->password, $this->dbname);
		} else {
			$this->_resource = mysqli_connect($this->server, $this->user, $this->password, $this->dbname);
		}
		if($this->_resource) {
			if($database != "") {
				$this->dbname = $database;
				$dbselect = mysqli_select_db($this->_resource,$this->dbname);
				if(!$dbselect) {
					mysqli_close($this->_resource);
					$this->_resource = $dbselect;
				}
			}
			return $this->_resource;
		} else {
			return false;
		}
	}
	function __construct( $options ) {
		// Register faked "destructor" in PHP4 to close all connections we might have made		
		if (version_compare(PHP_VERSION, '5') == -1) {
		   register_shutdown_function(array(&$this, '__destruct'));
		}
	}
	function __destruct() {
		$return = false;
		if(is_resource($this->_resource)) {
			$return = mysqli_close($this->_resource);
		}
		return $return;
	}
	function Quote( $text, $escaped = true ) {
		return '\''.($escaped ? $this->getEscaped( $text ) : $text).'\'';
	}
	function GetCol( $query ) {
		$this->setQuery( $query );
		return $this->loadResultArray();
	}
	function Execute( $query ) {
		jimport( 'database.recordset' );
		$query = trim( $query );
		$this->setQuery( $query );
		if(eregi( '^select', $query )) {
			$result = $this->loadRowList();
			return new JRecordSet( $result );
		} else {
			$result = $this->query();
			if ($result === false) {
				return false;
			} else {
				return new JRecordSet( array() );
			}
		}
	}	
	function SelectLimit( $query, $count, $offset=0 ) {
		jimport( 'database.recordset' );
		$this->setQuery( $query, $offset, $count );
		$result = $this->loadRowList();
		return new JRecordSet( $result );
	}		
	function PageExecute( $sql, $nrows, $page, $inputarr=false, $secs2cache=0 ) {
		jimport( 'database.recordset' );
		$this->setQuery( $sql, $page*$nrows, $nrows );
		$result = $this->loadRowList();
		return new JRecordSet( $result );
	}	
	function GetRow( $query ) {
		$this->setQuery( $query );
		$result = $this->loadRowList();
		return $result[0];
	}	
	function GetOne( $query ) {
		$this->setQuery( $query );
		$result = $this->loadResult();
		return $result;
	}	
	function sql_close() {
		$err_desc ="";
		if($this->_resource) {
			if($this->query_result) {
				mysqli_free_result($this->query_result);
			}
			$result = mysqli_close($this->_resource);
			return $result;
		} else {
			return false;
		}
	}
	function GenID( $foo1=null, $foo2=null ) {
		return '0';
	}
	function addQuoted( $quoted ) {
		if (is_string( $quoted )) {
			$this->_quoted[] = $quoted;
		} else {
			$this->_quoted = array_merge( $this->_quoted, (array)$quoted );
		}
		$this->_hasQuoted = true;
	}
	function hasUTF() {
		$verParts = explode( '.', $this->getVersion() );
		return ($verParts[0] == 5 || ($verParts[0] == 4 && $verParts[1] == 1 && (int)$verParts[2] >= 2));
	}
	function setUTF() {
		mysqli_query( $this->_resource,"SET NAMES 'utf8'" );
	}
	function getUTFSupport() {
		return $this->_utf;
	}
	function getErrorNum() {
		return $this->_errorNum;
	}
	function getErrorMsg($escaped = false) {
		if($escaped) {
			return addslashes($this->_errorMsg);
		} else {
			return $this->_errorMsg;
		}
	}	
	function getLog( ) {
		return $this->_log;
	}	
	function getPrefix() {
		return $this->_table_prefix;
	}
	function getNullDate() {
		return $this->_nullDate;
	}		
	function getTicker( ) {
		return $this->_ticker;
	}	
	function getEscaped( $text, $extra = false ) {
		$result = mysqli_real_escape_string( $this->_resource, $text );
		if ($extra) {
			$result = addcslashes( $result, '%_' );
		}
		return $result;
	}
	function nameQuote( $s ) {
		// Only quote if the name is not using dot-notation
		if (strpos( $s, '.' ) === false) {
			$q = $this->_nameQuote;
			if (strlen( $q ) == 1) {
				return $q . $s . $q;
			} else {
				return $q{0} . $s . $q{1};
			}
		}
		else {
			return $s;
		}
	}	
	function isQuoted( $fieldName ) {
		if ($this->_hasQuoted) {
			return in_array( $fieldName, $this->_quoted );
		} else {
			return true;
		}
	}
	function setQuery( $sql, $offset = 0, $limit = 0, $prefix='#__' ) {
		$this->_sql     = $this->replacePrefix( $sql, $prefix );
		$this->_limit	= (int) $limit;
		$this->_offset	= (int) $offset;
	}
	function insertid() {
		return mysqli_insert_id( $this->_resource );
	}		
	function query() {
		// Take a local copy so that we don't modify the original query and cause issues later
		$sql = $this->_sql;
		if ($this->_limit > 0 || $this->_offset > 0) {
			$sql .= ' LIMIT '.$this->_offset.', '.$this->_limit;
		}
		if ($this->_debug) {
			$this->_ticker++;
			$this->_log[] = $sql;
		}
		$this->_errorNum = 0;
		$this->_errorMsg = '';
		set_time_limit(0);
		$this->setUTF();
		$this->_cursor = mysqli_query( $this->_resource, $sql );

		if (!$this->_cursor && !($this->_cursor instanceof mysqli_result)) {
			$this->_errorNum = mysqli_errno( $this->_resource );
			$this->_errorMsg = mysqli_error( $this->_resource )." SQL=$sql";
			return false;
		}
		return $this->_cursor;
	}			
	function replacePrefix( $sql, $prefix='#__' ) {
		$sql = trim( $sql );

		$escaped = false;
		$quoteChar = '';

		$n = strlen( $sql );

		$startPos = 0;
		$literal = '';
		while ($startPos < $n) {
			$ip = strpos($sql, $prefix, $startPos);
			if ($ip === false) {
				break;
			}

			$j = strpos( $sql, "'", $startPos );
			$k = strpos( $sql, '"', $startPos );
			if (($k !== FALSE) && (($k < $j) || ($j === FALSE))) {
				$quoteChar	= '"';
				$j			= $k;
			} else {
				$quoteChar	= "'";
			}

			if ($j === false) {
				$j = $n;
			}

			$literal .= str_replace( $prefix, $this->_table_prefix,substr( $sql, $startPos, $j - $startPos ) );
			$startPos = $j;

			$j = $startPos + 1;

			if ($j >= $n) {
				break;
			}

			// quote comes first, find end of quote
			while (TRUE) {
				$k = strpos( $sql, $quoteChar, $j );
				$escaped = false;
				if ($k === false) {
					break;
				}
				$l = $k - 1;
				while ($l >= 0 && $sql{$l} == '\\') {
					$l--;
					$escaped = !$escaped;
				}
				if ($escaped) {
					$j	= $k+1;
					continue;
				}
				break;
			}
			if ($k === FALSE) {
				// error in the query - no end quote; ignore it
				break;
			}
			$literal .= substr( $sql, $startPos, $k - $startPos + 1 );
			$startPos = $k+1;
		}
		if ($startPos < $n) {
			$literal .= substr( $sql, $startPos, $n - $startPos );
		}
		return $literal;
	}
	function getAffectedRows() {
		return mysql_affected_rows( $this->_resource );
	}
	function queryBatch( $abort_on_error=true, $p_transaction_safe = false) {
		$this->_errorNum = 0;
		$this->_errorMsg = '';
		if ($p_transaction_safe) {
			$this->_sql = rtrim($this->_sql, "; \t\r\n\0");
			$si = $this->getVersion();
			preg_match_all( "/(\d+)\.(\d+)\.(\d+)/i", $si, $m );
			if ($m[1] >= 4) {
				$this->_sql = 'START TRANSACTION;' . $this->_sql . '; COMMIT;';
			} else if ($m[2] >= 23 && $m[3] >= 19) {
				$this->_sql = 'BEGIN WORK;' . $this->_sql . '; COMMIT;';
			} else if ($m[2] >= 23 && $m[3] >= 17) {
				$this->_sql = 'BEGIN;' . $this->_sql . '; COMMIT;';
			}
		}
		$query_split = $this->splitSql($this->_sql);
		$error = 0;
		foreach ($query_split as $command_line) {
			$command_line = trim( $command_line );
			if ($command_line != '') {
				$this->_cursor = mysqli_query( $this->_resource, $command_line );
				if ($this->_debug) {
					$this->_ticker++;
					$this->_log[] = $command_line;
				}
				if (!$this->_cursor) {
					$error = 1;
					$this->_errorNum .= mysqli_errno( $this->_resource ) . ' ';
					$this->_errorMsg .= mysqli_error( $this->_resource )." SQL=$command_line <br />";
					if ($abort_on_error) {
						return $this->_cursor;
					}
				}
			}
		}
		return $error ? false : true;
	}
	function getNumRows( $cur=null ) {
		return mysqli_num_rows( $cur ? $cur : $this->_cursor );
	}
	function loadResult() {
		if (!($cur = $this->query())) {
			return null;
		}
		$ret = null;
		if ($row = mysqli_fetch_row( $cur )) {
			$ret = $row[0];
		}
		mysqli_free_result( $cur );
		return $ret;
	}
	function loadResultArray($numinarray = 0) {
		if (!($cur = $this->query())) {
			return null;
		}
		$array = array();
		while ($row = mysqli_fetch_row( $cur )) {
			$array[] = $row[$numinarray];
		}
		mysqli_free_result( $cur );
		return $array;
	}
	function loadAssoc() {
		if (!($cur = $this->query())) {
			return null;
		}
		$ret = null;
		if ($array = mysqli_fetch_assoc( $cur )) {
			$ret = $array;
		}
		mysqli_free_result( $cur );
		return $ret;
	}		
	function loadAssocList( $key='' )
	{
		if (!($cur = $this->query())) {
			return null;
		}
		$array = array();
		while ($row = mysqli_fetch_assoc( $cur )) {
			if ($key) {
				$array[$row[$key]] = $row;
			} else {
				$array[] = $row;
			}
		}
		mysqli_free_result( $cur );
		return $array;
	}
	function loadObject( ) {
		if (!($cur = $this->query())) {
			return null;
		}
		$ret = null;
		if ($object = mysqli_fetch_object( $cur )) {
			$ret = $object;
		}
		mysqli_free_result( $cur );
		return $ret;
	}
	function loadObjectList( $key='' ) {
		if (!($cur = $this->query())) {
			return null;
		}
		$array = array();
		while ($row = mysqli_fetch_object( $cur )) {
			if ($key) {
				$array[$row->$key] = $row;
			} else {
				$array[] = $row;
			}
		}
		mysqli_free_result( $cur );
		return $array;
	}
	function stderr( $showSQL = false ) {
		if ( $this->_errorNum != 0 ) {
			return "DB function failed with error number $this->_errorNum"
			."<br /><font color=\"red\">$this->_errorMsg</font>"
			.($showSQL ? "<br />SQL = <pre>$this->_sql</pre>" : '');
		} else {
			return "DB function reports no errors";
		}
	}	
	function loadRow() {
		if (!($cur = $this->query())) {
			return null;
		}
		$ret = null;
		if ($row = mysqli_fetch_row( $cur )) {
			$ret = $row;
		}
		mysqli_free_result( $cur );
		return $ret;
	}
	function loadRowList( $key=null ) {
		if (!($cur = $this->query())) {
			return null;
		}
		$array = array();
		while ($row = mysqli_fetch_row( $cur )) {
			if ($key !== null) {
				$array[$row[$key]] = $row;
			} else {
				$array[] = $row;
			}
		}
		mysqli_free_result( $cur );
		return $array;
	}
	function updateObject( $table, &$object, $keyName, $updateNulls=true) {
		$fmtsql = 'UPDATE '.$this->nameQuote($table).' SET %s WHERE %s';
		$tmp = array();
		foreach (get_object_vars( $object ) as $k => $v) {
			if( is_array($v) or is_object($v) or $k[0] == '_' ) { 
			// internal or NA field
				continue;
			}
			if( $k == $keyName ) { // PK not to be updated
				$where = $keyName . '=' . $this->Quote( $v );
				continue;
			}
			if ($v === null) {
				if ($updateNulls) {
					$val = 'NULL';
				} else {
					continue;
				}
			} else {
				if($v=="NOW_AHORA")
				  $val = "now()";
			    else if (stripos($v, "DATE_FORMAT") !== false) 
			      $val = JFunc::SqlDate($v);
				else
				  $val = $this->isQuoted( $k ) ? $this->Quote( $v ) : (int) $v;
			}
			$tmp[] = $this->nameQuote( $k ) . '=' . $val;
		}
		$this->setQuery( sprintf( $fmtsql, implode( ",", $tmp ) , $where ) );
		return $this->query();
	}			
	function getVersion() {
		return mysqli_get_server_info( $this->_resource );
	}
	function getCollation () {
		if ( $this->hasUTF() ) {
			$this->setQuery( 'SHOW FULL COLUMNS FROM #__content' );
			$array = $this->loadAssocList();
			return $array['4']['Collation'];
		} else {
			return "N/A (mySQL < 4.1.2)";
		}
	}
	function getTableList() {
		$this->setQuery( 'SHOW TABLES' );
		return $this->loadResultArray();
	}
	function getTableCreate( $tables ) {
		settype($tables, 'array'); //force to array
		$result = array();
		foreach ($tables as $tblval) {
			$this->setQuery( 'SHOW CREATE table ' . $this->getEscaped( $tblval ) );
			$rows = $this->loadRowList();
			foreach ($rows as $row) {
				$result[$tblval] = $row[1];
			}
		}
		return $result;
	}
	function getTableFields( $tables, $typeonly = true ) {
		settype($tables, 'array'); //force to array
		$result = array();
		foreach ($tables as $tblval) {
			$this->setQuery( 'SHOW FIELDS FROM ' . $tblval );
			$fields = $this->loadObjectList();
			if($typeonly) {
				foreach ($fields as $field) {
					$result[$tblval][$field->Field] = preg_replace("/[(0-9)]/",'', $field->Type );
				}
			} else {
				foreach ($fields as $field) {
					$result[$tblval][$field->Field] = $field;
				}
			}
		}
		return $result;
	}	
	function splitSql( $queries ) {
		$start = 0;
		$open = false;
		$open_char = '';
		$end = strlen($queries);
		$query_split = array();
		for($i=0;$i<$end;$i++) {
			$current = substr($queries,$i,1);
			if(($current == '"' || $current == '\'')) {
				$n = 2;
				while(substr($queries,$i - $n + 1, 1) == '\\' && $n < $i) {
					$n ++;
				}
				if($n%2==0) {
					if ($open) {
						if($current == $open_char) {
							$open = false;
							$open_char = '';
						}
					} else {
						$open = true;
						$open_char = $current;
					}
				}
			}
			if(($current == ';' && !$open)|| $i == $end - 1) {
				$query_split[] = substr($queries, $start, ($i - $start + 1));
				$start = $i + 1;
			}
		}
		return $query_split;
	}
	function insertObject( $table, &$object, $keyName = NULL ) {
		$fmtsql = 'INSERT INTO '.$this->nameQuote($table).' ( %s ) VALUES ( %s ) ';
		$fields = array();
		foreach (get_object_vars( $object ) as $k => $v) {
			if (is_array($v) or is_object($v) or $v === NULL) {
				continue;
			}
			if ($k[0] == '_') { // internal field
				continue;
			}
			$fields[] = $this->nameQuote( $k );
			
			if($v=="NOW_AHORA")
               $values[] = "now()";
			else if (stripos($v, "DATE_FORMAT") !== false) 
			   $values[] = JFunc::SqlDate($v);
			else if (stripos($v, "DATE_TIME_XLS") !== false) 
			   $values[] = JFunc::SqlTime($v);
			else
               $values[] = $this->isQuoted( $k ) ? $this->Quote( $v ) : (int) $v;			
		}
		$this->setQuery( sprintf( $fmtsql, implode( ",", $fields ) ,  implode( ",", $values ) ) );
		if (!$this->query()) {
			return false;
		}
		$id = $this->insertid();
		if ($keyName && $id) {
			$object->$keyName = $id;
		}
		return true;
	}	
	function ListarItem(&$object) {
		$fields = array();
		foreach (get_object_vars( $object ) as $k => $v) {
			if (is_array($v) or is_object($v) or $v === NULL) {
				continue;
			}
			if ($k[0] == '_') { // internal field
				continue;
			}
			$fields[] = $this->nameQuote( $k );
			
			if($v=="NOW_AHORA")
               $values[] = "now()";
			else if (stripos($v, "DATE_FORMAT") !== false) 
			   $values[] = JFunc::SqlDate($v);
			else
               $values[] = $this->isQuoted( $k ) ? $this->Quote( $v ) : (int) $v;			
		}
		return true;
	}
	function explain() {
		$temp = $this->_sql;
		$this->_sql = "EXPLAIN $this->_sql";
		if (!($cur = $this->query())) {
			return null;
		}
		$first = true;
		$buffer = '<table id="explain-sql">';
		$buffer .= '<thead><tr><td colspan="99">'.$this->getQuery().'</td></tr>';
		while ($row = mysqli_fetch_assoc( $cur )) {
			if ($first) {
				$buffer .= '<tr>';
				foreach ($row as $k=>$v) {
					$buffer .= '<th>'.$k.'</th>';
				}
				$buffer .= '</tr>';
				$first = false;
			}
			$buffer .= '</thead><tbody><tr>';
			foreach ($row as $k=>$v) {
				$buffer .= '<td>'.$v.'</td>';
			}
			$buffer .= '</tr>';
		}
		$buffer .= '</tbody></table>';
		mysqli_free_result( $cur );
		$this->_sql = $temp;
		return $buffer;
	}
	function getQuery() {
		return $this->_sql;
	}
}