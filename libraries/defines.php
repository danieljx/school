<?php
  defined( '_JEXEC' ) or die( 'Restricted access' );
  $parts = explode( DS, JPATH_BASE );
  define( 'JPATH_ROOT',		implode( DS, $parts ) );
  define( 'JPATH_LIBRARIES',	JPATH_ROOT.DS.'libraries' );
  define( 'JPATH_MODULES',	JPATH_ROOT.DS.'modules' );
  define( 'JPATH_COMPONENTS',	JPATH_ROOT.DS.'components' );
  define( 'JPATH_IMAGES',	JPATH_ROOT.DS.'front'.DS.'images' );
  define( 'JPATH_CSS',	        JPATH_ROOT.DS.'front'.DS.'css' );
  define( 'JPATH_JS',	        JPATH_ROOT.DS.'front'.DS.'js' );

// aqui se define el separador de mil y el separador decimal
  define('SEPDEC',',');
  define('SEPMIL','.');

  //echo JPATH_ROOT;
  require_once( JPATH_LIBRARIES.DS.'function.php');
  require_once( JPATH_LIBRARIES.DS.'loader.php');
  require_once( JPATH_LIBRARIES.DS.'request.php');
  require_once( JPATH_LIBRARIES.DS.'framework.php');    
  define( 'JPATH_EXCEL',	JPATH_LIBRARIES.DS.'Excel' );
  
