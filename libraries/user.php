<?php
class JUser {
	var $user_id		= null;
	var $name			= null;
	var $username		= null;
	var $email			= null;
	var $password		= null;
	var $rule_id 		= null;
	var $rule_name 		= null;
	var $lastvisitDate	= null;
	var $ter_name		= null;
	var $_errorMsg		= null;
	function __construct() {
		if (isset($_SESSION['user_id'])) {
			if(!$_SESSION['user_id']) {
				$_SESSION['user_id'];
			}
			$this->user_id = $_SESSION['user_id'];   
		} 
		if(is_null($this->user_id)) {
			$this->user_id = -1;
		} else {
			$db = JFactory::getDBO();		 
			$sql = "Select  DATE_FORMAT(NOW(),'%d %b %Y %T') as timers\n";
			$sql.= "	   ,u.user_id\n";
			$sql.= "	   ,u.user_title\n";
			$sql.= "	   ,u.user_email\n";
			$sql.= "	   ,u.user_name\n";
			$sql.= "	   ,u.registerDate\n";
			$sql.= "	   ,DATE_FORMAT(u.lastvisitDate,'%d/%m/%Y') as lastvisitDate\n";
			$sql.= "	   ,u.user_lock\n";
			$sql.= "	   ,u.rule_id\n";
			$sql.= "	   ,u.ter_name\n";
			$sql.= "	   ,u.third_id\n";
			$sql.= "	   ,r.rule_name\n";
			$sql.= "From sys_user u\n";
			$sql.= "Inner join sys_rule r on r.rule_id = u.rule_id\n";
			$sql.= "Where u.user_id = " . $this->user_id . "\n";
			$sql.= "Group by u.user_id\n";
			//echo $sql . "-----";
			$db->setQuery( $sql ); 
			$rows = $db->loadObjectList();
			if(count($rows)!=0) {
				$this->timers			= $rows[0]->timers;
				$this->user_id			= $rows[0]->user_id;
				$this->user_title      	= $rows[0]->user_title;
				$this->user_email		= $rows[0]->user_email;
				$this->user_name      	= $rows[0]->user_name;
				$this->registerDate		= $rows[0]->registerDate;
				$this->lastvisitDate 	= $rows[0]->lastvisitDate;
				$this->rule_id			= $rows[0]->rule_id;
				$this->ter_name 		= $rows[0]->ter_name;
				$this->third_id 		= $rows[0]->third_id;
				$this->rule_name      	= $rows[0]->rule_name;
			}
		}  
	}
	function getVisit() {
		return $this->lastvisitDate;
	}
	function getError() {
		return $this->_errorMsg;
	}
	function getEmail() {
		return $this->user_email;
	}
	function getRolId() {
		return $this->rule_id;
	}
	function getRol() {
		return $this->rule_name;
	}
	function getId() {
		return $this->user_id;
	}
	function getUserName() {
		return $this->user_name;
	}
	function getName() {
		return $this->user_title;
	}
	function getTerName() {
		return $this->ter_name;
	}
	function getUserThird() {
		return $this->third_id;
	}
}