<?php
class JFactory {

	public static function getHead() {
		$meta = JFactory::getMetasObj();
		$meta->getHead();
	}	
	public static function getMeta() {
		$meta = JFactory::getMetasObj();
		$meta->getMeta();
	}
	public static function load() {
		$conf = JFactory::getConfig();

		$meta = JFactory::getMetasObj();
		$meta->setTitle($conf->getValue('config.title'));
		$meta->setDesc( $conf->getValue('config.desc'));
		$meta->setKey(  $conf->getValue('config.key'));
        
		
		
		$user = JFactory::getUser();
		$comp = JFactory::getComponent();
		$mod  = JFactory::getModulo();
		
		
                if(file_exists( JPATH_ROOT. DS. "lang" .DS . $conf->getValue('config.lang') . ".php"))
                  {require_once(JPATH_ROOT .DS. "lang" .DS . $conf->getValue('config.lang') . ".php");}
                if(file_exists( JPATH_ROOT. DS. "lang" .DS . "default" . ".php"))
                  {require_once(JPATH_ROOT. DS. "lang" .DS . "default" . ".php");}
	}
	public static function getJavaScript() {
		//echo "\t// Codigo Java Script FrameWork\n"; 
		require_once(JPATH_ROOT . DS . "lang.php"); 
		$comp = JFactory::getComponent();
		$comp->getJavaScript(); 
		$mod = JFactory::getModulo();
		$mod->getJavaScript();
	}
	public static function getStyle()
	{
		//echo "\t// Codigo Style FrameWork\n"; 
		$comp = JFactory::getComponent();
		$comp->getStyles(); 
		$mod = JFactory::getModulo();
		$mod->getStyles();
	}
	public static function getShowMod()
	{
		$mod = JFactory::getModulo();
		$mod->getCode();
	}
	public static function getClases()
	{
		$mod = JFactory::getModulo();
		$mod->getClases();
	}
	public static function getShowComp($name)
	{
		$comp = JFactory::getComponent();
		$comp->getShow($name);
	}
	public static function getValueConf($name)
	{
		 $conf = JFactory::getConfig();
		 return $conf->getValue($name);
	}
	public static function &getMetasObj()
	{
		static $instance;

		if (!is_object($instance))
		{
  		  $instance = JFactory::_createMetasObj();
		}

		return $instance;
	}
	public static function &_createMetasObj()
	{
		jimport("metas");
		$com = new JMetas();
		return $com;
	}	
	public static function &getComponent()
	{
		static $instance;

		if (!is_object($instance))
		{
  		  $instance = JFactory::_createComponent();
		}

		return $instance;
	}
	public static function &_createComponent()
	{
		jimport("component");
		$com = new JComponent();
		return $com;
	}	
	public static function &_createConfig($file, $type = 'PHP')
	{
		jimport('registry.registry');

		require_once $file;

		// Create the registry with a default namespace of config
		$registry = new JRegistry('config');

		// Create the JConfig object
		$config = new JConfig();

		// Load the configuration values into the registry
		$registry->loadObject($config);

		return $registry;
	}
	public static function &getConfig()
	{
		static $instance;

		if (!is_object($instance))
		{
  		  $instance = JFactory::_createConfig(JPATH_ROOT.DS.'config.php', 'PHP');
		}

		return $instance;
	}
	public static function &getModulo()
	{
		static $instance;

		if (!is_object($instance))
		{
			$instance = JFactory::_createModulo();
		}

		return $instance;
	}
	public static function &_createModulo()
	{
		jimport("module");
		$user = new JModule();
		return $user;
	}	
	public static function &getUser()
	{
		static $instance;

		if (!is_object($instance))
		{
			$instance = JFactory::_createUser();
		}

		return $instance;
	}
	public static function &_createUser()
	{
		jimport("user");
		$user = new JUser();
		return $user;
	}
	public static function &getDBO()
	{
		static $instance;

		if (!is_object($instance))
		{
			$instance = JFactory::_createDBO();
		}

		return $instance;
	}	

	public static function &_createDBO()
	{
		$conf =& JFactory::getConfig();

		$host 		= $conf->getValue('config.host');
		$user 		= $conf->getValue('config.user');
		$password 	= $conf->getValue('config.password');
		$database	= $conf->getValue('config.db');
		$prefix 	= $conf->getValue('config.dbprefix');
		$driver 	= $conf->getValue('config.dbtype');
		
		
		jimport('database');

		$options	= array ( 'driver' => $driver, 'host' => $host, 'user' => $user, 'password' => $password, 'database' => $database, 'prefix' => $prefix );

		$db =new JDatabase( $options );
		if(is_object($db))
		  {
		  	
		   $db->conectar($options);	
		  
		  
		  }

		return $db;
	}
	
	public static function getReq($name, $default = null, $hash = 'default')
	{
		$hash = strtoupper( $hash );
		if ($hash === 'METHOD') {
		   $hash = strtoupper( $_SERVER['REQUEST_METHOD'] );
		}
		
		$var = $default;

		// Get the input hash
		switch ($hash)
		{
			case 'GET' :
				$input = &$_GET;
				break;
			case 'POST' :
				$input = &$_POST;
				break;
			case 'FILES' :
				$input = &$_FILES;
				break;
			case 'COOKIE' :
				$input = &$_COOKIE;
				break;
			case 'ENV'    :
				$input = &$_ENV;
				break;
			case 'SERVER'    :
				$input = &$_SERVER;
				break;
			default:
				$input = &$_REQUEST;
				$hash = 'REQUEST';
				break;
		}
		
		if(isset($input[$name]) && !empty($input[$name]))
		  {$var = $input[$name];}

		return $var;
	}
    public static function &getXMLnode($object, $param) {
       foreach($object as $key => $value) {
           if(isset($object->$key->$param)) {
               return $object->$key->$param;
           }
           if(is_object($object->$key)&&!empty($object->$key)) {
               $new_obj = $object->$key;
               // Must use getXMLnode function there (recursive)
               $ret = getXMLnode($new_obj, $param);  

           }
       }
       if($ret) return (string) $ret;
       return false;
    }
	
}