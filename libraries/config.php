<?php
class JFrameworkConfig
{
	var $dbtype 	= 'mysql';
	var $host 	= 'localhost';
	var $user 	= '';
	var $password 	= '';
	var $db 	= '';
	var $dbprefix 	= 'jos_';
	var $language 	= 'spanish';
}