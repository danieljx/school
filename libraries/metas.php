<?php
class JMetas
{
	var $title   = null;
	var $desc    = null;
	var $keyword = null;
	var $css     = array();
	var $js      = array();
	
        function __construct()	
        {
        	
        	if(file_exists(JPATH_BASE . DS. "front.xml"))
        	  {
        	  	$xml = simplexml_load_file(JPATH_BASE . DS. "front.xml");
        	  	
        	  	foreach ($xml->css->file as $value)
        	  	{
        	  		$i = count($this->css) -1;
        	  		$this->css[$i] = $value;
        	  	}
        	  	
        	  	foreach ($xml->js->file as $value)
        	  	{
        	  		$i = count($this->js) -1;
        	  		$this->js[$i] = $value;
        	  	}
        	  
        	  }
        }
	
	function getTitle()
	{return $this->title;}
	function getDesc()
	{return $this->desc;}
	function getKey()
	{return $this->keyword;}
	
	function setTitle($var)
	{$this->title = $var;}
	function setDesc($var)
	{$this->desc = $var;}
	function setKey($var)
	{$this->keyword = $var;}
	
	function getMeta()
	{
		echo "    <title>" . $this->title. "</title>\n";
		echo "    <meta  http-equiv=\"Content-Type\"  content=\"text/html; charset=UTF-8\" />\n";
		echo "    <meta name=\"description\"  content=\"" . $this->desc  . "\" />\n";
		echo "    <meta name=\"keywords\" content=\"" . $this->keyword  . "\" />\n";
	}	
	function getStyles()
	{
		foreach ($this->css as $value)
		{
			echo "    <link rel=\"stylesheet\" href=\"" . $value . "\" type=\"text/css\" />\n";
		}
	}
	function getJavaScript()
	{
		foreach ($this->js as $value)
		{
			echo "    <script type=\"text/javascript\"  language=\"JavaScript\" src=\"" . $value . "\"></script>\n";
		}
	}
	function getHead()
	{
		$this->getMeta();
		$this->getStyles();
		$this->getJavaScript();
	}
}