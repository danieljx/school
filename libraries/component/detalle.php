<?php
class JCompdetall
{
	var $comp_id   	= null;
	var $comp_name 	= null;
	var $modulos 	= null;
	var $xml     	= null;
	var $path    	= null;
	
	function load()
	{
		$this->path = JPATH_COMPONENTS . DS . $this->comp_name . DS;
		$this->xml = simplexml_load_file($this->path . "views.xml");
                
                if(file_exists( $this->path. "lang" .DS . JFactory::getValueConf('config.lang') . ".php"))
                  {require_once($this->path. "lang" .DS . JFactory::getValueConf('config.lang') . ".php");}
                if(file_exists( $this->path. "lang" .DS . "default.php"))
                  {require_once($this->path. "lang" .DS . "default.php");}
	}
	
	function getId()
	{
		return $this->comp_id;
	}
	function setId($par)
	{
		$this->comp_id = $par;
	}
	function getName()
	{
		return $this->comp_name;
	}
	function setName($par)
	{
		$this->comp_name = $par;
	}
	function getModulos()
	{
		return $this->modulos;
	}
	function setModulos($par)
	{
		$this->modulos = $par;
	}
	function getStyles()
	{
		if(isset($this->xml->style))
		  {
		  	require_once($this->path . $this->xml->style);
		  }
	}
	function getJavaScript()
	{
		if(isset($this->xml->java))
		  {
		  	require_once($this->path . $this->xml->java);
		  }
	}
	function getCode()
	{
		if(isset($this->xml->code))
		  {
		  	require_once($this->path . $this->xml->code);
		  }
	}
}	