<?php
	session_start();
	ob_start();
	define( '_JEXEC', 1 );
	define('JPATH_BASE', dirname(__FILE__) );
	define( 'DS', DIRECTORY_SEPARATOR );
	require_once ( JPATH_BASE .DS.'libraries'.DS.'defines.php' );
	$title	= JRequest::getVar("title");
	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=Listado-". date("d-m-Y-H-i-s") . ".xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n";
	echo "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n";
	echo "<head>\n";
	echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n";
	echo "<title>" . $title . "</title>\n";
	echo "</head>\n";
	echo "<body>\n";
	echo "<table width=\"600\" border=\"0\">\n";
	echo "<tr>\n";
	echo "<th width=\"600\">\n";
	echo "<div style=\"color:#003; text-align:center; text-shadow:#666;\"><font size=\"+2\">" . $title . "<br /></font></div></th>\n";
	echo "</tr>\n";
	echo "</table>\n";
	JFactory::getShowMod();
	echo "</body>\n";
	echo "</html>\n";
?>