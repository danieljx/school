<?php 
	$db   = JFactory::getDBO();
	$user = JFactory::getUser();
	$rol  = $user->getRolId();
	if($user->getId() == -1) {
		echo "<nav class=\"navbar navbar-default navbar-primary\">";
		echo "<div class=\"container-fluid\">\n";
	} else {
		echo "<nav class=\"navbar navbar-default navbar-fixed-top navbar-primary\">";
		echo "<div class=\"container-fluid\">\n";
		echo "<div class=\"navbar-header\">\n";
		echo "<a class=\"navbar-brand\" href=\"#\" style=\"width: 50px; padding: 2px; margin-left: 5px;\"><img style=\"width: 90%;\" src=\"front/images/logo.png\"></a>\n";
		echo "</div>\n";
		echo "<div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">\n";
		echo "<ul class=\"nav navbar-nav\">\n";
		echo "<li><a id=\"linkUsers\" data-toggle=\"collapse\" href=\"#collapseUsers\" aria-expanded=\"false\" aria-controls=\"collapseUsers\">" . $user->getName() . "</a></li>\n";
		echo "</ul>\n";
		echo "<div id=\"incoNavBar\" class=\"collapse navbar-collapse navbar-left\">\n";
		$sql = "Select m.men_id\n";
		$sql.= "	  ,m.men_name\n";
		$sql.= "	  ,m.men_desc\n";
		$sql.= "	  ,m.men_type\n";
		$sql.= "	  ,m.men_accion\n";
		$sql.= "	  ,m.men_target\n";
		$sql.= "	  ,m.men_order\n";
		$sql.= "	  ,m.men_icon\n";
		$sql .= "From sys_menu m\n";
		$sql .= "Inner Join sys_rule_accion r on r.accion_item = m.men_id and r.accion_type = 'MEN' and r.rule_id = " . $rol . "\n";
		$sql .= "Where  m.men_father = 1\n";
		$sql .= "  and  m.men_sta = 'S'\n";
		$sql .= "  and  m.men_class = 'T'\n";
		$sql .= "  and  m.men_type = 'ICO'\n";
		$sql .= "Order by m.men_order\n";
		// echo $sql . "---";
		$db->setQuery( $sql );
		$rows = $db->loadObjectList();
		for($i = 0; $i < count($rows) ; $i++) {
			$men_name = $rows[$i]->men_name;
			
			if(defined($rows[$i]->men_desc)) {
				$men_name = constant($rows[$i]->men_desc);
			}
			echo "<button type=\"button\" id=\"topButton_" . $rows[$i]->men_id . "\" data-titleX=\"" . $men_name . "\" data-contentX=\"#contentPopover_" . $rows[$i]->men_id . "\" class=\"buttonIcon btn btn-primary navbar-btn\"><span class=\"sr-only\">" . $men_name . "</span>\n";
			echo "<i class=\"glyphicon " . $rows[$i]->men_icon . "\"></i>\n";
			echo "<span class=\"badge\">0</span>\n";
			echo "</button>\n";
			echo "<div id=\"contentPopover_" . $rows[$i]->men_id . "\" style=\"display: none\">\n";
			echo " contenido <b>" . $rows[$i]->men_id . "</b> - " . $men_name . "\n";
			echo "</div>\n";
		}
		echo "</div>\n";
		echo "</div>\n";
	}
	echo "</div>\n";
	echo "</nav>\n";