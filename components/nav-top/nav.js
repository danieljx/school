$(function(){
    $("#incoNavBar > button.buttonIcon").popover({
        html : true,
        placement : "bottom",
        content: function() {
          return $($(this).data("contentx")).html();
        },
        title: function() {
          return $(this).data("titlex");
        }
    });
});