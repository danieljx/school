<?php 
function MenuSubNivel($id,$sep) { 
	$temp	= "";
	$db		= JFactory::getDBO();
	$user	= JFactory::getUser();
	$rol	= $user->getRolId();

	$sql = "Select m.men_id\n";
	$sql.= "	  ,m.men_name\n";
	$sql.= "	  ,m.men_desc\n";
	$sql.= "	  ,m.men_type\n";
	$sql.= "	  ,m.men_accion\n";
	$sql.= "	  ,m.men_target\n";
	$sql.= "	  ,m.men_order\n";
	$sql.= "	  ,m.men_icon\n";
	$sql.= "From sys_menu m\n";
	$sql.= "Inner Join sys_rule_accion r on r.accion_item = m.men_id and r.accion_type = 'MEN' and r.rule_id = " . $rol . "\n";
	$sql.= "Where  m.men_father = " . $id . "\n";
	$sql.= "  and  m.men_sta = 'S'\n";
	$sql.= "  and  m.men_class = 'L'\n";
	$sql.= "Order by m.men_order\n";
	$db->setQuery( $sql );
	$rows = $db->loadObjectList();
	$arrow = true;
	for($i = 0; $i < count($rows) ; $i++) {
		$men_name = $rows[$i]->men_name;
		if(defined($rows[$i]->men_desc)) {
			$men_name = constant($rows[$i]->men_desc);
		}
		if($arrow) {
			// $temp .=  "::before";
			$arrow = false;
		}
		if($rows[$i]->men_type == "SUB") {
			$temp2 = MenuSubNivel($rows[$i]->men_id,$sep . "  ");
			if(!empty($temp2)) {
				if(!empty($temp)) {
					$temp .= "<li role=\"presentation\" class=\"divider\"></li>\n";
				}
				$target = "dropdownMenu" . $rows[$i]->men_id;
				$temp .= $sep . "\t\t\t<li><a class=\"trigger right-caret\" id=\"" . $target . "\" href=\"#\" data-toggle=\"dropdown\" aria-expanded=\"false\" class=\"dropdown-toggle\" aria-haspopup=\"true\" role=\"button\">\n";
				if(!empty($rows[$i]->men_icon)) {
					$temp .= "<i class=\"glyphicon " . $rows[$i]->men_icon . "\"></i>\n";
				}
				$temp .=  "&nbsp;&nbsp;" . $men_name;
				$temp .= "</a>\n" . $sep . "  <ul aria-labelledby=\"" . $target . "\" class=\"dropdown-menu sub-menu\" role=\"menu\">\n" . $temp2 . $sep . "  </ul>" . $sep . "  </li>\n";
			}
		} else {
			if(!empty($temp)) {
				$temp .= "<li role=\"presentation\" class=\"divider\"></li>\n";
			}
			$temp .= $sep . "\t\t\t<li role=\"presentation\"><a role=\"menuitem\" href=\"" . $rows[$i]->men_accion . "\"  " . (!empty($rows[$i]->men_target) ? "target=\"" .$rows[$i]->men_target ."\"": "" )  . ">\n";
			if(!empty($rows[$i]->men_icon)) {
				$temp .= "<i class=\"glyphicon " . $rows[$i]->men_icon . "\"></i>\n";
			}
			$temp .= "&nbsp;&nbsp;" . $men_name . "</a></li>\n";			
		}
	}
	return $temp;
}
$db   = JFactory::getDBO();
$user = JFactory::getUser();
$rol  = $user->getRolId();
	if($user->getId() != -1) {
		$imgSrc 	= "";
		$thirdPath 	= "";
		$sql  = "Select t.third_id\n";
		$sql .= "	   ,t.third_path\n";
		$sql .= "	   ,t.third_img\n";
		$sql .= "From mod_third t\n";
		$sql .= "Where t.third_id = '" . $user->getUserThird() . "'\n";
		$db->setQuery( $sql );
		$rows = $db->loadObjectList();
		if(count($rows) > 0) {
			$imgSrc 	= $rows[0]->third_img;
			$thirdPath 	= $rows[0]->third_path;
		}
		echo "\t<div id=\"headerMenulLeft\" class=\"col-sm-3 col-md-2 sidebar\">\n";
		echo "<div class=\"row\">\n";
		echo "	<div class=\"profile-header-container\">\n";
		echo "		<div class=\"profile-header-img\">\n";
		if(!empty($imgSrc) && file_exists(JPATH_IMAGES . "/profile/" . $thirdPath . "/profile/" . $imgSrc)) {
			echo "			<span class=\"img-circle img-circle-bg\" style=\"background-image: url('" . JPATH_IMAGES . "/profile/" . $thirdPath . "/profile/" . $imgSrc . "')\"></span>\n";
		} else {
			echo "			<span class=\"img-circle glyphicon glyphicon-user\"></span>\n";
		}
		echo "			<div class=\"rank-label-container\">\n";
		echo "				<span class=\"label label-default rank-label\"><a data-toggle=\"collapse\" href=\"#collapseUsers\" aria-expanded=\"false\" aria-controls=\"collapseUsers\">" . $user->getName() . "</a></span>\n";
		echo "			</div>\n";
		echo "		</div>\n";
		echo "	</div>\n";
		echo "</div>\n";
		echo "\t\t<ul class=\"nav nav-sidebar\">\n";
		$sql = "Select m.men_id\n";
		$sql.= "	  ,m.men_name\n";
		$sql.= "	  ,m.men_desc\n";
		$sql.= "	  ,m.men_type\n";
		$sql.= "	  ,m.men_accion\n";
		$sql.= "	  ,m.men_target\n";
		$sql.= "	  ,m.men_order\n";
		$sql.= "	  ,m.men_icon\n";
		$sql.= "From sys_menu m\n";
		$sql.= "Inner Join sys_rule_accion r on r.accion_item = m.men_id and r.accion_type = 'MEN' and r.rule_id = " . $rol . "\n";
		$sql.= "Where  m.men_father = 1\n";
		$sql.= "  and  m.men_sta = 'S'\n";
		$sql.= "  and  m.men_class = 'L'\n";
		$sql.= "Order by m.men_order\n";
		// echo $sql . "---";
		$db->setQuery( $sql );
		$rows = $db->loadObjectList();				
		echo "\t\t\t<li><a href=\"index.php\" role=\"button\"><i class=\"glyphicon glyphicon-home\"></i>\n&nbsp;Inicio</a></li>\n";
		for($i = 0; $i < count($rows) ; $i++) {
			$men_name = $rows[$i]->men_name;
			if(defined($rows[$i]->men_desc)) {
				$men_name = constant($rows[$i]->men_desc);
			}
			if($rows[$i]->men_type == "SUB") {
				$temp = MenuSubNivel($rows[$i]->men_id,"            ");
				if(!empty($temp)) {
					$target = "dropdownMenu" . $rows[$i]->men_id;
					echo "\t\t\t<li class=\"dropdown\">\n";
					echo "\t\t\t\t<a id=\"" . $target . "\" data-toggle=\"dropdown\" aria-expanded=\"false\" class=\"dropdown-toggle\" aria-haspopup=\"true\" role=\"button\">\n";
					if(!empty($rows[$i]->men_icon)) {
						echo "<i class=\"glyphicon " . $rows[$i]->men_icon . "\"></i>\n";
					}
					echo "&nbsp;" . $men_name;
					echo "\t\t\t\t\t<span class=\"caret\"></span>\n";
					echo "\t\t\t\t</a>\n";
					echo "\t\t\t\t\t<ul aria-labelledby=\"" . $target . "\" class=\"dropdown-menu\" role=\"menu\">\n" . $temp . "</ul>\n"; 
					echo "\t\t\t</li>\n";
				}
			} else {
				echo "\t\t\t<li><a href=\"" . $rows[$i]->men_accion . "\"  " . (!empty($rows[$i]->men_target) ? "target=\"" .$rows[$i]->men_target ."\"": "" )  . ">\n";
				if(!empty($rows[$i]->men_icon)) {
					echo "<i class=\"glyphicon " . $rows[$i]->men_icon . "\"></i>\n";
				}
				echo "&nbsp;" . $men_name . "</a></li>\n";
			}
		}
		echo "\t\t\t<li><a href=\"close.php\"><i class=\"glyphicon glyphicon-off\"></i>\n&nbsp;Salir</a></li>\n";
		echo "\t\t</ul>\n";
		echo "\t</div>\n";
	}
?>