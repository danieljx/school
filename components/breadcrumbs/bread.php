<?php
	function BreadSubNivel($id, $type, $views, $sep = "\t") {
		$temp	= "";
		$mod  	= JFactory::getModulo();
		$db		= JFactory::getDBO();
		$user	= JFactory::getUser();
		$rol	= $user->getRolId();
		$view 	= JRequest::getVar("vista");
		$sql = "Select b.bread_id\n";
		$sql.= "	  ,b.mod_id\n";
		$sql.= "	  ,b.bread_type\n";
		$sql.= "	  ,b.bread_view\n";
		$sql.= "	  ,b.bread_title\n";
		$sql.= "	  ,b.bread_accion\n";
		$sql.= "	  ,b.bread_father\n";
		$sql.= "	  ,b.bread_icon\n";
		$sql .= "From sys_breadcumbs b\n";
		$sql .= "Left Join sys_module m on m.mod_id = b.mod_id\n";
		$sql .= "Where b.bread_id is not null\n";
		$sql .= "  and b.bread_father = '" . $id . "'\n";
		if($type == "PR") {
			$sql .= "  and b.mod_id = '" . $mod->getModId() . "'\n";
		} else if($type == "MOD") {
			$sql .= "  and b.bread_view = '" . $view . "'\n";
		}
		$db->setQuery( $sql );
		$rows = $db->loadObjectList();
		$arrow = true;
		for($i = 0; $i < count($rows) ; $i++) {
			$temp .= $sep . "<li>\n";
			$temp .= "<a class=\"active\" href=\"" . $rows[$i]->bread_accion . "\">\n";
			if(!empty($rows[$i]->bread_icon)) {
				$temp .= "<i class=\"glyphicon " . $rows[$i]->bread_icon . "\"></i>\n";
			}
			$temp .= $rows[$i]->bread_title . "</a>\n";
			$temp .= $sep . "</li>\n";
			$temp2 = BreadSubNivel($rows[$i]->bread_id, $rows[$i]->bread_type, $rows[$i]->bread_view, $sep . "\t");
			if(!empty($temp2)) {
				$temp .= $temp2 . $sep;
			}
		}
		return $temp;
	}
	$db   = JFactory::getDBO();
	$mod  = JFactory::getModulo();
	$user = JFactory::getUser();
	$view = JRequest::getVar("vista");
	$rol  = $user->getRolId();
	if($user->getId() != -1) {
		$sql = "Select b.bread_id\n";
		$sql.= "	  ,b.mod_id\n";
		$sql.= "	  ,b.bread_type\n";
		$sql.= "	  ,b.bread_view\n";
		$sql.= "	  ,b.bread_title\n";
		$sql.= "	  ,b.bread_accion\n";
		$sql.= "	  ,b.bread_father\n";
		$sql.= "	  ,b.bread_icon\n";
		$sql .= "From sys_breadcumbs b\n";
		$sql .= "Where b.bread_id = 1\n";
		$db->setQuery( $sql );
		$rows = $db->loadObjectList();
		for($i = 0; $i < count($rows) ; $i++) {
			echo "<ol class=\"breadcrumb\">\n";
			echo "<li>\n";
			echo "<a class=\"active\" href=\"" . $rows[$i]->bread_accion . "\">\n";
			if(!empty($rows[$i]->bread_icon)) {
				echo "<i class=\"glyphicon " . $rows[$i]->bread_icon . "\"></i>\n";
			}
			echo $rows[$i]->bread_title . "</a>\n";
			echo "</li>\n";
			echo BreadSubNivel($rows[$i]->bread_id, $rows[$i]->bread_type, $rows[$i]->bread_view, "\t") . "\n";
			echo "</ol>\n";
		}
	}