<?php
	$target = JFunc::FTarget();
	$mod	= JFactory::getModulo();
	$db		= JFactory::getDBO();
	$user	= JFactory::getUser();
    if(!empty($back)) {
        $back	= json_decode($back,true);
    } else {
        $back	= json_decode("[]",true);
    }
    $back["view"]		= "list";
	$enroll = new JEnrolls($back);
?>
<script>
	objDataBack   = <?php echo json_encode($back); ?>;
	arrayDataList = <?php echo json_encode($enroll->getenrolls()); ?>;
</script>
<h2 class="page-header">Inscripciones</h2>
<div class="panel panel-info">
	<div class="panel-heading">
		<h3 id="panel-title" class="panel-title">Filtro de Inscripciones</h3>
	</div>
	<div class="panel-body">
		<form id="enrollsFilter" data-module="enroll" data-vista="enrollsData" class="form-horizontal formList" method="POST" target="<?PHP echo $target; ?>">
			<div class="form-group">
				<div class="col-xs-4">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
						<input type="text" class="form-control" placeholder="Nombre del Estudiante" id="enroll_name" name="enroll_name">
					</div>
				</div>
				<div class="col-xs-4">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
						<input type="text" class="form-control" placeholder="ejemplo@ejemplo.com" id="enroll_email" name="enroll_email">
					</div>
				</div>
				<div class="col-xs-4">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-tasks"></span></span>
						<select class="form-control selectpicker" title='Selecione Periodo' name="period_id" id="period_id" multiple>
							<?php
							$sql = "Select p.period_id\n";
							$sql.= "	  ,p.period_code\n";
                            $sql.= "      ,concat(p.period_code,': ' ,DATE_FORMAT(p.period_start,'%d/%m/%Y'),' - ',DATE_FORMAT(p.period_end,'%d/%m/%Y')) as period_name\n";
							$sql.= "From mod_period p\n";
							$sql.= "Where p.period_id is not null\n";
							$db->setQuery( $sql );
							$rows = $db->loadObjectList();
							for($i=0; $i < count($rows); $i++) {
								echo "<option value=\"" . $rows[$i]->period_id . "\" >" .  $rows[$i]->period_name . "</option>\n";
							}
							?>
						</select>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="container-fluid">
					<div class="btn-group">
						<button type="submit" class="btn btn-primary">
							<i class="glyphicon glyphicon-filter"></i>
							Filtrar
						</button>
						<button type="reset" class="btn btn-warning">
							<i class="glyphicon glyphicon-erase"></i>
							Limpiar
						</button>
						<?php if($mod->getInsert()) { ?>
						<button id="newEnrolls" type="button" class="btn btn-info">
							<i class="glyphicon glyphicon-edit"></i>
							Nueva Inscripcion
						</button>
						<?php } ?>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<div class="container-fluid">
	<div class="table-responsive">
		<table id="enrollsTabla" class="table dataTableList table-bordered" data-update="0" data-delete="0">
			<thead>
				<tr>
					<th data-visible="false" data-type="num" data-class="enroll_id" data-bSearchable="false" data-bSortable="false">#</th>
					<th data-type="string" data-class="third_names" data-bSearchable="true" data-bSortable="true">Estudiantes</th>
                    <th data-type="string" data-class="third_rif" data-bSearchable="true" data-bSortable="true">CI/RIF</th>
                    <th data-type="string" data-class="period_code" data-bSearchable="true" data-bSortable="true">Periodo</th>
					<th data-type="string" data-class="level_name" data-bSearchable="true" data-bSortable="true">Nivel</th>
					<th data-type="string" data-class="date_start" data-bSearchable="true" data-bSortable="true">Fecha</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
</div>