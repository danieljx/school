<?php
	$mod  = JFactory::getModulo();
	$db   = JFactory::getDBO();
	$datos = json_decode(JRequest::getVar("datos"),true);
	
	$sql = "Select p.period_id\n";
	$sql.= "      ,p.period_code\n";
	$sql.= "      ,p.period_sta\n";
	$sql.= "      ,DATE_FORMAT(p.period_start,'%d/%m/%Y') as period_start\n";
	$sql.= "      ,DATE_FORMAT(p.period_end,'%d/%m/%Y') as period_end\n";
	$sql.= "      ,DATE_FORMAT(p.user_fec,'%d/%m/%Y %H:%i:%s') as user_fec\n";
	$sql.= "      ,concat(p.period_code,': ' ,DATE_FORMAT(p.period_start,'%d/%m/%Y'),' - ',DATE_FORMAT(p.period_end,'%d/%m/%Y')) as period_name\n";
	$sql.= "      ,p.user_id\n";
	$sql.= "      ,if(p.period_sta = 1,'Activo','Culminado') as period_status\n";
	$sql.= "      ,if(e.enroll_id is null,'No Inscrito','Inscrito') as enroll_status\n";
	$sql.= "      ,if(p.period_sta = 1,'info',if(e.enroll_id is not null,'success','danger')) as color_status\n";
	$sql.= "      ,if(e.enroll_id is null, 'NO','YES') as enroll\n";
	$sql.= "From mod_period p\n";
	$sql.= "Left Join mod_enroll e on e.period_id = p.period_id and e.third_id = '" . $datos["third_id"] . "'\n";
	$sql.= "Where p.period_sta = 1\n";
	$sql.= "Order by p.period_start desc\n";
	$db->setQuery( $sql );
	$rows = $db->loadObjectList();
	$arrayAccion = Array();
	for($i = 0; $i < count($rows) ; $i++) {
		echo "<div class=\"panel panel-" . $rows[$i]->color_status . "\">\n";
		echo "	<div id=\"collapseAccionModule_" . $rows[$i]->period_id . "\">\n";
		echo "		<div class=\"panel-body\">\n";
		echo "		    <div class=\"panel panel-default\">\n";
		echo "		        <div class=\"panel-body\">\n";
		echo "		            <div class=\"container-fluid\">\n";
		echo "		                <div class=\"btn-group\">\n";
		if($rows[$i]->period_sta == "1" && $rows[$i]->enroll == "NO") {
			echo "		                    <button type=\"button\" id=\"addEnroll\" third_id=\"" . $datos["third_id"] . "\" period_id=\"" . $rows[$i]->period_id . "\" class=\"btn btn-primary\">\n";
			echo "		                        <i class=\"glyphicon glyphicon-book\"></i>\n";
			echo "		                        Inscribir\n";
			echo "		                    </button>\n";
			echo "		                    <button type=\"button\" id=\"addEnrollRight\" style=\"height:34px;\" class=\"btn btn-primary dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n";
			echo "		                        <span class=\"caret\"></span>\n";
			echo "		                        <span class=\"sr-only\">Toggle Dropdown</span>\n";
			echo "		                    </button>\n";
			echo "		                    <ul class=\"dropdown-menu\">\n";
			$sqll = "Select l.level_id\n";
			$sqll.= "	   ,l.level_name\n";
			$sqll.= "	   ,group_concat(concat(m.matter_id,'@Z@',m.matter_name,'@Z@',m.matter_code) separator '@X@') as matters\n";
			$sqll.= "From mod_level l\n";
			$sqll.= "Inner Join mod_matter m on m.matter_level = l.level_id\n";
			$sqll.= "Where l.level_type = 2\n";
			$sqll.= "  and l.level_id not in (Select x.level_id\n";
			$sqll.= "                         From mod_enroll x\n";
			$sqll.= "                         Where x.period_id = '" . $rows[$i]->period_id . "'\n";
			$sqll.= "                           and x.third_id = '" . $datos["third_id"] . "')\n";
			$sqll.= "Group by l.level_id\n";
			$db->setQuery( $sqll );
			$rowl = $db->loadObjectList();
			for($l=0; $l < count($rowl); $l++) {
				if(!empty($rowl[$l]->matters)) {
					$matters = explode("@X@",$rowl[$l]->matters);
					$arrayAccion = array();
					for($m = 0; $m < count($matters); $m++) {
						$mattersItem = explode("@Z@",$matters[$m]);
						$arrayAccionItem = array();
						$arrayAccionItem["matter_id"]   = $mattersItem[0];
						$arrayAccionItem["matter_name"] = $mattersItem[1];
						$arrayAccionItem["matter_code"] = $mattersItem[2];
						array_push($arrayAccion, $arrayAccionItem);
					}
					echo "<script>var matters_" . $rowl[$l]->level_id . " = " . json_encode($arrayAccion) . ";</script>";
				}
				echo "		                    <li><a third_id=\"" . $datos["third_id"] . "\" period_id=\"" . $rows[$i]->period_id . "\" period_name=\"" . $rows[$i]->period_name . "\"  level_id=\"" . $rowl[$l]->level_id . "\" onclick=\"enrollThird(\$(this))\">" .  $rowl[$l]->level_name . "</a></li>\n";
			}
			echo "		                    </ul>\n";
		}
		if($rows[$i]->period_sta == "1") {
			echo "<input type=\"hidden\" id=\"period_id\" name=\"period_id\" value=\"" . $rows[$i]->period_id . "\">\n";
			echo "<input type=\"hidden\" id=\"period_sta\" name=\"period_sta\" value=\"" . $rows[$i]->period_sta . "\">\n";
		}
		echo "		                </div>\n";
		echo "		            </div>\n";
		echo "		        </div>\n";
		echo "		   </div>\n";
		echo "		   <div id=\"periodMatterContent_" . $rows[$i]->period_id . "\">\n";
		echo "		   <table id=\"periodMatter_" . $rows[$i]->period_id . "\" class=\"table table-bordered\">\n";
		echo "		       <thead>\n";
		echo "		           <tr>\n";
		echo "		               <th>Nivel</th>\n";
		echo "		               <th>Materias</th>\n";
		echo "		               <th>Seccion</th>\n";
		echo "		           </tr>\n";
		echo "		       </thead>\n";
		echo "		      <tbody>\n";
		$sqm = "Select tm.item_id\n";
		$sqm.= "      ,tm.third_id\n";
		$sqm.= "      ,tm.period_id\n";
		$sqm.= "      ,tm.sec_id\n";
		$sqm.= "      ,tm.matter_id\n";
		$sqm.= "      ,tm.item_sta\n";
		$sqm.= "      ,tm.item_note\n";
		$sqm.= "      ,DATE_FORMAT(tm.item_start,'%d/%m/%Y') as item_start\n";
		$sqm.= "      ,DATE_FORMAT(tm.item_end,'%d/%m/%Y') as item_end\n";
		$sqm.= "      ,DATE_FORMAT(tm.user_fec,'%d/%m/%Y %H:%i:%s') as user_fec\n";
		$sqm.= "      ,tm.user_id\n";
		$sqm.= "      ,l.level_id\n";
		$sqm.= "      ,l.level_name\n";
		$sqm.= "      ,m.matter_name\n";
		$sqm.= "      ,s.sec_code\n";
		$sqm.= "      ,n.note_class\n";
		$sqm.= "      ,s.sec_code\n";
		$sqm.= "      ,pm.item_id as teacher_third\n";
		$sqm.= "      ,ifnull(a.tassits,0) as tassits\n";
		$sqm.= "      ,ifnull(pm.item_assistance,0) as passits\n";
		$sqm.= "      ,FLOOR(ifnull((ifnull(a.tassits,0)*100)/ifnull(pm.item_assistance,0),0)) as assist\n";
		$sqm.= "From mod_third_matter tm\n";
		$sqm.= "Inner Join mod_matter m on m.matter_id = tm.matter_id\n";
		$sqm.= "Inner Join mod_level l on l.level_id = m.matter_level\n";
		$sqm.= "Left Join mod_section s on s.sec_id = tm.sec_id\n";
		$sqm.= "Left Join mod_note n on n.note_code = tm.item_note\n";
		$sqm.= "Left Join mod_teacher_matter pm on pm.matter_id = tm.matter_id and pm.sec_id = tm.sec_id\n";
		$sqm.= "Left Join (Select  xt.matter_third\n";
		$sqm.= "       			  ,sum(ifnull(xt.court_assists,0)) as tassits\n";
		$sqm.= "       	   From mod_third_matter_court xt\n";
		$sqm.= "       	   Where xt.matter_third is not null\n";
		$sqm.= "       	   Group by xt.matter_third) a on a.matter_third = tm.item_id\n";
		$sqm.= "Where tm.period_id = '" .$rows[$i]->period_id . "'\n";
		$sqm.= "  and tm.third_id = '" . $datos["third_id"] . "'\n";
		$sqm.= "Order by tm.item_end desc\n";
		//echo $sqm;
		$db->setQuery( $sqm );
		$rowm = $db->loadObjectList();
		for($m = 0; $m < count($rowm) ; $m++) {
			echo "		      <tr item=\"" . $rowm[$m]->item_id . "\">\n";
			echo "		      <td ref=\"level\">" . $rowm[$m]->level_name . "</td>\n";
			echo "		      <td ref=\"matter\">" . $rowm[$m]->matter_name . "</td>\n";
			echo "		      <td ref=\"section\">\n";
			echo "		        <div class=\"btn-group\" style=\"z-index: " . (count($rowm)-$m) . ";\">\n";
			if($rows[$i]->period_sta == "1") {
				echo "              <select class=\"form-control col-xs-12 selectpicker\" title=\"Selecione Sección\" item_matter=\"" . $rowm[$m]->item_id . "\" name=\"sec_id\" id=\"sec_id_" .$rows[$i]->period_id . "\">\n";
				$sqs = "Select s.sec_id\n";
				$sqs.= "      ,s.sec_code\n";
				$sqs.= "From mod_section s\n";
				$sqs.= "Where s.sec_sta = '1'\n";
				$sqs.= "  and s.sec_level = '" . $rowm[$m]->level_id . "'\n";
				$db->setQuery( $sqs );
				$rowsc = $db->loadObjectList();
				for($sc = 0; $sc < count($rowsc) ; $sc++) {
					echo "              <option value=\"" . $rowsc[$sc]->sec_id . "\" " . ($rowsc[$sc]->sec_id == $rowm[$m]->sec_id?"selected":"") . " data-content=\"<kbd>" . $rowsc[$sc]->sec_code . "</kbd>\">" . $rowsc[$sc]->sec_code . "</option>\n";
				}
				echo "              </select>\n";
			} else {
				echo "		      <kbd>" . $rowm[$m]->sec_code . "</kbd>\n";
			}
			echo "		      	</div>\n";
			echo "		      </td>\n";
			echo "		      </tr>\n";
		}
		echo "		      </tbody>\n";
		echo "		   </table>\n";
		echo "		   </div>\n";
		echo "		</div>\n";
		echo "	</div>\n";
		echo "</div>\n";
	}