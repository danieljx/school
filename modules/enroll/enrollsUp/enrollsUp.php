<?php
	$datos  = json_decode(JRequest::getVar("datos"),true);
    if(empty($datos["third_id"])) {
        $datos["third_type"] = "3";
        $third  = new JThirds($datos);
        $temp   = $third->setThirds();
        $datos["third_id"] = $temp["id"];
    }
    $enroll = new JEnrolls($datos);
    echo(json_encode($enroll->setEnrolls()));
