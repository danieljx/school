<?php
$target	= JFunc::FTarget();
$mod	= JFactory::getModulo();    
$db		= JFactory::getDBO();
$user	= JFactory::getUser();
$datos	= json_decode(JRequest::getVar("datos"),true);
$title	= JRequest::getVar("title");
$pdf 	= new PDFList();
$pdf->setTitle($title);
$pdf->setInicio();
$pdf->SetXY(10, 45);
$pdf->SetTextColor(0, 0, 10);
$pdf->SetFont('helvetica', 'B', 8);
$pdf->Cell(40, 5, html_entity_decode("Nombre"), 1, 0, 'C');
$pdf->Cell(30, 5, html_entity_decode("Codigo"), 1, 0, 'C');
$pdf->Cell(20, 5, html_entity_decode("Estatus"), 1, 0, 'C');
$pdf->Cell(20, 5, html_entity_decode("Nivel"), 1, 0, 'C');
$pdf->SetXY(10,50);
for($i = 0; $i < count($datos); $i++) {
	$pdf->SetX(10);
	$pdf->SetFont('helvetica', '', 8);
	$CebColor = array();
	$CebColor[0] = (($i%2==0)?"255":"225");
	$CebColor[1] = (($i%2==0)?"255":"225");
	$CebColor[2] = (($i%2==0)?"255":"225");
	$pdf->SetFillColorArray($CebColor);
	$pdf->Cell(40, 5, html_entity_decode($datos[$i]["matter_name"]), 1, 0, 'C', true, '', 1);
	$pdf->Cell(30, 5, html_entity_decode($datos[$i]["matter_code"]), 1, 0, 'C', true, '', 1);
	$StaColor = array();
	if($datos[$i]["matter_sta"] == "1") {
		$StaColor[0] = (($i%2==0)?"162":"197");
		$StaColor[1] = (($i%2==0)?"232":"232");
		$StaColor[2] = (($i%2==0)?"162":"197");
	} else {
		$StaColor[0] = (($i%2==0)?"240":"240");
		$StaColor[1] = (($i%2==0)?"168":"204");
		$StaColor[2] = (($i%2==0)?"168":"204");
	}
	$pdf->SetFillColorArray($StaColor);
	$pdf->Cell(20, 5, html_entity_decode($datos[$i]["matter_status"]), 1, 0, 'C', true, '', 1);
	$pdf->SetFillColorArray($CebColor);
	$pdf->SetFont('helvetica', 'B', 8);
	$pdf->Cell(20, 5, html_entity_decode($datos[$i]["level_name"]), 1, 0, 'C', true, '', 1);
	$pdf->Ln();
}
$pdf->Output('Listado-'. date("d-m-Y-H-i-s"), 'I');