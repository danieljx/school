<?php
class JEnrolls {
    private $enroll_id			= null;
    private $third_id			= null;
    private $period_id  		= null;
    private $level_id			= null;
    private $date_start			= null;
    private $user_id			= null;
    private $user_fec			= null;
    private $accion				= null;
    private $view				= "list";

    function __construct($datos = array()) {
        $this->requestEnrolls($datos);
    }
    public function getDisable($tipo = "campo") {
        $mod  = JFactory::getModulo();
        if($this->accion == "add" && !$mod->getInsert()) {
            return "disabled";
        } else if($this->accion == "upd" && !$mod->getUpdate()) {
            return "disabled";
        } else if($this->accion == "del" && $tipo=="campo") {
            return "disabled";
        } else if($this->accion == "del" && !$mod->getDelete()) {
            return "disabled";
        }
        return "";
    }
    public function getClassAtribute() {
        return get_object_vars($this);
    }
    public function requestEnrolls($datos = array()) {
        if(count($datos) > 0) {
            foreach($datos as $key => $value) {
                if(array_key_exists($key,$this->getClassAtribute())) {
                    $this->$key =  $value;
                }
            }
        }
    }
    public function setNoteEnrolls($datos = array()) {
        $user = JFactory::getUser();
        $mod  = JFactory::getModulo();
        $db   = JFactory::getDBO();
        $temp = array("status" => "ERROR", "type" => "alert-danger", "text" => "NO TIENE ACCESO", "id" => "");
        if($mod->getUpdate()) {
            $obj 	= new stdClass();
            if(count($datos) > 0) {
                foreach($datos as $key => $value) {
                    if($key != "accion" && $key != "view") {
                        $obj->$key =  $value;
                    }
                }
            }
            $obj->user_id	= $user->getId();
            $obj->user_fec  = 'NOW_AHORA';
            if(!$db->updateObject('mod_third_matter', $obj,"item_id")) {
                $temp["id"]  	= "";
                $temp["status"]	= "ERROR";
                $temp["text"]  	= $db->getErrorMsg();
                $temp["type"]  	= "alert-danger";
            } else {
                $temp["id"]  	= $obj->item_id;
                $temp["status"]	= "OK";
                $temp["text"]  	= "Operacion Satisfactoria";
                $temp["type"]  	= "alert-info";
            }
        }
        return $temp;
    }
    public function setCourtEnrolls($datos = array()) {
        $user = JFactory::getUser();
        $mod  = JFactory::getModulo();
        $db   = JFactory::getDBO();
        $temp = array("status" => "ERROR", "type" => "alert-danger", "text" => "NO TIENE ACCESO", "id" => "");
        if($mod->getUpdate()) {
            $obj 	= new stdClass();
            if(count($datos) > 0) {
                foreach($datos as $key => $value) {
                    if($key != "accion" && $key != "view") {
                        $obj->$key =  $value;
                        if($key == "court_start" || $key == "court_end") {
                            $obj->$key  = "DATE_FORMAT" .(empty($value)   ? "" : $value);
                        }
                    }
                }
            }
            $obj->user_id	= $user->getId();
            $obj->user_fec  = 'NOW_AHORA';
            if(!$db->updateObject('mod_third_matter_court', $obj,"court_id")) {
                $temp["id"]  	= "";
                $temp["status"]	= "ERROR";
                $temp["text"]  	= $db->getErrorMsg();
                $temp["type"]  	= "alert-danger";
            } else {
                $temp["id"]  	= $obj->court_id;
                $temp["status"]	= "OK";
                $temp["text"]  	= "Operacion Satisfactoria";
                $temp["type"]  	= "alert-info";
            }
        }
        return $temp;
    }
    public function setEnrolls() {
        $user = JFactory::getUser();
        $mod  = JFactory::getModulo();
        $db   = JFactory::getDBO();
        $temp = array("status" => "ERROR", "type" => "alert-danger", "text" => "NO TIENE ACCESO", "id" => "");
        if($this->accion != "del") {
            $obj 	= new stdClass();
            $datos	= $this->getClassAtribute();
            if(count($datos) > 0) {
                foreach($datos as $key => $value) {
                    if($key != "accion" && $key != "view") {
                        $obj->$key =  $value;
                    }
                }
            }
            $obj->user_id	= $user->getId();
            $obj->user_fec  = 'NOW_AHORA';
            if($this->accion == "add" && $mod->getInsert()) {
                $obj->date_start  = 'NOW_AHORA';
                if(!$db->insertObject('mod_enroll', $obj,'enroll_id')) {
                    $temp["id"]  	= "";
                    $temp["status"]	= "ERROR";
                    $temp["text"]  	= $db->getErrorMsg();
                    $temp["type"]  	= "alert-danger";
                } else {
                    $this->enroll_id    = $obj->enroll_id;
                    $temp["id"]		    = $obj->enroll_id;
                    $temp["status"]	    = "OK";
                    $temp["text"]  	    = "Operacion Satisfactoria";
                    $temp["type"]  	    = "alert-info";
                }
                if($temp["status"]	!= "ERROR") {
                    $sql  = "Insert into mod_third_matter (third_id,period_id,matter_id,item_sta,item_start,user_id,user_fec)\n";
                    $sql .= "Select '" . $this->third_id . "', '" . $this->period_id . "', m.matter_id, 1, now(), '" . $user->getId() . "', now()\n";
                    $sql .= "From mod_matter m\n";
                    $sql .= "Where  m.matter_level = '" . $this->level_id . "'\n";
                    if(!$db->Execute($sql)) {
                        $temp["id"]  	= $obj->enroll_id;
                        $temp["status"]	= "ERROR";
                        $temp["text"]  	= $db->getErrorMsg();
                        $temp["type"]  	= "alert-danger";
                    } else {
                        $temp["id"]  	= $obj->enroll_id;
                        $temp["status"]	= "OK";
                        $temp["text"]  	= "Operacion Satisfactoria";
                        $temp["type"]  	= "alert-info";
                    }
                    if($temp["status"]	!= "ERROR") {
                        $sql  = "Select i.item_id as matter_third\n";
                        $sql .= "From mod_third_matter i\n";
                        $sql .= "Where  i.third_id = '" . $this->third_id . "'\n";
                        $sql .= "   and i.period_id = '" . $this->period_id . "'\n";
                        $db->setQuery($sql);
                        $rows = $db->loadObjectList();
                        if(count($rows) > 0) {
                            for($i=0; $i < count($rows); $i++) {
                                $sqlm  = "Insert into mod_third_matter_court (court_period,court_assists,matter_third,court_sta,court_start,user_id,user_fec)\n";
                                $sqlm .= "       value (1,0,'" . $rows[$i]->matter_third . "',1,now(),'" . $user->getId() . "',now())\n";
                                $db->Execute($sqlm);
                                $sqlm  = "Insert into mod_third_matter_court (court_period,court_assists,matter_third,court_sta,court_start,user_id,user_fec)\n";
                                $sqlm .= "       value (2,0,'" . $rows[$i]->matter_third . "',1,now(),'" . $user->getId() . "',now())\n";
                                $db->Execute($sqlm);
                                $sqlm  = "Insert into mod_third_matter_court (court_period,court_assists,matter_third,court_sta,court_start,user_id,user_fec)\n";
                                $sqlm .= "       value (3,0,'" . $rows[$i]->matter_third . "',1,now(),'" . $user->getId() . "',now())\n";
                                if(!$db->Execute($sqlm)) {
                                    $temp["id"]  	= $obj->enroll_id;
                                    $temp["status"]	= "ERROR";
                                    $temp["text"]  	= $db->getErrorMsg();
                                    $temp["type"]  	= "alert-danger";
                                } else {
                                    $temp["id"]  	= $obj->enroll_id;
                                    $temp["status"]	= "OK";
                                    $temp["text"]  	= "Operacion Satisfactoria";
                                    $temp["type"]  	= "alert-info";
                                }
                            }
                        }
                    }
                }
            } else if($mod->getUpdate()) {
                $obj->third_id   = (int)$this->third_id;
                if(!$db->updateObject('mod_enroll', $obj,"enroll_id")) {
                    $temp["id"]  	= "";
                    $temp["text"]  	= $db->getErrorMsg();
                } else {
                    $temp["id"]  	= $obj->enroll_id;
                    $temp["status"]	= "OK";
                    $temp["text"]  	= "Operacion Satisfactoria";
                    $temp["type"]  	= "alert-info";
                }
            }
        } else {
            $sql = "DELETE FROM mod_enroll WHERE enroll_id = " . $this->enroll_id;
            if(!$db->Execute($sql)) {
                $temp["id"]  	= "";
                $temp["text"]  	= $db->getErrorMsg();
            } else {
                $temp["status"]	= "DELETE";
                $temp["text"]  	= "Operacion Satisfactoria";
                $temp["type"]  	= "alert-info";
            }
        }
        return  $temp;
    }
    public function getEnrolls() {
        $user = JFactory::getUser();
        $sql = "Select e.enroll_id\n";
        $sql.= "      ,e.third_id\n";
        $sql.= "      ,e.period_id\n";
        $sql.= "      ,e.level_id\n";
        $sql.= "      ,DATE_FORMAT(e.date_start,'%d/%m/%Y') as date_start\n";
        $sql.= "      ,DATE_FORMAT(e.user_fec,'%d/%m/%Y %H:%i:%s') as user_fec\n";
        $sql.= "      ,concat(ifnull(t.third_name,''),' ',ifnull(t.third_lastname,'')) as third_names\n";
        $sql.= "      ,t.third_rif\n";
        $sql.= "      ,l.level_name\n";
        $sql.= "      ,p.period_code\n";
        $sql.= "From mod_enroll e\n";
        $sql.= "Inner Join mod_third t on t.third_id = e.third_id\n";
        $sql.= "Inner Join mod_level l on l.level_id = e.level_id\n";
        $sql.= "Inner Join mod_period p on p.period_id = e.period_id\n";
        $sql.= "Where e.enroll_id is not null\n";
        if(!is_null($this->enroll_id) && !empty($this->enroll_id)) {
            $sql.= "and e.enroll_id = " . $this->third_id . "\n";
        }
        if(!is_null($this->third_id) && !empty($this->third_id)) {
            $sql.= "and t.third_id = " . $this->third_id . "\n";
        }
        // echo $sql;
        $db  = JFactory::getDBO();
        $db->setQuery($sql);
        $rows = $db->loadObjectList();
        $arrayDatos = array();
        if(count($rows) > 0 && $this->accion != "add") {
            for($i=0; $i < count($rows); $i++) {
                $itemArray = array();
                $iT = 0;
                foreach($rows[$i] as $key => $value) {
                    $itemArray[$iT] 	= $value;
                    $itemArray[$key] 	= $value;
                    $iT++;
                }
                array_push($arrayDatos, $itemArray);
            }
        }
        return $arrayDatos;
    }
}
class JThirds {
    private $third_id			= null;
    private $third_name			= null;
    private $third_lastname		= null;
    private $third_rif			= null;
    private $third_type			= null;
    private $third_father		= null;
    private $third_nationallity	= null;
    private $third_website		= null;
    private $third_description	= null;
    private $third_phone		= null;
    private $third_address		= null;
    private $third_facebook		= null;
    private $third_twitter		= null;
    private $third_email		= null;
    private $third_path			= null;
    private $third_img			= null;
    private $third_sta			= null;
    private $third_birth		= null;
    private $third_sex			= null;
    private $date_start			= null;
    private $user_id			= null;
    private $user_fec			= null;
    private $third_son			= null;
    private $third_rel			= null;
    private $third_oneRif		= null;
    private $accion				= null;
    private $view				= "list";
    
    function __construct($datos = array()) {
		$this->requestThirds($datos);
	}
    public function getDisable($tipo = "campo") {
    	$mod  = JFactory::getModulo();
    	if($this->accion == "add" && !$mod->getInsert()) {
			return "disabled";
		} else if($this->accion == "upd" && !$mod->getUpdate()) {
			return "disabled";
		} else if($this->accion == "del" && $tipo=="campo") {
			return "disabled";
		} else if($this->accion == "del" && !$mod->getDelete()) {
			return "disabled";
		}
    	return "";
	}
	public function getClassAtribute() {
        return get_object_vars($this);
    }
    public function requestThirds($datos = array()) {
		if(count($datos) > 0) {
			foreach($datos as $key => $value) {
                if(array_key_exists($key,$this->getClassAtribute())) {
                    $this->$key =  $value;
                }
			}
		}
    }
	public function setImageProfile() {
		return false;
	}
    public function setThirds() {
    	$user = JFactory::getUser();
    	$mod  = JFactory::getModulo();
    	$db   = JFactory::getDBO();
		$temp = array("status" => "ERROR", "type" => "alert-danger", "text" => "NO TIENE ACCESO", "id" => "");
    	if($this->accion != "del") {
			$obj 	= new stdClass();
			$datos	= $this->getClassAtribute();
			if(count($datos) > 0) {
				foreach($datos as $key => $value) {
					if($key != "accion" && $key != "view" && $key != "third_birth" && $key != "third_son" && $key != "third_rel" && $key != "third_oneRif") {
						$obj->$key = $value;
					}
				}
			}
			$obj->user_id	= $user->getId();
			$obj->user_fec  = 'NOW_AHORA';
            $obj->third_birth  = "DATE_FORMAT" .(empty($this->third_birth)   ? "" : $this->third_birth);
    	  	if($this->accion == "add" && $mod->getInsert()) {
				$obj->date_start  = 'NOW_AHORA';
				if(!$db->insertObject('mod_third', $obj,'third_id')) {
                    $temp["status"]	= "ERROR";
                    $temp["text"]  	= $db->getErrorMsg();
                    $temp["type"]  	= "alert-danger";
				} else {
					$this->third_id = $obj->third_id;
					$temp["id"]		= $obj->third_id;
					$temp["status"]	= "OK";
					$temp["text"]  	= "Operacion Satisfactoria";
					$temp["type"]  	= "alert-info";
				}
                if(!is_null($this->third_son) && !is_null($this->third_rel) && $temp["status"] != "ERROR") {
                    $sqlm  = "Insert into mod_third_father (third_id,father_id,rel_type,user_id,user_fec)\n";
                    $sqlm .= "       value ('" . $this->third_son . "','" . $this->third_id . "','" . $this->third_rel . "','" . $user->getId() . "',now())\n";
                    if(!$db->Execute($sqlm)) {
                        $temp["id"]  	= $obj->third_id;
                        $temp["status"]	= "ERROR";
                        $temp["text"]  	= $db->getErrorMsg();
                        $temp["type"]  	= "alert-danger";
                    } else {
                        $temp["id"]  	= $obj->third_id;
                        $temp["status"]	= "OK";
                        $temp["text"]  	= "Operacion Satisfactoria";
                        $temp["type"]  	= "alert-info";
                    }
                }
			} else if($mod->getUpdate()) {
				$obj->third_id   = (int)$this->third_id;
				if(!$db->updateObject('mod_third', $obj,"third_id")) {
					$temp["id"]  	= "";
					$temp["text"]  	= $db->getErrorMsg();
				} else {
					$temp["id"]  	= $obj->third_id;
					$temp["status"]	= "OK";
					$temp["text"]  	= "Operacion Satisfactoria";
					$temp["type"]  	= "alert-info";
				}
			}
		} else {
    	  	$sql = "DELETE FROM mod_third WHERE third_id = " . $this->third_id;
    	  	if(!$db->Execute($sql)) {					
				$temp["id"]  	= "";
				$temp["text"]  	= $db->getErrorMsg();
			} else {
				$temp["status"]	= "DELETE";
				$temp["text"]  	= "Operacion Satisfactoria";
				$temp["type"]  	= "alert-info";
			}
		}
    	return  $temp;
    }
    public function getThirdRepresents($third_id) {
        $user = JFactory::getUser();
        $sql = "Select t.third_id\n";
        $sql.= "      ,t.third_name\n";
        $sql.= "      ,t.third_lastname\n";
        $sql.= "      ,concat(ifnull(t.third_name,''),' ',ifnull(t.third_lastname,'')) as third_names\n";
        $sql.= "      ,t.third_rif\n";
        $sql.= "      ,t.third_type\n";
        $sql.= "      ,t.third_email\n";
        $sql.= "      ,t.third_nationallity\n";
        $sql.= "      ,t.third_description\n";
        $sql.= "      ,t.third_phone\n";
        $sql.= "      ,t.third_address\n";
        $sql.= "      ,t.third_email\n";
		$sql.= "      ,t.third_sex\n";
		$sql.= "      ,DATE_FORMAT(t.third_birth,'%d/%m/%Y') as third_birth\n";
        $sql.= "      ,DATE_FORMAT(t.date_start,'%d/%m/%Y') as date_start\n";
        $sql.= "      ,DATE_FORMAT(t.user_fec,'%d/%m/%Y %H:%i:%s') as user_fec\n";
        $sql.= "      ,r.type_name as third_relName\n";
        $sql.= "      ,r.type_id as third_rel\n";
        $sql.= "      ,f.third_id as third_son\n";
        $sql.= "From mod_third_father f\n";
        $sql.= "Inner Join mod_third t on t.third_id = f.father_id\n";
        $sql.= "Inner Join mod_third_rel_type r on r.type_id = f.rel_type\n";
        $sql.= "Where t.third_id is not null\n";
        $sql.= "  and f.third_id = '" . $third_id . "'\n";
        $db  = JFactory::getDBO();
        $db->setQuery($sql);
        $rows = $db->loadObjectList();
        $arrayDatos = Array();
        if(count($rows) > 0) {
            for($i=0; $i < count($rows); $i++) {
                $iT = 0;
                $arrayItemDatos = Array();
                foreach($rows[$i] as $key => $value) {
                    $arrayItemDatos[$iT] 	= $value;
                    $arrayItemDatos[$key] 	= $value;
                    $iT++;
                }
                array_push($arrayDatos,$arrayItemDatos);
            }
        }
        return $arrayDatos;
    }
	public function getThirds() {
    	$user = JFactory::getUser();
		$sql = "Select t.third_id\n";
		$sql.= "      ,t.third_name\n";
		$sql.= "      ,t.third_lastname\n";
		$sql.= "      ,concat(ifnull(t.third_name,''),' ',ifnull(t.third_lastname,'')) as third_names\n";
		$sql.= "      ,t.third_rif\n";
		$sql.= "      ,t.third_type\n";
		$sql.= "      ,t.third_email\n";
		$sql.= "      ,t.third_father\n";
		$sql.= "      ,t.third_nationallity\n";
		$sql.= "      ,t.third_website\n";
		$sql.= "      ,t.third_description\n";
		$sql.= "      ,t.third_phone\n";
		$sql.= "      ,t.third_address\n";
		$sql.= "      ,t.third_facebook\n";
		$sql.= "      ,t.third_twitter\n";
		$sql.= "      ,t.third_email\n";
		$sql.= "      ,t.third_path\n";
		$sql.= "      ,t.third_img\n";
		$sql.= "      ,t.third_sta\n";
		$sql.= "      ,t.third_sex\n";
		$sql.= "      ,DATE_FORMAT(t.third_birth,'%d/%m/%Y') as third_birth\n";
		$sql.= "      ,DATE_FORMAT(t.date_start,'%d/%m/%Y') as date_start\n";
		$sql.= "      ,DATE_FORMAT(t.user_fec,'%d/%m/%Y %H:%i:%s') as user_fec\n";
		$sql.= "      ,s.sta_name as third_status\n";
		$sql.= "      ,tp.type_name as third_typeName\n";
		$sql.= "From mod_third t\n";
		$sql.= "Inner Join mod_third_status s on s.sta_id = t.third_sta\n";
		$sql.= "Inner Join mod_third_type tp on tp.type_id = t.third_type\n";
		$sql.= "Where t.third_id is not null\n";
		if(!is_null($this->third_id) && !empty($this->third_id)) {
			$sql.= "and t.third_id = " . $this->third_id . "\n";
		}
		if(!is_null($this->third_name) && !empty($this->third_name)) {
			$sql.= "and (t.third_name like '%" . $this->third_name . "%'\n";
			$sql.= "or t.third_lastname like '%" . $this->third_name . "%')\n";
		}
		if(!is_null($this->third_rif) && !empty($this->third_rif)) {
			$sql.= "and t.third_rif like '%" . $this->third_rif . "%'\n";
		}
		if(!is_null($this->third_email) && !empty($this->third_email)) {
			$sql.= "and t.third_email like '%" . $this->third_email . "%'\n";
		}
        if(!is_null($this->third_oneRif) && !empty($this->third_oneRif)) {
            $sql.= "and t.third_rif = '" . $this->third_oneRif . "'\n";
        }
		if(!is_null($this->third_type)) {
			if(is_array($this->third_type)) {
				$in = true;
				foreach($this->third_type as $key => $itemData) {
					if($key == "not in") {
						$sql.= "and t.third_type " . $key . " (" . implode($itemData,",") . ")\n";
						$in = false;
					}
				}
				if($in) {
					$sql.= "and t.third_type in (" . implode($this->third_type,",") . ")\n";
				}
			} else if(!empty($this->third_type)){
				$sql.= "and t.third_type = '" . $this->third_type . "'\n";
			}
		}
		if(!is_null($this->third_sta)) {
			if(is_array($this->third_sta)) {
				$sql.= "and t.third_sta in (" . implode($this->third_sta,",") . ")\n";
			} else if(!empty($this->third_sta)){
				$sql.= "and t.third_sta = '" . $this->third_sta . "'\n";
			}
		}
		$db  = JFactory::getDBO();
		$db->setQuery($sql);
		$rows = $db->loadObjectList();
		$arrayDatos = Array();
		if($this->view == "modify") {
			$arrayDatos = $this->getClassAtribute();
			if(count($rows) == 1 && $this->accion != "add") {
				for($i=0; $i < count($rows); $i++) {
					$iT = 0;
					foreach($rows[$i] as $key => $value) {
						$arrayDatos[$iT] 	= $value;
						$arrayDatos[$key] 	= $value;
						$iT++;
					}
				}
			}
		} else if($this->view == "list"){
			if(count($rows) > 0) {
				for($i=0; $i < count($rows); $i++) {
					$iT = 0;
					$arrayItemDatos = Array();
					foreach($rows[$i] as $key => $value) {
						$arrayItemDatos[$iT] 	= $value;
						$arrayItemDatos[$key] 	= $value;
						$iT++;
					}
					array_push($arrayDatos,$arrayItemDatos);
				}
			}
		}
		return $arrayDatos;
	}
}