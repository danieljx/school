<?php
	$mod  = JFactory::getModulo();
	$db   = JFactory::getDBO();
	$datos = json_decode(JRequest::getVar("datos"),true);
	
	$sql = "Select p.period_id\n";
	$sql.= "      ,p.period_code\n";
	$sql.= "      ,p.period_sta\n";
	$sql.= "      ,DATE_FORMAT(p.period_start,'%d/%m/%Y') as period_start\n";
	$sql.= "      ,DATE_FORMAT(p.period_end,'%d/%m/%Y') as period_end\n";
	$sql.= "      ,DATE_FORMAT(p.user_fec,'%d/%m/%Y %H:%i:%s') as user_fec\n";
	$sql.= "      ,concat(p.period_code,': ' ,DATE_FORMAT(p.period_start,'%d/%m/%Y'),' - ',DATE_FORMAT(p.period_end,'%d/%m/%Y')) as period_name\n";
	$sql.= "      ,p.user_id\n";
	$sql.= "      ,if(p.period_sta = 1,'Activo','Culminado') as period_status\n";
	$sql.= "      ,if(e.enroll_id is null,'No Inscrito','Inscrito') as enroll_status\n";
	$sql.= "      ,if(p.period_sta = 1,'info',if(e.enroll_id is not null,'success','danger')) as color_status\n";
	$sql.= "      ,if(e.enroll_id is null, 'NO','YES') as enroll\n";
	$sql.= "      ,l.level_id\n";
	$sql.= "      ,l.level_name\n";
	$sql.= "      ,t.third_name\n";
	$sql.= "      ,t.third_lastname\n";
	$sql.= "      ,concat(ifnull(t.third_name,''),' ',ifnull(t.third_lastname,'')) as third_names\n";
	$sql.= "      ,t.third_rif\n";
	$sql.= "From mod_period p\n";
	$sql.= "Inner Join mod_enroll e on e.period_id = p.period_id and e.third_id = '" . $datos["third_id"] . "'\n";
	$sql.= "Inner Join mod_third t on t.third_id = e.third_id\n";
	$sql.= "Inner Join mod_level l on l.level_id = e.level_id\n";
	$sql.= "Where p.period_sta = 1\n";
	$sql.= "  and p.period_id = '" . $datos["period_id"] . "'\n";
	$sql.= "Order by p.period_start desc\n";
	$db->setQuery( $sql );
	$rows = $db->loadObjectList();
	for($i=0; $i < count($rows); $i++) {
		foreach($rows[$i] as $key => $value) {
			$datos[$key] 	= $value;
		}
	}
	echo "<div class=\"container\">\n";
	echo "	<div class=\"modal-content\">\n";
	echo "		<div class=\"modal-header\">\n";
	echo "			Resumen Inscripción : <kbd>" . $datos["third_rif"] . " - " . $datos["third_name"] . " " . $datos["third_lastname"] . "</kbd> " . $datos["period_name"] . "\n";
	echo "		</div>\n";
	echo "		<div class=\"modal-body\">\n";
	echo "			<h1>Materias  Inscritas en <kbd>" . $datos["level_name"] . "</kbd></h1>\n";
	echo "			<ul class=\"list-group\">\n";
	$sqm = "Select tm.item_id\n";
	$sqm.= "      ,tm.third_id\n";
	$sqm.= "      ,tm.period_id\n";
	$sqm.= "      ,tm.sec_id\n";
	$sqm.= "      ,tm.matter_id\n";
	$sqm.= "      ,tm.item_sta\n";
	$sqm.= "      ,tm.item_note\n";
	$sqm.= "      ,DATE_FORMAT(tm.item_start,'%d/%m/%Y') as item_start\n";
	$sqm.= "      ,DATE_FORMAT(tm.item_end,'%d/%m/%Y') as item_end\n";
	$sqm.= "      ,DATE_FORMAT(tm.user_fec,'%d/%m/%Y %H:%i:%s') as user_fec\n";
	$sqm.= "      ,tm.user_id\n";
	$sqm.= "      ,m.matter_name\n";
	$sqm.= "      ,s.sec_code\n";
	$sqm.= "From mod_third_matter tm\n";
	$sqm.= "Left Join mod_matter m on m.matter_id = tm.matter_id\n";
	$sqm.= "Left Join mod_section s on s.sec_id = tm.sec_id\n";
	$sqm.= "Where tm.period_id = '" .$datos["period_id"] . "'\n";
	$sqm.= "  and tm.third_id = '" . $datos["third_id"] . "'\n";
	$sqm.= "Order by tm.item_end desc\n";
	// echo $sqm;
	$db->setQuery( $sqm );
	$rowm = $db->loadObjectList();
	for($m = 0; $m < count($rowm) ; $m++) {	
		echo "				<li class=\"list-group-item\">" . $rowm[$m]->matter_name . "<span class=\"badge\">" . $rowm[$m]->sec_code . "</span></li>\n";
	}
	echo "			</ul>\n";
	echo "		</div>\n";
    echo "		<div class=\"modal-footer\">\n";
    echo "			<button type=\"button\" id=\"printEnrolls\" class=\"btn btn-danger\">\n";
    echo "				<i class=\"glyphicon glyphicon-print\"></i>\n";
    echo "				Reporte Inscripcion\n";
    echo "			</button>\n";
    echo "		</div>\n";
	echo "	</div>\n";
	echo "</div>\n";