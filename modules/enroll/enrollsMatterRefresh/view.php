<?php
	$mod  = JFactory::getModulo();
	$db   = JFactory::getDBO();
	$datos = json_decode(JRequest::getVar("datos"),true);
	echo "		   <table id=\"periodMatter_" . $datos["period_id"] . "\" class=\"table table-bordered\">\n";
	echo "		       <thead>\n";
	echo "		           <tr>\n";
	echo "		               <th>Nivel</th>\n";
	echo "		               <th>Materias</th>\n";
	echo "		               <th>Seccion</th>\n";
	echo "		           </tr>\n";
	echo "		       </thead>\n";
	echo "		      <tbody>\n";
	$sqm = "Select tm.item_id\n";
	$sqm.= "      ,tm.third_id\n";
	$sqm.= "      ,tm.period_id\n";
	$sqm.= "      ,tm.sec_id\n";
	$sqm.= "      ,tm.matter_id\n";
	$sqm.= "      ,tm.item_sta\n";
	$sqm.= "      ,tm.item_note\n";
	$sqm.= "      ,DATE_FORMAT(tm.item_start,'%d/%m/%Y') as item_start\n";
	$sqm.= "      ,DATE_FORMAT(tm.item_end,'%d/%m/%Y') as item_end\n";
	$sqm.= "      ,DATE_FORMAT(tm.user_fec,'%d/%m/%Y %H:%i:%s') as user_fec\n";
	$sqm.= "      ,tm.user_id\n";
	$sqm.= "      ,l.level_id\n";
	$sqm.= "      ,l.level_name\n";
	$sqm.= "      ,m.matter_name\n";
	$sqm.= "      ,s.sec_code\n";
	$sqm.= "      ,n.note_class\n";
	$sqm.= "      ,s.sec_code\n";
	$sqm.= "      ,pm.item_id as teacher_third\n";
	$sqm.= "      ,ifnull(a.tassits,0) as tassits\n";
	$sqm.= "      ,ifnull(pm.item_assistance,0) as passits\n";
	$sqm.= "      ,FLOOR(ifnull((ifnull(a.tassits,0)*100)/ifnull(pm.item_assistance,0),0)) as assist\n";
	$sqm.= "From mod_third_matter tm\n";
	$sqm.= "Inner Join mod_matter m on m.matter_id = tm.matter_id\n";
	$sqm.= "Inner Join mod_level l on l.level_id = m.matter_level\n";
	$sqm.= "Left Join mod_section s on s.sec_id = tm.sec_id\n";
	$sqm.= "Left Join mod_note n on n.note_code = tm.item_note\n";
	$sqm.= "Left Join mod_teacher_matter pm on pm.matter_id = tm.matter_id and pm.sec_id = tm.sec_id\n";
	$sqm.= "Left Join (Select  xt.matter_third\n";
	$sqm.= "       			  ,sum(ifnull(xt.court_assists,0)) as tassits\n";
	$sqm.= "       	   From mod_third_matter_court xt\n";
	$sqm.= "       	   Where xt.matter_third is not null\n";
	$sqm.= "       	   Group by xt.matter_third) a on a.matter_third = tm.item_id\n";
	$sqm.= "Where tm.period_id = '" .$datos["period_id"] . "'\n";
	$sqm.= "  and tm.third_id = '" . $datos["third_id"] . "'\n";
	$sqm.= "Order by tm.item_end desc\n";
	//echo $sqm;
	$db->setQuery( $sqm );
	$rowm = $db->loadObjectList();
	for($m = 0; $m < count($rowm) ; $m++) {
		echo "		      <tr item=\"" . $rowm[$m]->item_id . "\">\n";
		echo "		      <td ref=\"level\">" . $rowm[$m]->level_name . "</td>\n";
		echo "		      <td ref=\"matter\">" . $rowm[$m]->matter_name . "</td>\n";
		echo "		      <td ref=\"section\">\n";
		echo "		        <div class=\"btn-group\" style=\"z-index: " . (count($rowm)-$m) . ";\">\n";
		if($datos["period_sta"] == "1") {
			echo "              <select class=\"form-control col-xs-12 selectpicker\" title=\"Selecione Secci�n\" item_matter=\"" . $rowm[$m]->item_id . "\" name=\"sec_id\" id=\"sec_id_" .$datos["period_id"] . "\">\n";
			$sqs = "Select s.sec_id\n";
			$sqs.= "      ,s.sec_code\n";
			$sqs.= "From mod_section s\n";
			$sqs.= "Where s.sec_sta = '1'\n";
			$sqs.= "  and s.sec_level = '" . $rowm[$m]->level_id . "'\n";
			$db->setQuery( $sqs );
			$rowsc = $db->loadObjectList();
			for($sc = 0; $sc < count($rowsc) ; $sc++) {
				echo "              <option value=\"" . $rowsc[$sc]->sec_id . "\" " . ($rowsc[$sc]->sec_id == $rowm[$m]->sec_id?"selected":"") . " data-content=\"<kbd>" . $rowsc[$sc]->sec_code . "</kbd>\">" . $rowsc[$sc]->sec_code . "</option>\n";
			}
			echo "              </select>\n";
		} else {
			echo "		      <kbd>" . $rowm[$m]->sec_code . "</kbd>\n";
		}
		echo "		      	</div>\n";
		echo "		      </td>\n";
		echo "		      </tr>\n";
	}
	echo "		      </tbody>\n";
	echo "		   </table>\n";