<?php
$target = JFunc::FTarget();
$mod	= JFactory::getModulo();
$db		= JFactory::getDBO();
$user	= JFactory::getUser();
$datos	= json_decode(JRequest::getVar("datos"),true);
$datos["view"]	= "modify";
$back	= json_decode(JRequest::getVar("back"),true);
?>
<script>
    objDataBack   	    = <?php echo json_encode($back); ?>;
    objDataModify 	    = {};
</script>
<div class="col-xs-12">
<div id="rootwizard">
    <div class="navbar">
        <div class="navbar-inner">
            <div class="container">
                <ul>
                    <li ref="dataBasic"><a href="#tab1" data-toggle="tab">Paso 1. Carga Alumno</a></li>
                    <li ref="dataRepresent"><a href="#tab2" data-toggle="tab">Paso 2. Carga Representantes</a></li>
                    <li ref="dataInscript"><a href="#tab3" data-toggle="tab">Paso 3. Incribir</a></li>
                    <li ref="dataFinish"><a href="#tab4" data-toggle="tab">Terminado</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div id="bar" class="progress progress-striped active">
        <div class="bar progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
    </div>
    <div class="tab-content">
        <div class="tab-pane" id="tab1">
            <form id="thirdModify" data-id="third_id" data-module="third" data-vista="studentsUp" class="form-horizontal" method="POST" target="<?PHP echo $target; ?>">
                <div class="alertDiv oculto"></div>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 id="panel-title" class="panel-title">Datos de Alumno</h3>
                    </div>
                    <div class="panel-body">      
					    <span style="display:none;"><input class="submitThird" type="submit" value="Submit"></span>
                        <input type="hidden" id="third_id" name="third_id">
                        <input type="hidden" id="third_sta" name="third_sta" value="1">
                        <input type="hidden" id="accion" name="accion" value="add">
                        <div class="form-group">
                            <div class="container-fluid">
                                <div class="col-xs-11">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span for="third_rif" class="input-group-addon">Cedula Escolar</span>
                                            <div class="inner-addon right-addon">
                                                <i class="glyphicon glyphicon-search third_rif"></i>
                                                <input type="text" class="form-control col-xs-12" placeholder="Cedula Escolar" id="third_rif" name="third_rif" aria-required="true" data-rule-required="true" data-msg-required="Ingrese una Cedula" data-rule-maxlength="8" data-msg-maxlength="No ingrese mas de 8 Caracteres">
                                                <div class="Typeahead-menu"></div>
                                                <button class="u-hidden" type="submit">blah</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-5">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span for="third_name" class="input-group-addon">Nombres</span>
                                            <input type="text" class="form-control" placeholder="Nombres" id="third_name" name="third_name" aria-required="true" data-rule-required="true" data-msg-required="Ingrese un Nombre al Estudiante" data-rule-maxlength="150" data-msg-maxlength="No ingrese mas de 150 Caracteres">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span for="third_lastname" class="input-group-addon">Apellidos</span>
                                            <input type="text" class="form-control" placeholder="Apellidos" id="third_lastname" name="third_lastname" aria-required="true" data-rule-required="true" data-msg-required="Ingrese un Apellidos al Estudiante" data-rule-maxlength="150" data-msg-maxlength="No ingrese mas de 150 Caracteres">
                                        </div>
                                    </div>
									<div class="form-group">
										<div class="input-group">
											<span for="third_birth" class="input-group-addon">Fecha de Nacimiento</span>
											<input type="text" class="form-control datepicker" placeholder="DD/MM/YYYY" id="third_birth" name="third_birth" aria-required="true" data-rule-required="true" data-msg-required="Ingrese Fecha Nacimiento" data-rule-maxlength="10" data-msg-maxlength="No ingrese mas de 10 Caracteres">
										</div>
									</div>
									<div class="form-group">
										<div class="input-group" style="z-index: 7;">
											<span class="input-group-addon">Sexo</span>
											<select class="form-control selectpicker" title='Selecione Sexo' name="third_sex" id="third_sex" data-rule-required="true" data-rule-email="true" data-msg-required="Ingrese El Sexo">
												<option value="M">Masculino</option>
												<option value="F">Femenino</option>
											</select>
										</div>
									</div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">Nacionalidad</span>
                                            <select class="form-control selectpicker" title='Selecione Nacionalidad' name="third_nationallity" id="third_nationallity" aria-required="true" data-rule-required="true" data-msg-required="Ingrese la Nacionalidad">
                                                <option value="V">Venezolano</option>
                                                <option value="E">Extranjero</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span for="third_address" class="input-group-addon">Dirección</span>
                                            <textarea class="form-control" placeholder="Dirección" id="third_address" name="third_address" data-rule-maxlength="255" data-msg-maxlength="No ingrese mas de 200 Caracteres"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-5 col-md-offset-1">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span for="third_email" class="input-group-addon">Correo</span>
                                            <input type="email" class="form-control" placeholder="ejemplo@ejemplo.com" id="third_email" name="third_email" aria-required="true" data-rule-required="true" data-rule-email="true" data-msg-required="Ingrese un correo" data-msg-email="Ingrese un correo Valido" data-rule-maxlength="150" data-msg-maxlength="No ingrese mas de 150 Caracteres">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span for="third_phone" class="input-group-addon">Teléfono</span>
                                            <input type="text" class="form-control" placeholder="Teléfono" id="third_phone" name="third_phone" data-rule-number="true" data-msg-number="Solo Números" aria-required="true" data-rule-required="true" data-msg-required="Ingrese un Número de Teléfono" data-rule-maxlength="12" data-msg-maxlength="No ingrese mas de 12 Caracteres">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span for="third_description" class="input-group-addon">Descripción</span>
                                            <textarea class="form-control" placeholder="Descripción" id="third_description" name="third_description" data-rule-maxlength="255" data-msg-maxlength="No ingrese mas de 200 Caracteres"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script id="result-template" type="text/x-handlebars-template">
                            <div class="ProfileCard u-cf">
                                {{#if third_img}}
                                    <img class="ProfileCard-avatar" src="front/images/profile/{{third_path}}/profile/{{third_img}}">
                                {{else}}
                                    <span class="ProfileCard-avatar glyphicon glyphicon-user" style="font-size: 45px; padding: 2px;"></span>
                                {{/if}}
                                <div class="ProfileCard-details">
                                    <div class="ProfileCard-realName">{{third_names}}</div>
                                    <div class="ProfileCard-screenName">CE: {{third_rif}}</div>
                                    <div class="ProfileCard-description">Telefono : {{third_phone}}</div>
                                </div>
                            </div>
                        </script>
                        <script id="empty-template" type="text/x-handlebars-template">
                            <div class="EmptyMessage">Alumno no Existe</div>
                        </script>
                    </div>
                </div>
            </form>
        </div>
        <div class="tab-pane" id="tab2">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 id="panel-title" class="panel-title">Representantes</h3>
                </div>
                <div class="panel-body">
                    <div class="panel-group" id="represent">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="container-fluid">
                                    <div class="btn-group">
                                        <?php if($mod->getInsert()) { ?>
                                            <button id="addRepresent" type="button" class="btn btn-primary" title="Nuevo Representante" onclick="addNewRepresent()">
                                                <i class="glyphicon glyphicon-plus"></i>
                                                Nuevo
                                            </button>
                                            <button id="listRepresent" type="button" class="btn btn-success" title="Lista Representantes" onclick="upSetRepresent()">
                                                <i class="glyphicon glyphicon-list"></i>
                                                Representantes
                                            </button>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="container-fluid">
                                    <div class="table-responsive">
                                        <table id="representTabla" class="table table-bordered" data-update="<?php echo $mod->getUpdate(); ?>" data-delete="<?php echo $mod->getDelete(); ?>">
                                            <thead>
                                            <tr>
                                                <th data-visible="false" data-type="num" data-class="third_id" data-bSearchable="false" data-bSortable="false">#</th>
                                                <th data-type="string" data-class="third_names" data-bSearchable="false" data-bSortable="false">Nombre</th>
                                                <th data-type="string" data-class="third_rif" data-bSearchable="false" data-bSortable="false">CI/RIF</th>
                                                <th data-type="string" data-class="third_relName" data-bSearchable="false" data-bSortable="false">Relacion</th>
                                                <th data-type="string" data-class="third_phone" data-bSearchable="false" data-bSortable="false">Telefono</th>
                                                <th data-type="string" data-class="third_email" data-bSearchable="false" data-bSortable="false">E-Mail</th>
                                                <th data-type="string" data-class="accion" data-bSearchable="false" data-bSortable="false">Accion</th>
                                            </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="tab3">
            <div class="panel panel-primary">
				<div class="panel-heading">
					<h3 id="panel-title" class="panel-title">Inscripción</h3>
				</div>
				<div class="panel-body">
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="panel-group" id="perdiodElective" role="tablist" aria-multiselectable="true">
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
		<div class="tab-pane" id="tab4">
			<div id="enrollResume">
			</div>
		</div>
    </div>
    <div class="btn-group" style="float:right">
        <button type="button" class="btn btn-primary button-next" name='next'>
            Siguiente
            <i class="glyphicon glyphicon-chevron-right"></i>
        </button>
    </div>
    <div class="btn-group" style="float:right">
        <button type="button" class="btn btn-info button-new-inscription">
            <i class="glyphicon glyphicon-plus"></i>
            Nueva Inscripción
        </button>
    </div>
    <div class="btn-group" style="float:left">
        <button type="button" class="btn btn-primary button-previous" name='previous'>
            <i class="glyphicon glyphicon-chevron-left"></i>
            Anterior
        </button>
    </div>
</div>
</div>