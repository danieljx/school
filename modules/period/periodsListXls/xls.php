<?php
$target	= JFunc::FTarget();
$mod	= JFactory::getModulo();    
$db		= JFactory::getDBO();
$user	= JFactory::getUser();
$datos	= json_decode(JRequest::getVar("datos"),true);
$sql = "Select p.period_id\n";
$sql.= "      ,p.period_code\n";
$sql.= "      ,p.period_sta\n";
$sql.= "      ,DATE_FORMAT(p.period_start,'%d/%m/%Y') as period_start\n";
$sql.= "      ,DATE_FORMAT(p.period_end,'%d/%m/%Y') as period_end\n";
$sql.= "      ,DATE_FORMAT(p.user_fec,'%d/%m/%Y %H:%i:%s') as user_fec\n";
$sql.= "      ,p.user_id\n";
$sql.= "      ,if(p.period_sta = 1,'Activo','Inactivo') as period_status\n";
$sql.= "From mod_period p\n";
$sql.= "Where p.period_id is not null\n";
if(!is_null($datos["ids"]) && !empty($datos["ids"])) {
	$sql.= "and p.period_id in (" . $datos["ids"] . ")\n";
} else {
	$sql.= "and p.period_id is null\n";
}
$db  = JFactory::getDBO();
$db->setQuery($sql);
$rows = $db->loadObjectList();
$datos = Array();
if(count($rows) > 0) {
	for($i=0; $i < count($rows); $i++) {
		$arrayItemDatos = Array();
		foreach($rows[$i] as $key => $value) {
			$arrayItemDatos[$key] 	= $value;
		}
		array_push($datos,$arrayItemDatos);
	}
}
echo "<table width=\"641\" border=\"1\">\n";
echo "<tr>\n";
echo "<th width=\"50%\" style=\"background-color:#006; text-align:center; color:#FFF\"><strong>Coodigo</strong></th>\n";
echo "<th width=\"50%\" style=\"background-color:#006; text-align:center; color:#FFF\"><strong>Fecha Inicio</strong></th>\n";
echo "<th width=\"50%\" style=\"background-color:#006; text-align:center; color:#FFF\"><strong>Fecha Fin</strong></th>\n";
echo "<th width=\"50%\" style=\"background-color:#006; text-align:center; color:#FFF\"><strong>Estatus</strong></th>\n";
echo "</tr>\n";
for($i = 0; $i < count($datos); $i++) {
	echo "<tr>\n";
    echo "<td bgcolor=\"#ededed\" align=\"center\">" . $datos[$i]["period_code"]  ."</td>\n";
    echo "<td bgcolor=\"#ededed\" align=\"center\">" . $datos[$i]["period_start"]  ."</td>\n";
    echo "<td bgcolor=\"#ededed\" align=\"center\">" . $datos[$i]["period_end"]  ."</td>\n";
    echo "<td bgcolor=\"#ededed\" align=\"center\">" . $datos[$i]["period_status"]  ."</td>\n";
	echo "</tr>\n";
}
echo "</table>\n";