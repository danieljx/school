<?php
class JPeriods {
    private $period_id		= null;
    private $period_sta	    = null;
    private $period_code	= null;
    private $period_start	= null;
    private $period_end	    = null;
    private $user_id	    = null;
    private $user_fec	    = null;
    private $accion		    = null;
    private $view		    = "list";

    function __construct($datos = array()) {
		$this->requestPeriods($datos);
	}
    public function getDisable($tipo = "campo") {
    	$mod  = JFactory::getModulo();
    	if($this->accion == "add" && !$mod->getInsert()) {
			return "disabled";
		} else if($this->accion == "upd" && !$mod->getUpdate()) {
			return "disabled";
		} else if($this->accion == "del" && $tipo=="campo") {
			return "disabled";
		} else if($this->accion == "del" && !$mod->getDelete()) {
			return "disabled";
		}
    	return "";
	}
	public function getClassAtribute() {
        return get_object_vars($this);
    }
    public function requestPeriods($datos = array()) {
		if(count($datos) > 0) {
			foreach($datos as $key => $value) {
                if(array_key_exists($key,$this->getClassAtribute())) {
                    $this->$key =  $value;
                }
			}
		}
    }
    public function setPeriods() {
    	$user = JFactory::getUser();
    	$mod  = JFactory::getModulo();
    	$db   = JFactory::getDBO();
		$temp = array("status" => "ERROR", "type" => "alert-danger", "text" => "NO TIENE ACCESO", "id" => "");
    	if($this->accion != "del") {
			$obj 	= new stdClass();
			$datos	= $this->getClassAtribute();
			if(count($datos) > 0) {
				foreach($datos as $key => $value) {
                    if($key != "accion" && $key != "view" && !is_null($value) && !empty($value)) {
						$obj->$key =  $value;
					}
				}
			}
            $obj->period_start  = "DATE_FORMAT" .(empty($this->period_start)   ? "" : $this->period_start);
            $obj->period_end    = "DATE_FORMAT" .(empty($this->period_end)   ? "" : $this->period_end);
			$obj->user_id	    = $user->getId();
			$obj->user_fec      = 'NOW_AHORA';
			if($this->period_sta == "1") {
				$sql = "Update mod_period set period_sta = 2 Where period_sta = 1\n";
				if(!$db->Execute($sql)) {
					$temp["id"]  	= $obj->period_id;
					$temp["status"]	= "ERROR";
					$temp["text"]  	= $db->getErrorMsg();
					$temp["type"]  	= "alert-danger";
				} else {
					$temp["id"]  	= $obj->period_id;
					$temp["status"]	= "OK";
					$temp["text"]  	= "Operacion Satisfactoria";
					$temp["type"]  	= "alert-info";
				}
			}
    	  	if($this->accion == "add" && $mod->getInsert()) {
				if(!$db->insertObject('mod_period', $obj,'period_id')) {
                    $temp["id"]  	= "";
                    $temp["text"]  	= $db->getErrorMsg();
				} else {
					$this->period_id = $obj->period_id;
					$temp["id"]		= $obj->period_id;
					$temp["status"]	= "OK";
					$temp["text"]  	= "Operacion Satisfactoria";
					$temp["type"]  	= "alert-info";
				}   	  	  	
			} else if($mod->getUpdate()) {
				$obj->period_id   = (int)$this->period_id;
				if(!$db->updateObject('mod_period', $obj,"period_id")) {
					$temp["id"]  	= "";
					$temp["text"]  	= $db->getErrorMsg();
				} else {
					$temp["id"]  	= $obj->period_id;
					$temp["status"]	= "OK";
					$temp["text"]  	= "Operacion Satisfactoria";
					$temp["type"]  	= "alert-info";
				}
			}
		} else {
    	  	$sql = "DELETE FROM mod_period WHERE period_id = " . $this->period_id;
    	  	if(!$db->Execute($sql)) {					
				$temp["id"]  	= "";
				$temp["text"]  	= $db->getErrorMsg();
			} else {
				$temp["status"]	= "DELETE";
				$temp["text"]  	= "Operacion Satisfactoria";
				$temp["type"]  	= "alert-info";
			}
		}
    	return  $temp;
    }
	public function getPeriods() {
    	$user = JFactory::getUser();
		$sql = "Select p.period_id\n";
		$sql.= "      ,p.period_id as id\n";
		$sql.= "      ,p.period_code\n";
		$sql.= "      ,p.period_sta\n";
		$sql.= "      ,DATE_FORMAT(p.period_start,'%d/%m/%Y') as period_start\n";
		$sql.= "      ,DATE_FORMAT(p.period_end,'%d/%m/%Y') as period_end\n";
		$sql.= "      ,DATE_FORMAT(p.user_fec,'%d/%m/%Y %H:%i:%s') as user_fec\n";
		$sql.= "      ,p.user_id\n";
		$sql.= "      ,if(p.period_sta = 1,'Activo','Inactivo') as period_status\n";
		$sql.= "From mod_period p\n";
		$sql.= "Where p.period_id is not null\n";
		if(!is_null($this->period_id) && !empty($this->period_id)) {
			$sql.= "and p.period_id = " . $this->period_id . "\n";
		}
		if(!is_null($this->period_code) && !empty($this->period_code)) {
			$sql.= "and p.period_code like '%" . $this->period_code . "%'\n";
		}
		if(!is_null($this->period_sta)) {
			if(is_array($this->period_sta)) {
				$sql.= "and p.period_sta in (" . implode($this->period_sta,",") . ")\n";
			} else if(!empty($this->period_sta)){
				$sql.= "and p.period_sta = '" . $this->period_sta . "'\n";
			}
		}
		//echo $sql;
		$db  = JFactory::getDBO();
		$db->setQuery($sql);
		$rows = $db->loadObjectList();
		$arrayDatos = array();
		if($this->view == "modify") {
			$arrayDatos = $this->getClassAtribute();
			if(count($rows) == 1 && $this->accion != "add") {
				for($i=0; $i < count($rows); $i++) {
					$iT = 0;
					foreach($rows[$i] as $key => $value) {
						$arrayDatos[$iT] 	= $value;
						$arrayDatos[$key] 	= $value;
						$iT++;
					}
				}
			}
		} else if($this->view == "list"){
			if(count($rows) > 0) {
				for($i=0; $i < count($rows); $i++) {
					$iT = 0;
					$arrayItemDatos = Array();
					foreach($rows[$i] as $key => $value) {
						$arrayItemDatos[$iT] 	= $value;
						$arrayItemDatos[$key] 	= $value;
						$iT++;
					}
					array_push($arrayDatos,$arrayItemDatos);
				}
			}
		}
		return $arrayDatos;
	}
}