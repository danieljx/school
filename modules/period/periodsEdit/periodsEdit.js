function listPeriods() {
	var url = "index.php?token=" + targetCode + "&module=period&vista=periodsList&back=" + JSON.stringify(objDataBack);
		$(location).attr('href',url);
}
$(function () {
	$("button#periodList").on("click", function() {
        listPeriods();
	});
    $('input#period_start').datetimepicker({
        format: 'd/MM/YYYY'
    });
    $('input#period_end').datetimepicker({
        format: 'd/MM/YYYY'
    });
    $("input#period_start").on("dp.change", function (e) {
       $('input#period_end').data("DateTimePicker").minDate(e.date);
    });
    $("input#period_end").on("dp.change", function (e) {
      //  $('input#period_start').data("DateTimePicker").minDate(e.date);
    });
});