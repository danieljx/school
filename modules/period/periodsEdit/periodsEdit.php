<?php
	$target = JFunc::FTarget();
	$mod	= JFactory::getModulo();
	$db		= JFactory::getDBO();
	$user	= JFactory::getUser();
	$datos	= json_decode(JRequest::getVar("datos"),true);
	$datos["view"]	= "modify";
	$back	    = json_decode(JRequest::getVar("back"),true);
	$per        = new Jperiods($datos);
	$periodData  = $per->getPeriods();
    $periodData["accion"]	= (isset($datos["accion"])?$datos["accion"]:"add");
?>
<script>
	objDataBack   	= <?php echo json_encode($back); ?>;
	objDataModify 	= <?php echo json_encode($periodData); ?>;
</script>
<div class="col-xs-2">
	<ul class="nav nav-tabs tabs-left">
		<li class="active">
			<a href="#periodsData" data-toggle="tab">Datos</a>
		</li>
	</ul>
</div>
<div class="col-xs-10">
	<form id="periodModify" data-id="period_id" data-module="period" data-vista="periodsUp" class="form-horizontal formModify" method="POST" target="<?PHP echo $target; ?>">
        <div class="alertDiv oculto"></div>
        <div class="tab-content">
			<div class="tab-pane active" id="periodsData">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 id="panel-title" class="panel-title">Datos</h3>
					</div>
					<div class="panel-body">
                        <input type="hidden" id="period_id" name="period_id">
                        <input type="hidden" id="accion" name="accion">
                        <div class="form-group">
                            <div class="container-fluid">
								<div class="col-xs-5">
									<div class="form-group">
										<div class="input-group">
											<span for="period_code" class="input-group-addon">Código</span>
											<input type="text" class="form-control" placeholder="Codigo" id="period_code" name="period_code" aria-required="true" data-period-required="true" data-msg-required="Ingrese un Codigo al Periodo" data-rule-maxlength="150" data-msg-minlength="No ingrese mas de 20 Caracteres">
										</div>
									</div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span for="period_start" class="input-group-addon">Fecha Inicio1</span>
                                            <input type="text" class="form-control" placeholder="d/m/Y" id="period_start" name="period_start" aria-required="true" data-period-required="true" data-msg-required="Ingrese una Fecha" data-rule-maxlength="150" data-msg-minlength="No ingrese mas de 20 Caracteres">
                                        </div>
									</div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span for="period_end" class="input-group-addon">Fecha Fin</span>
                                            <input type="text" class="form-control" placeholder="d/m/Y" id="period_end" name="period_end" aria-required="true" data-period-required="true" data-msg-required="Ingrese una Fecha" data-rule-maxlength="150" data-msg-minlength="No ingrese mas de 20 Caracteres">
                                        </div>
									</div>
								</div>
								<div class="col-xs-5 col-md-offset-1">
								<?php if($mod->getStatus()) { ?>
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon">Estatus</span>
											<input type="radio" class="form-control" id="period_sta" name="period_sta" value="1" data-on-color="success" data-label-text="Activo">
											<input type="radio" class="form-control" id="period_sta" name="period_sta" value="2" data-on-color="danger" data-label-text="Inactivo">
										</div>
									</div>
								<?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-default">
		  <div class="panel-body">
			<div class="container-fluid">
				<div class="btn-group">
					<button type="submit" class="btn btn-success">
						<i class="glyphicon glyphicon-ok"></i>
						Guardar
					</button>
					<button type="button" id="periodList" class="btn btn-info">
						<i class="glyphicon glyphicon-th-list"></i>
						Lista
					</button>
				</div>
			</div>
		  </div>
		</div>
	</form>
</div>