<?php
	$target = JFunc::FTarget();
	$mod	= JFactory::getModulo();
	$db		= JFactory::getDBO();
	$user	= JFactory::getUser();
	$back	= JRequest::getVar("back");
	if(!empty($back)) {
		$back	= json_decode($back,true);
	} else {
		$back	= json_decode("[]",true);
	}
	$back["view"]	= "list";
	$per = new JPeriods($back);
?>
<script>
	objDataBack   = <?php echo json_encode($back); ?>;
	arrayDataList = <?php echo json_encode($per->getPeriods()); ?>;
</script>
<h2 class="page-header">Periodos</h2>
<div class="panel panel-info">
	<div class="panel-heading">
		<h3 id="panel-title" class="panel-title">Filtro de Periodos</h3>
	</div>
	<div class="panel-body">
		<form id="periodsFilter" data-module="period" data-vista="periodsData" class="form-horizontal formList" method="POST" target="<?PHP echo $target; ?>">
			<div class="form-group">
				<div class="col-xs-4">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-barcode"></span></span>
                    <input type="text" class="form-control" placeholder="Codigo de la Periodo" name="period_code">
					</div>
				</div>
				<div class="col-xs-4">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-stats"></span></span>
						<select class="form-control selectpicker" title='Selecione Estatus' name="period_sta" id="period_sta" multiple>
							<option value="1">Activo</option>
							<option value="2">Inactivo</option>
						</select>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="container-fluid">
					<div class="btn-group">
						<button type="submit" class="btn btn-primary">
							<i class="glyphicon glyphicon-filter"></i>
							Filtrar
						</button>
						<button type="reset" class="btn btn-warning">
							<i class="glyphicon glyphicon-erase"></i>
							Limpiar
						</button>
						<?php if($mod->getInsert()) { ?>
						<button id="newPeriod" type="button" class="btn btn-info">
							<i class="glyphicon glyphicon-edit"></i>
							Nuevo
						</button>
						<?php } ?>
						<?php if($mod->getPrint()) { ?>
						<button id="pdfListPeriods" type="button" class="btn btn-danger pdf" data-module="period" data-view="periodsListPdf" data-title="Listado Periodos">
							<i class="glyphicon glyphicon-print"></i>
							Pdf
						</button>
						<?php } ?>
						<?php if($mod->getExport()) { ?>
						<button id="xlsListPeriods" type="button" class="btn btn-success excel" data-module="period" data-view="periodsListXls" data-title="Listado Periodos">
							<i class="glyphicon glyphicon-share"></i>
							Excel
						</button>
						<?php } ?>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<div class="container-fluid">
	<div class="table-responsive">
		<table id="periodsTabla" class="table dataTableList table-bordered" data-module="period" data-del-view="periodsUp" data-update="<?php echo $mod->getUpdate(); ?>" data-delete="<?php echo $mod->getDelete(); ?>">
			<thead>
				<tr>
					<th data-visible="false" data-type="num" data-class="period_id" data-bSearchable="false" data-bSortable="false">#</th>
					<th data-type="string" data-class="period_code" data-bSearchable="true" data-bSortable="true">Codigo</th>
					<th data-type="string" data-class="period_start" data-bSearchable="true" data-bSortable="true">Fecha Inicio</th>
					<th data-type="string" data-class="period_end" data-bSearchable="true" data-bSortable="true">Fecha Fin</th>
					<th data-type="string" data-class="period_status" data-bSearchable="true" data-bSortable="true">Estatus</th>
					<th data-type="string" data-class="accion" data-bSearchable="false" data-bSortable="false">Accion</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
</div>