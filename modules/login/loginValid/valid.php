<?php
$db   = JFactory::getDBO();
$err  = "OK";
$type = "alert-danger";
$usr  = JRequest::getVar("user_name");
$pwd  = JRequest::getVar("user_pass");
$reg  = JRequest::getVar("regresar");
if(empty($usr)) {
	$err = "Ingrese Usuario";
} else if(empty($pwd)) {
	$err = "Ingrese Contraseña";
} else {
	$sql = "Select user_id\n";
	$sql.= "	  ,user_pass\n";
	$sql.= "	  ,user_try\n";
	$sql.= "	  ,user_sta\n";
	$sql.= "From sys_user\n";
	$sql.= "Where user_name = " . strtolower($db->Quote($usr)) . "\n";
	$db->setQuery( $sql );
	$rows = $db->loadObjectList();
	if(count($rows) > 0) {
		if($rows[0]->user_sta == "S") {
			if(strtolower($rows[0]->user_pass) == strtolower($pwd)) {
				if(!isset($_SESSION['user_id'])) {
					$_SESSION['user_id'] = "";
				}
				$_SESSION['user_id'] = $rows[0]->user_id;
				$sql = "Update sys_user set lastvisitDate = now() where user_id = " . $rows[0]->user_id;
				$db->Execute( $sql );
				$type = "alert-success";
			} else {
				if($rows[0]->user_try < 3 && $rows[0]->user_id != "1") {
					$sql = "Update sys_user\n";
					$sql.= "Set user_try = (user_try + 1)\n";
					$sql.= "   ,user_sta = if(user_try<3,'S','N')\n";
					$sql.= "Where user_name = " . strtolower($db->Quote($usr)) . "\n";
					$db->Execute( $sql );
					if(($rows[0]->user_try+1) < 3) {
						$type = "alert-warning";
						$err = "Error Contraseña | Quedan " . (3-($rows[0]->user_try+1)) . " Intentos";					
					} else {
						$type = "alert-danger";
						$err = "Usuario Bloqueado. Comunicarse con el Departamento de Sistemas";
					}
				} else {
					$type = "alert-danger";
					$err = "Error Contraseña";
				}
			}
		} else {
			$type = "alert-danger";
			$err = "Usuario Bloqueado. Comunicarse con el Departamento de Sistemas";
		}
	} else {
		$type = "alert-danger";
		$err = "Error Usuario no Existe";
	}
}
?>
<html>
	<head>
		<script type="text/javascript" 	language="JavaScript" src="front/vendor/Jquery-2.1.4/jquery-2.1.4.min.js"></script>
		<script type="text/javascript"  language="JavaScript" src="front/js/scripts.js"></script>
		<script type="text/javascript"  language="JavaScript" src="front/js/global.js"></script>
		<script type="text/javascript"  language="JavaScript">     
			$().ready(function() {
				window.parent.$("#respForm").attr({"text":$("textarea#text").val(),"type":$("span#type").html()}).click();
				if($("textarea#text").val() == "OK") {
					$('#frmconectar').submit();
				}
			});
		</script>
	</head>
	<body>
		<form id="frmconectar" action="index.php" target="_top"  method="post" >
			<textarea id="text"><?php echo $err;?></textarea>
			<span id="type"><?php echo $type;?></span>
	<?php if(empty($reg)) { ?>
			<input type="hidden"  id="module" name="module" value="inicio">
			<input type="hidden"  id="vista"  name="vista"  value="forma">
	<?php } else {
			$pars = explode("&",$reg);
			for($i=0; $i < count($pars); $i++) {
				$par = explode("=",$pars[$i]);
				if(isset($par[1]) && !empty($par[1])) {
					echo "<input type=\"hidden\"  id=\"" . $par[0] . "\"  name=\"" . $par[0] . "\"  value=\"" . $par[1] . "\">\n";
				}
			}
		}
	?>	  
		</form>
	</body>
</html>