function setRespForm(Obj) {
	$("div.alertDiv > div[role=alert]").removeClass('alert-success').removeClass('alert-danger').addClass(Obj.attr("type")).html("<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>" + Obj.attr("text"));
	$("div.alertDiv").fadeIn();
}
$(document).ready(function () {
    $('#logform').validate({
        rules: {
            pass: {
                minlength: 2,
                required: true
            },
            user: {
                required: true
            },
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('success').addClass('error');
        },
        success: function (element) {
            element.text('').addClass('valid').closest('.form-group').removeClass('error').addClass('success');
        }
    });
});