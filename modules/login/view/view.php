<?php
  $target = JFunc::FTarget();
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Ingreso al Sistema</div>
				<div class="panel-body">
                    <div class="col-sm-3 placeholder">
                        <img style="width: 90%;" src="front/images/logo.png">
                    </div>
                    <div class="col-sm-9 placeholder">
                        <form class="form-horizontal" role="form" method="POST" name="logform" id="logform" action="hide.php" target="<?PHP echo $target; ?>">
                            <div class="alertDiv oculto">
                                <div class="alert alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="user">Usuario</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control required" name="user_name" value="" data-msg-required="Ingrese un Nombre De Usuario" maxlength="15">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="pass">Contraseña</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control required" name="user_pass" data-msg-required="Ingrese una Contraseña">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">Ingresar</button>
                                </div>
                            </div>
                            <div class="oculto">
                                <input type="button" value="" id="respForm" name="respForm" onclick="setRespForm($(this))">
                            </div>
                            <input type="hidden" name="module"    id="module"  value="login">
                            <input type="hidden" name="vista"     id="vista"   value="loginValid">
                            <input type="hidden" name="regresar"  id="regresar"  value="<?php echo  JFactory::getReq("regresar",""); ?>">
                        </form>
					</div>
					<iframe name="<?PHP echo $target; ?>" id="<?PHP echo $target; ?>" width="0" height="0" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" src="about:blank"></iframe>
				</div>
			</div>
		</div>
	</div>
</div>