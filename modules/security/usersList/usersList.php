<?php
	$target = JFunc::FTarget();
	$mod	= JFactory::getModulo();
	$db		= JFactory::getDBO();
	$user	= JFactory::getUser();
	$back	= JRequest::getVar("back");
	if(!empty($back)) {
		$back	= json_decode($back,true);
	} else {
		$back	= json_decode("[]",true);
	}
	$back["view"]		= "list";
	$users 	= new JUsers($back);
?>
<script>
	objDataBack   = <?php echo json_encode($back); ?>;
	arrayDataList = <?php echo json_encode($users->getUsers()); ?>;
</script>
<h2 class="page-header">Usuarios</h2>
<div class="panel panel-info">
	<div class="panel-heading">
		<h3 id="panel-title" class="panel-title">Filtro de Usuarios</h3>
	</div>
	<div class="panel-body">
		<form id="userFilter" data-module="security" data-vista="usersData" class="form-horizontal formList" method="POST" action="oculto.php" target="<?PHP echo $target; ?>">
			<div class="form-group">
				<div class="col-xs-4">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
						<input type="text" class="form-control" placeholder="Usuario" name="user_name">
					</div>
				</div>
				<div class="col-xs-4">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
						<input type="text" class="form-control" placeholder="ejemplo@ejemplo.com" name="user_email">
					</div>
				</div>
				<div class="col-xs-4">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-tasks"></span></span>
						<select class="form-control selectpicker" title='Selecione Roles' name="rule_id" id="rule_id" multiple>
							<?php
							$sql = "Select rule_id\n";
							$sql.= "	  ,rule_name\n";
							$sql.= "From sys_rule\n";
							$sql.= "Where rule_lock = 'N'\n";
							$sql.= "  and rule_sta = 'S'\n";
							$db->setQuery( $sql );
							$rows = $db->loadObjectList();
							for($i=0; $i < count($rows); $i++) {
								echo "<option value=\"" . $rows[$i]->rule_id . "\" >" .  $rows[$i]->rule_name . "</option>\n";
							} 		 
							?>	
						</select>						
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="container-fluid">
					<div class="btn-group">
						<button type="submit" class="btn btn-primary">
							<i class="glyphicon glyphicon-filter"></i>
							Filtrar
						</button>
						<button type="reset" class="btn btn-warning">
							<i class="glyphicon glyphicon-erase"></i>
							Limpiar
						</button>
						<?php if($mod->insert) { ?>
						<button id="newUser" type="button" class="btn btn-info">
							<i class="glyphicon glyphicon-edit"></i>
							Nuevo
						</button>
						<?php } ?>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<div class="container-fluid">
	<div class="table-responsive">
		<table id="usersTabla" class="table dataTableList table-bordered" data-update="<?php echo $mod->update; ?>" data-delete="<?php echo $mod->delete; ?>">
			<thead>
				<tr>
					<th data-visible="false" data-type="num" data-class="user_id" data-bSearchable="false" data-bSortable="false">#</th>
					<th data-type="string" data-class="user_name" data-bSearchable="true" data-bSortable="true">Usuario</th>
					<th data-type="string" data-class="rule_name" data-bSearchable="true" data-bSortable="true">Rol</th>
					<th data-type="string" data-class="user_status" data-bSearchable="true" data-bSortable="true">Estatus</th>
					<th data-type="string" data-class="lastvisitDate" data-bSearchable="true" data-bSortable="true">Ultimo Login</th>
					<th data-type="string" data-class="accion" data-bSearchable="false" data-bSortable="false">Accion</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
</div>

