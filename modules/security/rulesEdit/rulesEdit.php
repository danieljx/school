<?php
	$target = JFunc::FTarget();
	$mod	= JFactory::getModulo();
	$db		= JFactory::getDBO();
	$user	= JFactory::getUser();
	$datos	= json_decode(JRequest::getVar("datos"),true);
	$datos["view"]	= "modify";
	$back	= json_decode(JRequest::getVar("back"),true);
	$rules  = new JRules($datos);
	$ruleData = $rules->getRules();
    $ruleData["accion"]	= (isset($datos["accion"])?$datos["accion"]:"add");
?>
<script>
	objDataBack   	= <?php echo json_encode($back); ?>;
	objDataModify 	= <?php echo json_encode($ruleData); ?>;
</script>
<div class="col-xs-2"> <!-- required for floating -->
  <!-- Nav tabs -->
  <ul class="nav nav-tabs tabs-left"><!-- 'tabs-right' for right tabs -->
    <li class="active">
		<a href="#rulesData" data-toggle="tab">Datos</a>
	</li>
    <li>
		<a href="#rulesMenu" data-toggle="tab">Permisos Menu</a>
	</li>
    <li>
		<a href="#rulesModule" data-toggle="tab">Permisos Modulos</a>
	</li>
  </ul>
</div>
<div class="col-xs-10">
	<form id="ruleModify" data-id="rule_id" data-module="security" data-vista="rulesUp" class="form-horizontal" method="POST" target="<?PHP echo $target; ?>">
        <div class="alertDiv oculto"></div>
		<div class="tab-content">
			<div class="tab-pane active" id="rulesData">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 id="panel-title" class="panel-title">Datos</h3>
					</div>
					<div class="panel-body">
						<input type="hidden" id="rule_id" name="rule_id">
                        <input type="hidden" id="accion" name="accion">
						<div class="form-group">
							<div class="container-fluid">
								<div class="col-xs-5">
									<div class="form-group">
										<div class="input-group">
											<span for="rule_name" class="input-group-addon">Nombre Rol</span>
											<input type="text" class="form-control" placeholder="Nombre de Rol" id="rule_name" name="rule_name" aria-required="true" data-rule-required="true" data-msg-required="Ingrese un Nombre para el Rol">
										</div>
									</div>
								</div>
								<div class="col-xs-5 col-md-offset-1">
								<?php if($mod->getStatus()) { ?>
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon">Estatus</span>
											<input type="radio" class="form-control" id="rule_sta" name="rule_sta" value="S" data-on-color="success" data-label-text="Activo" <?php echo ($ruleData["rule_sta"] == "S"?"checked":"");?>>
											<input type="radio" class="form-control" id="rule_sta" name="rule_sta" value="N" data-on-color="danger" data-label-text="Inactivo" <?php echo ($ruleData["rule_sta"] == "N"?"checked":"");?>>
										</div>
									</div>
								<?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="tab-pane" id="rulesMenu">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 id="panel-title" class="panel-title">Permisos Menu</h3>
					</div>
					<div class="panel-body">
						<div class="col-xs-10">
							<h5 class="page-header"></h5>
							<input type="hidden" id="menuCheked" name="menuCheked" value="">
							<input type="hidden" id="menuUnCheked" name="menuUnCheked" value="">
							<div id="menuRules">
								<ul id="menuTopUl">
									<li id="home" data-jstree='{"selected":true,"disabled":true,"icon":"glyphicon glyphicon-home"}'>Inicio</li>
									<li id="top" data-jstree='{"opened":true,"disabled":true,"icon":"glyphicon glyphicon-hand-up"}'>Menu Top
										<ul id="menuSubTopUl">
										<?php
											$sql = "Select m.men_id\n";
											$sql.= "	  ,m.men_name\n";
											$sql.= "	  ,m.men_desc\n";
											$sql.= "	  ,m.men_type\n";
											$sql.= "	  ,m.men_accion\n";
											$sql.= "	  ,m.men_target\n";
											$sql.= "	  ,m.men_order\n";
											$sql.= "	  ,m.men_icon\n";
											$sql.= "	  ,if(r.accion is null,false,true) as menCheck\n";
											$sql .= "From sys_menu m\n";
											$sql .= "Left Join sys_rule_accion r on r.accion_item = m.men_id and r.accion_type = 'MEN' and r.rule_id = '" . $ruleData["rule_id"] . "'\n";
											$sql .= "Where  m.men_father = 1\n";
											$sql .= "  and  m.men_sta = 'S'\n";
											$sql .= "  and  m.men_class = 'T'\n";
											$sql .= "  and  m.men_type = 'ICO'\n";
											$sql .= "Order by m.men_order\n";
											$db->setQuery( $sql );
											$rows = $db->loadObjectList();
											for($i = 0; $i < count($rows) ; $i++) {
												echo "<li id=\"menuTopLi_" . $rows[$i]->men_id . "\" data-jstree='{\"idMenu\":" . $rows[$i]->men_id . ",\"checkMenu\":" . $rows[$i]->menCheck . ",\"selected\":" . $rows[$i]->menCheck . (!empty($rows[$i]->men_icon)?",\"icon\":\"glyphicon " . $rows[$i]->men_icon . "\"":"") . "}'>" . $rows[$i]->men_name . "\n";
												echo "</li>\n";
											}
										?>
										</ul>
									</li>
									<li id="left" data-jstree='{"opened":true,"disabled":true,"icon":"glyphicon glyphicon-hand-left"}'>Menu Left
										<ul id="menuSubLeftUl">
										<?php
											$sql = "Select m.men_id\n";
											$sql.= "	  ,m.men_name\n";
											$sql.= "	  ,m.men_desc\n";
											$sql.= "	  ,m.men_type\n";
											$sql.= "	  ,m.men_accion\n";
											$sql.= "	  ,m.men_target\n";
											$sql.= "	  ,m.men_order\n";
											$sql.= "	  ,m.men_icon\n";
											$sql.= "	  ,if(r.accion is null,false,true) as menCheck\n";
											$sql.= "From sys_menu m\n";
											$sql.= "Left Join sys_rule_accion r on r.accion_item = m.men_id and r.accion_type = 'MEN' and r.rule_id = '" . $ruleData["rule_id"] . "'\n";
											$sql.= "Where  m.men_father = 1\n";
											$sql.= "  and  m.men_sta = 'S'\n";
											$sql.= "  and  m.men_class = 'L'\n";
											$sql.= "Order by m.men_order\n";
											$db->setQuery( $sql );
											$rows = $db->loadObjectList();				
											for($i = 0; $i < count($rows) ; $i++) {
												$men_name = $rows[$i]->men_name;
												if($rows[$i]->men_type == "SUB") {
													$temp = $rules->ruleMenuSubNivel($rows[$i]->men_id,"            ");
													if(!empty($temp)) {
														$target = "dropdownMenu" . $rows[$i]->men_id;
														echo "\t\t\t<li id=\"" . $rows[$i]->men_id . "\" data-jstree='{\"disabled\":true,\"idMenu\":" . $rows[$i]->men_id . ",\"checkMenu\":" . $rows[$i]->menCheck . ",\"opened\":true,\"selected\":" . $rows[$i]->menCheck . (!empty($rows[$i]->men_icon)?",\"icon\":\"glyphicon " . $rows[$i]->men_icon . "\"":"") . "}'>\n";
														echo "&nbsp;" . $men_name;
														echo "\t\t\t\t\t<ul>\n" . $temp . "</ul>\n"; 
														echo "\t\t\t</li>\n";
													}
												} else {
													echo "\t\t\t<li id=\"" . $rows[$i]->men_id . "\" data-jstree='{\"idMenu\":" . $rows[$i]->men_id . ",\"checkMenu\":" . $rows[$i]->menCheck . ",\"opened\":true,\"selected\":" . $rows[$i]->menCheck . (!empty($rows[$i]->men_icon)?",\"icon\":\"glyphicon " . $rows[$i]->men_icon . "\"":"") . "}'>\n";
													echo "&nbsp;" . $men_name . "</li>\n";
												}
											}
										?>
										</ul>
									</li>
									<li id="out" data-jstree='{"selected":true,"disabled":true,"icon":"glyphicon glyphicon-off"}'>Salir</li>
								</ul>
							</div>
							<h5 class="page-header"></h5>
						</div>
					</div>
				</div>
			</div>
			<div class="tab-pane" id="rulesModule">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 id="panel-title" class="panel-title">Permisos Modulos</h3>
					</div>
					<div class="panel-body">
						<div class="panel-group" id="accionModule" role="tablist" aria-multiselectable="true">
							<?php
								$sql  = "Select m.mod_id\n";
								$sql .= "	   ,m.mod_title\n";
								$sql .= "	   ,ifnull(r.accion,'') as accion\n";
								$sql .= "From sys_module m\n";
								$sql .= "Left Join sys_rule_accion r on r.accion_item = m.mod_id and r.accion_type = 'MOD' and r.rule_id = '" . $ruleData["rule_id"] . "'\n";
								$sql .= "Where  m.mod_id is not null\n";
								$sql .= "Order by m.mod_id\n";
								$db->setQuery( $sql );
								$rows = $db->loadObjectList();
								$arrayAccion = Array();
								for($i = 0; $i < count($rows) ; $i++) {
									$arrayAccion[$rows[$i]->mod_id] = Array();
									if(!empty($rows[$i]->accion)) {
										$arrayAccionItem = explode(";",$rows[$i]->accion);
										for($d = 0; $d < count($arrayAccionItem) ; $d++) {
											$arrayAccion[$rows[$i]->mod_id][] = $arrayAccionItem[$d];
										}
									}
									echo "<div class=\"panel panel-default\">\n";
									echo "	<div class=\"panel-heading\" role=\"tab\" id=\"headingAccionModule_" . $rows[$i]->mod_id . "\">\n";
									echo "		<h4 class=\"panel-title\">\n";
									echo "			<a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accionModule\" href=\"#collapseAccionModule_" . $rows[$i]->mod_id . "\" aria-expanded=\"false\" aria-controls=\"collapseAccionModule_" . $rows[$i]->mod_id . "\">\n";
									echo "			" . $rows[$i]->mod_title . "\n";
									echo "			</a>\n";
									echo "		</h4>\n";
									echo "	</div>\n";
									echo "	<div id=\"collapseAccionModule_" . $rows[$i]->mod_id . "\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-expanded=\"false\" aria-labelledby=\"headingAccionModule_" . $rows[$i]->mod_id . "\">\n";
									echo "		<div class=\"panel-body\">\n";
									echo "			<ul class=\"list-group\">\n";
									$sqla  = "Select a.accion_id\n";
									$sqla .= "	   	,a.accion_name\n";
									$sqla .= "	   	,a.accion_code\n";
									$sqla .= "From sys_accion a\n";
									$sqla .= "Where  a.accion_id is not null\n";
									$sqla .= "Order by a.accion_id\n";
									$db->setQuery( $sqla );
									$rowa = $db->loadObjectList();
									for($a = 0; $a < count($rowa) ; $a++) {
										echo "				<li class=\"list-group-item\">\n";
										echo "					<div class=\"row\">\n";
										echo "						<div class=\"col-xs-2\">" . $rowa[$a]->accion_name . "</div>\n";
										echo "						<div class=\"col-xs-6\">\n";
										echo "							<input type=\"radio\" class=\"form-control accionMod\" id=\"accion_code_" . $rows[$i]->mod_id . "_" . $rowa[$a]->accion_id . "\" name=\"accion_code_" . $rows[$i]->mod_id . "_" . $rowa[$a]->accion_id . "\" value=\"" . $rowa[$a]->accion_code . "\" " . ((strpos($rows[$i]->accion,$rowa[$a]->accion_code) > -1)?"checked":"") . " data-id=\"" . $rows[$i]->mod_id . "\" data-accion=\"true\" data-on-color=\"success\" data-label-text=\"Si\">\n";
										echo "							<input type=\"radio\" class=\"form-control accionMod\" id=\"accion_code_" . $rows[$i]->mod_id . "_" . $rowa[$a]->accion_id . "\" name=\"accion_code_" . $rows[$i]->mod_id . "_" . $rowa[$a]->accion_id . "\" value=\"" . $rowa[$a]->accion_code . "\" " . ((strpos($rows[$i]->accion,$rowa[$a]->accion_code) > -1)?"":"checked") . " data-id=\"" . $rows[$i]->mod_id . "\" data-accion=\"false\" data-on-color=\"warning\" data-label-text=\"No\">\n";
										echo "						</div>\n";
										echo "					</div>\n";
										echo "				</li>\n";
									}
									echo "			</ul>\n";
									echo "		</div>\n";
									echo "	</div>\n";
									echo "</div>\n";
								}
								echo "<script>var arrayAccion = " . json_encode($arrayAccion) . ";</script>";
							?>
						</div>					
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-default">
		  <div class="panel-body">
			<div class="container-fluid">
				<div class="btn-group">
					<button type="submit" class="btn btn-success">
						<i class="glyphicon glyphicon-ok"></i>
						Guardar
					</button>
					<button type="button" id="ruleList" class="btn btn-info">
						<i class="glyphicon glyphicon-th-list"></i>
						Lista
					</button>
				</div>
			</div>
		  </div>
		</div>
	</form>
</div>