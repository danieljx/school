
function listRule() {
	var url = "index.php?token=" + targetCode + "&module=security&vista=rulesList&back=" + JSON.stringify(objDataBack);
		$(location).attr('href',url);
}
function idGetParent(_self,nodeMenu) {
	var parentMenu;
	var menuCheked		= $("#menuCheked").val();
		menuCheked		= (menuCheked != ""?menuCheked.split(","):[]);
	var menuUnCheked	= $("#menuUnCheked").val();
		menuUnCheked	= (menuUnCheked != ""?menuUnCheked.split(","):[]);
	if(nodeMenu.state.selected) {
		if(nodeMenu.state.checkMenu) {
			menuUnCheked	= menuUnCheked.filter(function(item){
				return item != nodeMenu.state.idMenu;
			});
			$("#menuUnCheked").val(menuUnCheked.join(","));
		} else {
			menuCheked.push(nodeMenu.state.idMenu);
			$("#menuCheked").val(menuCheked.join(","));
		}
	} else {
		if(nodeMenu.state.checkMenu) {
			menuUnCheked.push(nodeMenu.state.idMenu);
			$("#menuUnCheked").val(menuUnCheked.join(","));
		} else {
			menuCheked 	= menuCheked.filter(function(item){
				return item != nodeMenu.state.idMenu;
			});
			$("#menuCheked").val(menuCheked.join(","));
		}
	}
	if(typeof nodeMenu.parent != 'undefined' && nodeMenu.parent != null && !(nodeMenu.parent.match(/^(home|top|left|out)$/))) {
		parentMenu = $.jstree.reference(_self).get_node(nodeMenu.parent);
		idGetParent(_self,parentMenu);
	}
}
function accionMod(oBj) {
	var ObjData = oBj.data();
	var code 	= oBj.val();
	if(ObjData.accion) {
		arrayAccion[ObjData.id].push(code);
	} else {
		arrayAccion[ObjData.id] = arrayAccion[ObjData.id].filter(function (item) {
			return (item != code);
		});
	}
}
$(function () {
	$("button#ruleList").on("click", function() {
		listRule();
	});
	$("input.accionMod").on("switchChange.bootstrapSwitch", function(event, state) {
		accionMod($(this));
	});
	$('#menuRules').on("changed.jstree", function (e, d) {
		if(typeof d.node != 'undefined') {
			var _self = this;
			idGetParent(_self,d.node);
		}
	}).jstree({
		'core': {
			'themes': {
				'name': 'proton',
				'responsive': true
			}
		},
		"checkbox": {
			"two_state": true,
			"override_ui": true
		},
		"plugins": ["themes", "crrm", "search", "dnd", "ui", "checkbox"]
	});
	$('form#ruleModify').autofill(objDataModify);
	$('form#ruleModify').validate({
		submitHandler: function(form) {
			var datos = $('form#ruleModify').serializeObject();
				datos.arrayAccion = arrayAccion;
				datos = JSON.stringify(datos);
			$(form).submit(function() {
				$_self = $(this);
				$(this).ajaxSubmit({
					async: true,
					type: "POST",
					dataType: "json",
					url: "json.php",
					data: {
						module: $_self.data("module"),
						vista: $_self.data("vista"),
						datos: datos,
					},
					beforeSubmit: function(formData, jqForm, options) {
						$_self.find("button[type=submit]").attr({"disabled":"disabled"});
						return true;
					},
					success: function(respJson, statusText, xhr, $form) {
						if(respJson.status != "ERROR") {
							$("#" + $_self.data("id")).val(respJson.id);
							$("#accion").val("upd");
							resAlert = "\t<div class=\"alert alert-dismissible " + respJson.type + "\" role=\"alert\">\n";
							resAlert+= "\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\n";
							resAlert+= "\t\t\t<span aria-hidden=\"true\">&times;</span>\n";
							resAlert+= "\t\t</button>\n";
							resAlert+= "\t" + respJson.text + "</div>\n";
							$_self.find("div.alertDiv").html(resAlert);
							$_self.find("div.alertDiv").fadeIn();
						var menuCheked		= $("#menuCheked").val();
							menuCheked		= (menuCheked != ""?menuCheked.split(","):[]);
						var menuUnCheked	= $("#menuUnCheked").val();
							menuUnCheked	= (menuUnCheked != ""?menuUnCheked.split(","):[]);
							if(menuCheked.length > 0) {
								for(var i=0; i < menuCheked.length; i++) {
									$('#menuRules').jstree(true).get_node(menuCheked[i]).state.checkMenu = true;
								}
							}
							if(menuUnCheked.length > 0) {
								for(var o=0; o < menuUnCheked.length; o++) {
									$('#menuRules').jstree(true).get_node(menuUnCheked[o]).state.checkMenu = false;
								}
							}
							$("#menuCheked").val("");
							$("#menuUnCheked").val("");
						} else {
							$("#myModal" + targetCode + " div.modal-header > h4.modal-title").html(respJson.status);
							$("#myModal" + targetCode + " div.modal-body").html(respJson.text);
							$("#myModal" + targetCode + "").modal("show");
						}
						$_self.find("button[type=submit]").removeAttr("disabled");
					},
					error: function(jqXHR, textStatus, errorThrown) {
						$("#myModal" + targetCode + " div.modal-header > h4.modal-title").html(textStatus);
						$("#myModal" + targetCode + " div.modal-body").html(errorThrown);
						$("#myModal" + targetCode + "").modal("show");
					}
				});
				return false;
			});
		},
        highlight: function (element) {
			$(element).closest('.form-group').removeClass('success').addClass('error');
        },
		success: function(element) {
            element.text('').addClass('valid').closest('.form-group').removeClass('error').addClass('success');
		}
	});
});