<?php
	$target = JFunc::FTarget();
	$mod	= JFactory::getModulo();
	$db		= JFactory::getDBO();
	$user	= JFactory::getUser();
	$datos	= json_decode(JRequest::getVar("datos"),true);
	$datos["view"]	= "modify";
	$back	= json_decode(JRequest::getVar("back"),true);
	$users  = new JUsers($datos);
	$userData = $users->getUsers();
    $userData["accion"]	= (isset($datos["accion"])?$datos["accion"]:"add");
?>
<script>
	objDataBack   	= <?php echo json_encode($back); ?>;
	objDataModify 	= <?php echo json_encode($userData); ?>;
</script>
<form id="userModify" data-id="user_id" data-module="security" data-vista="usersUp" class="form-horizontal formModify" method="POST" target="<?PHP echo $target; ?>">
    <div class="alertDiv oculto"></div>
    <div class="panel panel-primary">
		<div class="panel-heading">
			<h3 id="panel-title" class="panel-title">Datos</h3>
		</div>
		<div class="panel-body">
				<input type="hidden" id="user_id" name="user_id">
                <input type="hidden" id="accion" name="accion">
				<div class="form-group">
					<div class="container-fluid">
						<div class="col-xs-4">
							<div class="form-group">
								<div class="input-group">
									<span for="user_name" class="input-group-addon">Usuario</span>
									<input type="text" class="form-control" placeholder="Usuario" id="user_name" name="user_name" aria-required="true" data-rule-required="true" data-rule-minlength="6" data-msg-minlength="El Usuario debe tener al menos 6 letras" data-msg-required="Ingrese un Usuario">
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span for="user_title" class="input-group-addon">Nombre Usuario</span>
									<input type="text" class="form-control" placeholder="Nombre de Usuario" id="user_title" name="user_title" aria-required="true" data-rule-required="true" data-msg-required="Ingrese un Nombre para el Usuario">
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span for="rol_id" class="input-group-addon">Roles</span>
									<select class="form-control selectpicker" title='Selecione Rol' id="rule_id" name="rule_id" aria-required="true" data-rule-required="true" data-msg-required="Seleccione un  Pol para Usuario">
										<?php
										$sql = "Select rule_id\n";
										$sql.= "	  ,rule_name\n";
										$sql.= "From sys_rule\n";
										$sql.= "Where rule_lock = 'N'\n";
										$sql.= "  and rule_sta = 'S'\n";
										$db->setQuery( $sql );
										$rows = $db->loadObjectList();
										for($i=0; $i < count($rows); $i++) {
											echo "<option value=\"" . $rows[$i]->rule_id . "\" >" .  $rows[$i]->rule_name . "</option>\n";
										} 		 
										?>	
									</select>						
								</div>
							</div>
						</div>
						<div class="col-xs-4 col-md-offset-1">
							<div class="form-group">
								<div class="input-group">
									<span for="user_email" class="input-group-addon">Correo</span>
									<input type="email" class="form-control" placeholder="ejemplo@ejemplo.com" id="user_email" name="user_email" aria-required="true" data-rule-required="true" data-rule-email="true" data-msg-required="Ingrese un correo" data-msg-email="Ingrese un correo Valido">
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span for="user_pass" class="input-group-addon">Contraseña</span>
									<input type="password" class="form-control" placeholder="Passwords" id="user_pass" name="user_pass" data-rule-minlength="8" data-msg-minlength="La contraseña de be tener al menos 8 Caracteres">
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span for="confirm_user_pass" class="input-group-addon">Confirmar Contraseña</span>
									<input type="password" class="form-control" placeholder="Confirma Passwords" id="confirm_user_pass" name="confirm_user_pass" equalto="#user_pass" data-rule-minlength="8" data-msg-minlength="La contraseña de be tener al menos 8 Caracteres">
								</div>
							</div>
							<?php if($mod->getStatus()) { ?>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">Estatus</span>
									<input type="radio" class="form-control" id="user_sta" name="user_sta" value="S" data-on-color="success" data-label-text="Activo">
									<input type="radio" class="form-control" id="user_sta" name="user_sta" value="N" data-on-color="danger" data-label-text="Inactivo">
								</div>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-body">
			<div class="container-fluid">
				<div class="btn-group">
					<button type="submit" class="btn btn-success">
						<i class="glyphicon glyphicon-ok"></i>
						Guardar
					</button>
					<button type="button" id="usersList" class="btn btn-info">
						<i class="glyphicon glyphicon-th-list"></i>
						Lista
					</button>
				</div>
			</div>
		</div>
	</div>
</form>