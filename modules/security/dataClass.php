<?php
class JUsers {
    private $user_id		= null;
    private $user_title		= null;
    private $user_email		= null;
    private $user_name		= null;
    private $user_pass		= null;
    private $registerDate	= null;
    private $lastvisitDate	= null;
    private $user_lock		= "N";
    private $user_sta		= "S";
    private $rule_id		= null;
    private $ter_name		= null;
    private $third_id		= null;
    private $user_try		= 0;
    private $accion			= null;
    private $view			= "list";

    function __construct($datos = array()) {
		$this->requestUsers($datos);
	}
    public function getDisable($tipo = "campo") {
    	$mod  = JFactory::getModulo();
    	if($this->accion == "add" && !$mod->getInsert()) {
			return "disabled";
		} else if($this->accion == "upd" && !$mod->getUpdate()) {
			return "disabled";
		} else if($this->accion == "del" && $tipo=="campo") {
			return "disabled";
		} else if($this->accion == "del" && !$mod->getDelete()) {
			return "disabled";
		}
    	return "";
	}
	public function getClassAtribute() {
        return get_object_vars($this);
    }
    public function requestUsers($datos = array()) {
		if(count($datos) > 0) {
			foreach($datos as $key => $value) {
				$this->$key =  $value;
			}
		}
    }
	public function getUsersPass() {
		$sql = "Select u.user_id\n";
		$sql.= "      ,u.user_pass\n";
		$sql.= "Where u.user_id is not null\n";
		$sql.= "  and u.user_id = '" . $this->user_id . "'\n";
		$db  = JFactory::getDBO();
		$db->setQuery($sql);
		$rows = $db->loadObjectList();
		$arrayDatos = array();
		if(count($rows) > 0) {
			$this->user_pass = $rows[0]->user_pass;
		}
		return $this->user_pass;
	}
    public function setUsers() {
    	$user = JFactory::getUser();
    	$mod  = JFactory::getModulo();
    	$db   = JFactory::getDBO();
		$temp = array("status" => "ERROR", "type" => "alert-danger", "text" => "NO TIENE ACCESO", "id" => "");
    	if($this->accion != "del") {
			$obj 	= new stdClass();
			$datos	= $this->getClassAtribute();
			if(count($datos) > 0) {
				foreach($datos as $key => $value) {
					if(($key != "user_pass" && $key != "confirm_user_pass" && $key != "accion" && $key != "view" && !is_null($value)) || ($key == "user_pass" && !empty($value))) {
						$obj->$key =  $value;
					}
				}
			}
    	  	if($this->accion == "add" && $mod->getInsert()) {
				$obj->registerDate  = 'NOW_AHORA';
				if(!$db->insertObject('sys_user', $obj,'user_id')) {
					$temp = $db->getErrorMsg();
				} else {
					$this->user_id  = $obj->user_id;
					$temp["id"]  	= $obj->user_id;
					$temp["status"]	= "OK";
					$temp["text"]  	= "Operacion Satisfactoria";
					$temp["type"]  	= "alert-info";
				}
			} else if($mod->getUpdate()) {
				$obj->user_id   = (int)$this->user_id;
				if($this->user_sta == "S") {
					$obj->user_try = "0";
				}
				if(!$db->updateObject('sys_user', $obj,"user_id")) {
					$temp["id"]  	= "";
					$temp["text"]  	= $db->getErrorMsg();
				} else {
					$temp["id"]  	= $obj->user_id;
					$temp["status"]	= "OK";
					$temp["text"]  	= "Operacion Satisfactoria";
					$temp["type"]  	= "alert-info";
				}
			}
		} else {
    	  	$sql = "DELETE FROM sys_user WHERE user_id = " . $this->user_id;
    	  	if(!$db->Execute($sql)) {					
				$temp["id"]  	= "";
				$temp["text"]  	= $db->getErrorMsg();
			} else {
				$temp["status"]	= "DELETE";
				$temp["text"]  	= "Operacion Satisfactoria";
				$temp["type"]  	= "alert-info";
			}
		}
    	return  $temp;
    }
	public function getUsers() {
    	$user = JFactory::getUser();
		$sql = "Select u.user_id\n";
		$sql.= "      ,u.user_name\n";
		$sql.= "      ,u.user_title\n";
		$sql.= "      ,u.user_email\n";
		$sql.= "      ,u.user_lock\n";
		$sql.= "      ,u.user_sta\n";
		$sql.= "      ,u.rule_id\n";
		$sql.= "      ,u.ter_name\n";
		$sql.= "      ,u.third_id\n";
		$sql.= "      ,u.user_try\n";
		$sql.= "      ,DATE_FORMAT(u.registerDate,'%d/%m/%Y') as registerDate\n";
		$sql.= "      ,DATE_FORMAT(u.lastvisitDate,'%d/%m/%Y %H:%i:%s') as lastvisitDate\n";
		$sql.= "      ,r.rule_name\n";
		$sql.= "      ,if(u.user_sta = 'S','Activo','Inactivo') as user_status\n";
		$sql.= "From sys_user u\n";
		$sql.= "Inner Join sys_rule r on r.rule_id = u.rule_id\n";
		$sql.= "Where u.user_id is not null\n";
		if($user->getId() != 1) {
			$sql.= "and u.user_lock = 'N'\n";
		}
		if(!is_null($this->user_id) && !empty($this->user_id)) {
			$sql.= "and u.user_id = " . $this->user_id . "\n";
		}
		if(!is_null($this->user_name) && !empty($this->user_name)) {
			$sql.= "and u.user_name like '%" . $this->user_name . "%'\n";
		}
		if(!is_null($this->user_email) && !empty($this->user_email)) {
			$sql.= "and u.user_email like '%" . $this->user_email . "%'\n";
		}
		if(!is_null($this->rule_id)) {
			if(is_array($this->rule_id)) {
				$sql.= "and u.rule_id in (" . implode($this->rule_id,",") . ")\n";
			} else if(!empty($this->rule_id)){
				$sql.= "and u.rule_id = '" . $this->rule_id . "'\n";
			}
		}
		$db  = JFactory::getDBO();
		$db->setQuery($sql);
		$rows = $db->loadObjectList();
		$arrayDatos = array();
		if($this->view == "modify") {
			$arrayDatos = $this->getClassAtribute();
			if(count($rows) == 1 && $this->accion != "add") {
				for($i=0; $i < count($rows); $i++) {
					$iT = 0;
					foreach($rows[$i] as $key => $value) {
						$arrayDatos[$iT] 	= $value;
						$arrayDatos[$key] 	= $value;
						$iT++;
					}
				}
			}
		} else if($this->view == "list"){
			if(count($rows) > 0) {
				for($i=0; $i < count($rows); $i++) {
					$iT = 0;
					$arrayItemDatos = Array();
					foreach($rows[$i] as $key => $value) {
						$arrayItemDatos[$iT] 	= $value;
						$arrayItemDatos[$key] 	= $value;
						$iT++;
					}
					array_push($arrayDatos,$arrayItemDatos);
				}
			}
		}
		return $arrayDatos;
	}
}
class JRules {
    private $rule_id		= null;
    private $rule_name		= null;
    private $rule_sta		= "S";
    private $rule_lock		= "N";
    private $rule_desc		= null;
    private $rule_type		= null;
    private $accion			= null;
    private $menuCheked		= null;
    private $menuUnCheked	= null;
    private $arrayAccion	= null;
    private $view			= "list";
    
    function __construct($datos = array()) {
		$this->requestRules($datos);
	}
    public function getDisable($tipo = "campo") {
    	$mod  = JFactory::getModulo();
    	if($this->accion == "add" && !$mod->getInsert()) {
			return "disabled";
		} else if($this->accion == "upd" && !$mod->getUpdate()) {
			return "disabled";
		} else if($this->accion == "del" && $tipo=="campo") {
			return "disabled";
		} else if($this->accion == "del" && !$mod->getDelete()) {
			return "disabled";
		}
    	return "";
	}
	public function getClassAtribute() {
        return get_object_vars($this);
    }
    public function requestRules($datos = array()) {
		if(count($datos) > 0) {
			foreach($datos as $key => $value) {
				if(array_key_exists($key,$this->getClassAtribute())) {
					$this->$key =  $value;
				}
			}
		}
    }
    public function setRules() {
    	$user = JFactory::getUser();
    	$mod  = JFactory::getModulo();
    	$db   = JFactory::getDBO();
		$temp = array("status" => "ERROR", "type" => "alert-danger", "text" => "NO TIENE ACCESO", "id" => "");
    	if($this->accion != "del") {
			$obj 	= new stdClass();
			$datos	= $this->getClassAtribute();
			if(count($datos) > 0) {
				foreach($datos as $key => $value) {
					if(!is_null($value) && $key != "menuCheked" && $key != "menuUnCheked" && $key != "arrayAccion" && $key != "accion" && $key != "view") {
						$obj->$key =  $value;
					}
				}
			}
    	  	if($this->accion == "add" && $mod->getInsert()) {
				if(!$db->insertObject('sys_rule', $obj,'rule_id')) {
					$temp = $db->getErrorMsg();
				} else {
					$this->rule_id  = $obj->rule_id;
					$temp["id"]  	= $obj->rule_id;
					$temp["status"]	= "OK";
					$temp["text"]  	= "Operacion Satisfactoria";
					$temp["type"]  	= "alert-info";
				}   	  	  	
			} else if($mod->getUpdate()) {
				$obj->rule_id   = (int)$this->rule_id;
				if(!$db->updateObject('sys_rule', $obj,"rule_id")) {
					$temp["id"]  	= $obj->rule_id;
					$temp["status"]	= "ERROR";
					$temp["text"]  	= $db->getErrorMsg();
					$temp["type"]  	= "alert-danger";
				} else {
					$temp["id"]  	= $obj->rule_id;
					$temp["status"]	= "OK";
					$temp["text"]  	= "Operacion Satisfactoria";
					$temp["type"]  	= "alert-info";
				}
			}
			if(!is_null($this->menuCheked) && !empty($this->menuCheked)) {
				$sql  = "Insert into sys_rule_accion (rule_id,accion_type,accion_item,accion,user_id,user_fec)\n";
				$sql .= "Select '" . $this->rule_id . "', 'MEN', m.men_id, 'E', '" . $user->getId() . "', now()\n";
				$sql .= "From sys_menu m\n";
				$sql .= "Where  m.men_id in (" . $this->menuCheked . ")\n";
				$sql .= "  And  m.men_id not in (Select x.accion_item\n";
				$sql .= "                   From sys_rule_accion x\n";
				$sql .= "                   Where x.rule_id  = '" . $this->rule_id . "'\n";
				$sql .= "                     And x.accion_type = 'MEN')\n";
				if(!$db->Execute($sql)) {
					$temp["id"]  	= $obj->rule_id;
					$temp["status"]	= "ERROR";
					$temp["text"]  	= $db->getErrorMsg();
					$temp["type"]  	= "alert-danger";
				} else {
					$temp["id"]  	= $obj->rule_id;
					$temp["status"]	= "OK";
					$temp["text"]  	= "Operacion Satisfactoria";
					$temp["type"]  	= "alert-info";
				}
			}
			if(!is_null($this->menuUnCheked) && !empty($this->menuUnCheked)) {
				$sql = "Delete From sys_rule_accion Where rule_id = '" . $this->rule_id . "' and accion_item in (" . $this->menuUnCheked . ")"; 
				if(!$db->Execute($sql)) {
					$temp["id"]  	= $obj->rule_id;
					$temp["status"]	= "ERROR";
					$temp["text"]  	= $db->getErrorMsg();
					$temp["type"]  	= "alert-danger";
				} else {
					$temp["id"]  	= $obj->rule_id;
					$temp["status"]	= "OK";
					$temp["text"]  	= "Operacion Satisfactoria";
					$temp["type"]  	= "alert-info";
				}
			}
			if(!is_null($this->arrayAccion) && is_array($this->arrayAccion)) {
				if(count($this->arrayAccion) > 0) {
					foreach($this->arrayAccion as $key => $value){
						$sql = "Select accion\n";
						$sql.= "From sys_rule_accion\n";
						$sql.= "Where rule_id = '" . $this->rule_id . "'\n";
						$sql.= "  and accion_item = '" . $key . "'\n";
						$sql.= "  and accion_type = 'MOD'\n";
						$db->setQuery($sql);
						$rows = $db->loadObjectList();
						if(count($rows) > 0) {
							if($rows[0]->accion != implode(";", $value)) {
								$sql = "Update sys_rule_accion set accion = '" . implode(";", $value) . "' WHERE rule_id = '" . $this->rule_id . "' and accion_item = '" . $key . "'";
								if(!$db->Execute($sql)) {
									$temp["id"]  	= $obj->rule_id;
									$temp["status"]	= "ERROR";
									$temp["text"]  	= $db->getErrorMsg();
									$temp["type"]  	= "alert-danger";
								} else {
									$temp["id"]  	= $obj->rule_id;
									$temp["status"]	= "OK";
									$temp["text"]  	= "Operacion Satisfactoria";
									$temp["type"]  	= "alert-info";
								}
							}
						} else {
							$sql  = "Insert into sys_rule_accion (rule_id,accion_type,accion_item,accion,user_id,user_fec) Values('" . $this->rule_id . "','MOD','" . $key . "','" . implode(";", $value) . "','" . $user->getId() . "',now())\n";
							if(!$db->Execute($sql)) {
								$temp["id"]  	= $obj->rule_id;
								$temp["status"]	= "ERROR";
								$temp["text"]  	= $db->getErrorMsg();
								$temp["type"]  	= "alert-danger";
							} else {
								$temp["id"]  	= $obj->rule_id;
								$temp["status"]	= "OK";
								$temp["text"]  	= "Operacion Satisfactoria";
								$temp["type"]  	= "alert-info";
							}
						}
					}
				}
			}
		} else {
    	  	$sql = "DELETE FROM sys_rule WHERE rule_id = " . $this->rule_id;
    	  	if(!$db->Execute($sql)) {
				$temp["id"]  	= "";
				$temp["status"]	= "ERROR";
				$temp["text"]  	= $db->getErrorMsg();
				$temp["type"]  	= "alert-danger";
			} else {
				$temp["status"]	= "DELETE";
				$temp["text"]  	= "Operacion Satisfactoria";
				$temp["type"]  	= "alert-info";
			}
		}
    	return  $temp;
    }
	public function getRules() {
    	$user = JFactory::getUser();
		$sql = "Select r.rule_id\n";
		$sql.= "      ,r.rule_name\n";
		$sql.= "      ,r.rule_lock\n";
		$sql.= "      ,r.rule_sta\n";
		$sql.= "      ,r.rule_desc\n";
		$sql.= "      ,r.rule_type\n";
		$sql.= "      ,if(r.rule_sta = 'S','Activo','Inactivo') as rule_status\n";
		$sql.= "From sys_rule r\n";
		$sql.= "Where r.rule_id is not null\n";
		if($user->getId() != 1) {
			$sql.= "and r.rule_lock = 'N'\n";
		}
		if(!is_null($this->rule_id) && !empty($this->rule_id)) {
			$sql.= "and r.rule_id = '" . $this->rule_id . "'\n";
		}
		if(!is_null($this->rule_name) && !empty($this->rule_name)) {
			$sql.= "and r.rule_name like '%" . $this->rule_name . "%'\n";
		}
		$db  = JFactory::getDBO();
		$db->setQuery($sql);
		$rows = $db->loadObjectList();
		$arrayDatos = array();
		if($this->view == "modify") {
			$arrayDatos = $this->getClassAtribute();
			if(count($rows) == 1 && $this->accion != "add") {
				for($i=0; $i < count($rows); $i++) {
					$iT = 0;
					foreach($rows[$i] as $key => $value) {
						$arrayDatos[$iT] 	= $value;
						$arrayDatos[$key] 	= $value;
						$iT++;
					}
				}
			}
		} else if($this->view == "list"){
			if(count($rows) > 0) {
				for($i=0; $i < count($rows); $i++) {
					$iT = 0;
					$arrayItemDatos = Array();
					foreach($rows[$i] as $key => $value) {
						$arrayItemDatos[$iT] 	= $value;
						$arrayItemDatos[$key] 	= $value;
						$iT++;
					}
					array_push($arrayDatos,$arrayItemDatos);
				}
			}
		}
		return $arrayDatos;
	}
	public function ruleMenuSubNivel($id,$sep) { 
		$temp	= "";
		$db		= JFactory::getDBO();
		$user	= JFactory::getUser();
		$rol	= $user->getRolId();
		$sql = "Select m.men_id\n";
		$sql.= "	  ,m.men_name\n";
		$sql.= "	  ,m.men_desc\n";
		$sql.= "	  ,m.men_type\n";
		$sql.= "	  ,m.men_accion\n";
		$sql.= "	  ,m.men_target\n";
		$sql.= "	  ,m.men_order\n";
		$sql.= "	  ,m.men_icon\n";
		$sql.= "	  ,if(r.accion is null,false,true) as menCheck\n";
		$sql.= "From sys_menu m\n";
		$sql.= "Left Join sys_rule_accion r on r.accion_item = m.men_id and r.accion_type = 'MEN' and r.rule_id = '" . $this->rule_id . "'\n";
		$sql.= "Where  m.men_father = " . $id . "\n";
		$sql.= "  and  m.men_sta = 'S'\n";
		$sql.= "  and  m.men_class = 'L'\n";
		$sql.= "Order by m.men_order\n";
		$db->setQuery( $sql );
		$rows = $db->loadObjectList();
		$arrow = true;
		for($i = 0; $i < count($rows) ; $i++) {
			$men_name = $rows[$i]->men_name;
			if($rows[$i]->men_type == "SUB") {
				$temp2 = $this->ruleMenuSubNivel($rows[$i]->men_id,$sep . "  ");
				if(!empty($temp2)) {
					$target = "dropdownMenu" . $rows[$i]->men_id;
					$temp .= $sep . "\t\t\t<li id=\"" . $rows[$i]->men_id . "\" data-jstree='{\"disabled\":true,\"idMenu\":" . $rows[$i]->men_id . ",\"checkMenu\":" . $rows[$i]->menCheck . ",\"opened\":true,\"selected\":" . $rows[$i]->menCheck . (!empty($rows[$i]->men_icon)?",\"icon\":\"glyphicon " . $rows[$i]->men_icon . "\"":"") . "}'>\n";
					$temp .=  "&nbsp;&nbsp;" . $men_name;
					$temp .= "\n" . $sep . "  <ul>\n" . $temp2 . $sep . "  </ul>" . $sep . "  </li>\n";
				}
			} else {
				$temp .= $sep . "\t\t\t<li id=\"" . $rows[$i]->men_id . "\" data-jstree='{\"idMenu\":" . $rows[$i]->men_id . ",\"checkMenu\":" . $rows[$i]->menCheck . ",\"opened\":true,\"selected\":" . $rows[$i]->menCheck . (!empty($rows[$i]->men_icon)?",\"icon\":\"glyphicon " . $rows[$i]->men_icon . "\"":"") . "}'>\n";
				$temp .= "&nbsp;&nbsp;" . $men_name . "</li>\n";			
			}
		}
		return $temp;
	}
}