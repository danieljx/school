<?php
	$target = JFunc::FTarget();
	$mod	= JFactory::getModulo();
	$db		= JFactory::getDBO();
	$user	= JFactory::getUser();
	$back	= JRequest::getVar("back");
	if(!empty($back)) {
		$back	= json_decode($back,true);
	} else {
		$back	= json_decode("[]",true);
	}
	$back["view"]		= "list";
	$rules 	= new JRules($back);
?>
<script>
	objDataBack   = <?php echo json_encode($back); ?>;
	arrayDataList = <?php echo json_encode($rules->getRules()); ?>;
</script>
<h2 class="page-header">Roles</h2>
<div class="panel panel-info">
	<div class="panel-heading">
		<h3 id="panel-title" class="panel-title">Filtro de Roles</h3>
	</div>
	<div class="panel-body">
		<form id="rolesFilter" data-module="security" data-vista="rulesData" class="form-horizontal formList" method="POST" target="<?PHP echo $target; ?>">
			<div class="form-group">
				<div class="col-xs-4">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-tasks"></span></span>
						<input type="text" class="form-control" placeholder="Nombre Roles" name="rule_name">
					</div>
				</div>
				<div class="col-xs-4">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-stats"></span></span>
						<select class="form-control selectpicker" title='Selecione Estatus' name="rule_sta" id="rule_sta" multiple>
							<option value="S">Activo</option>
							<option value="N">Inactivo</option>
						</select>						
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="container-fluid">
					<div class="btn-group">
						<button type="submit" class="btn btn-primary">
							<i class="glyphicon glyphicon-filter"></i>
							Filtrar
						</button>
						<button type="reset" class="btn btn-warning">
							<i class="glyphicon glyphicon-erase"></i>
							Limpiar
						</button>
						<?php if($mod->insert) { ?>
						<button id="newRule" type="button" class="btn btn-info">
							<i class="glyphicon glyphicon-edit"></i>
							Nuevo
						</button>
						<?php } ?>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<div class="container-fluid">
	<div class="table-responsive">
		<table id="rulesTabla" class="table dataTableList table-bordered" data-update="<?php echo $mod->insert; ?>" data-delete="<?php echo $mod->delete; ?>">
			<thead>
				<tr>
					<th data-visible="false" data-type="num" data-class="rule_id" data-bSearchable="false" data-bSortable="false">#</th>
					<th data-type="string" data-class="rule_name" data-bSearchable="true" data-bSortable="true">Rol</th>
					<th data-type="string" data-class="rule_status" data-bSearchable="true" data-bSortable="true">Estatus</th>
					<th data-type="string" data-class="accion" data-bSearchable="false" data-bSortable="false">Accion</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
</div>

