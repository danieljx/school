<?php
	$target = JFunc::FTarget();
	$mod	= JFactory::getModulo();
	$db		= JFactory::getDBO();
	$user	= JFactory::getUser();
	$datos	= json_decode(JRequest::getVar("datos"),true);
	$datos["view"]	= "modify";
	$back	= json_decode(JRequest::getVar("back"),true);
	$thirds  = new JThirds($datos);
	$thirdData = $thirds->getThirds();
    $thirdData["accion"]	= (isset($datos["accion"])?$datos["accion"]:"add");
?>
<script>
	objDataBack   	= <?php echo json_encode($back); ?>;
	objDataModify 	= <?php echo json_encode($thirdData); ?>;
</script>
<div class="col-xs-2">
	<ul class="nav nav-tabs tabs-left">
		<li class="active">
			<a href="#thirdsData" data-toggle="tab">Datos</a>
		</li>
		<li>
			<a href="#thirdsSecc" data-toggle="tab">Cursos</a>
		</li>
	</ul>
	<div class="profile-header-container">
		<div class="profile-header-img">
			<?php
				if(!empty($thirdData["third_img"]) && file_exists("front/images/profile/" . $thirdData["third_path"] . "/profile/" . $thirdData["third_img"])) {
					echo "			<span class=\"img-circle img-circle-bg\" style=\"background-image: url('front/images/profile/" . $thirdData["third_path"] . "/profile/" . $thirdData["third_img"] . "')\"></span>\n";
				} else {
					echo "			<span class=\"img-circle glyphicon glyphicon-user\"></span>\n";
				}
			?>
		</div>
	</div>
</div>
<div class="col-xs-10">
	<form id="thirdModify" data-id="third_id" data-module="third" data-vista="teachersUp" class="form-horizontal formModify" method="POST" target="<?PHP echo $target; ?>">
        <div class="alertDiv oculto"></div>
        <div class="tab-content">
			<div class="tab-pane active" id="thirdsData">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 id="panel-title" class="panel-title">Datos</h3>
					</div>
					<div class="panel-body">
						<input type="hidden" id="third_id" name="third_id">
                        <input type="hidden" id="accion" name="accion">
						<div class="form-group">
							<div class="container-fluid">
								<div class="col-xs-5">
									<div class="form-group">
										<div class="input-group">
											<span for="third_name" class="input-group-addon">Nombres</span>
											<input type="text" class="form-control" placeholder="Nombres" id="third_name" name="third_name" aria-required="true" data-third-required="true" data-msg-required="Ingrese un Nombre al Profesor" data-rule-maxlength="150" data-msg-maxlength="No ingrese mas de 150 Caracteres">
										</div>
									</div>
									<div class="form-group">
										<div class="input-group">
											<span for="third_lastname" class="input-group-addon">Apellidos</span>
											<input type="text" class="form-control" placeholder="Apellidos" id="third_lastname" name="third_lastname" aria-required="true" data-third-required="true" data-msg-required="Ingrese un Apellidos al Profesor" data-rule-maxlength="150" data-msg-maxlength="No ingrese mas de 150 Caracteres">
										</div>
									</div>
									<div class="form-group">
										<div class="input-group">
											<span for="third_rif" class="input-group-addon">CI/Rif</span>
											<input type="text" class="form-control" placeholder="CI/Rif" id="third_rif" name="third_rif" aria-required="true" data-third-required="true" data-msg-required="Ingrese una Cedula" data-rule-maxlength="8" data-msg-maxlength="No ingrese mas de 8 Caracteres">
										</div>
									</div>
									<div class="form-group">
										<div class="input-group">
											<span for="third_birth" class="input-group-addon">Fecha de Nacimiento</span>
											<input type="text" class="form-control datepicker" placeholder="DD/MM/YYYY" id="third_birth" name="third_birth" aria-required="true" data-rule-required="true" data-msg-required="Ingrese Fecha Nacimiento" data-rule-maxlength="10" data-msg-maxlength="No ingrese mas de 10 Caracteres">
										</div>
									</div>
									<div class="form-group">
										<div class="input-group" style="z-index: 7;">
											<span class="input-group-addon">Sexo</span>
											<select class="form-control selectpicker" title='Selecione Sexo' name="third_sex" id="third_sex" data-rule-required="true" data-rule-email="true" data-msg-required="Ingrese El Sexo">
												<option value="M">Masculino</option>
												<option value="F">Femenino</option>
											</select>
										</div>
									</div>
								<?php if($mod->getStatus()) { ?>
									<div class="form-group">
										<div class="input-group" style="z-index: 6;">
											<span class="input-group-addon">Estatus</span>
											<select class="form-control selectpicker" title='Selecione Estatus' name="third_sta" id="third_sta">
												<?php
												$sql = "Select sta_id\n";
												$sql.= "	  ,sta_name\n";
												$sql.= "From mod_third_status\n";
												$sql.= "Where sta_id in (1,2,3)\n";
												$db->setQuery( $sql );
												$rows = $db->loadObjectList();
												for($i=0; $i < count($rows); $i++) {
													echo "<option value=\"" . $rows[$i]->sta_id . "\" >" .  $rows[$i]->sta_name . "</option>\n";
												}
												?>
											</select>
										</div>
									</div>
								<?php } ?>
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon">Nacionalidad</span>
											<select class="form-control selectpicker" title='Selecione Nacionalidad' name="third_nationallity" id="third_nationallity">
												<option value="V">Venezolano</option>
												<option value="E">Extranjero</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-xs-5 col-md-offset-1">							
									<div class="form-group">
										<div class="input-group">
											<span for="third_email" class="input-group-addon">Correo</span>
											<input type="email" class="form-control" placeholder="ejemplo@ejemplo.com" id="third_email" name="third_email" aria-required="true" data-rule-required="true" data-rule-email="true" data-msg-required="Ingrese un correo" data-msg-email="Ingrese un correo Valido" data-rule-maxlength="150" data-msg-maxlength="No ingrese mas de 150 Caracteres">
										</div>
									</div>
									<div class="form-group">
										<div class="input-group">
											<span for="third_phone" class="input-group-addon">Teléfono</span>
											<input type="text" class="form-control" placeholder="Telefono" id="third_phone" name="third_phone" data-rule-number="true" data-msg-number="Solo Números" aria-required="true" data-rule-required="true" data-msg-required="Ingrese un N&uaccute;mero de Teléfono" data-rule-maxlength="12" data-msg-maxlength="No ingrese mas de 12 Caracteres">
										</div>
									</div>
									<div class="form-group">
										<div class="input-group">
											<span for="third_address" class="input-group-addon">Dirección</span>
											<textarea class="form-control" placeholder="Dirección" id="third_address" name="third_address" data-rule-maxlength="255" data-msg-maxlength="No ingrese mas de 200 Caracteres"></textarea>
										</div>
									</div>
									<div class="form-group">
										<div class="input-group">
											<span for="third_description" class="input-group-addon">Descripción</span>
											<textarea class="form-control" placeholder="Descripción" id="third_description" name="third_description" data-rule-maxlength="255" data-msg-maxlength="No ingrese mas de 200 Caracteres"></textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="tab-pane" id="thirdsSecc">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 id="panel-title" class="panel-title">Secciones</h3>
					</div>
					<div class="panel-body">
						<div class="col-xs-10">
							<div id="seccThirds">
							//
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="tab-pane" id="thirdsCalendar">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 id="panel-title" class="panel-title">Horario</h3>
					</div>
					<div class="panel-body">
						<div class="panel-group" id="calendar">
							//
						</div>					
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-default">
		  <div class="panel-body">
			<div class="container-fluid">
				<div class="btn-group">
					<button type="submit" class="btn btn-success">
						<i class="glyphicon glyphicon-ok"></i>
						Guardar
					</button>
					<button type="button" id="thirdList" class="btn btn-info">
						<i class="glyphicon glyphicon-th-list"></i>
						Lista
					</button>
				</div>
			</div>
		  </div>
		</div>
	</form>
</div>