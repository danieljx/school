<?php
	$target = JFunc::FTarget();
	$mod	= JFactory::getModulo();
	$db		= JFactory::getDBO();
	$user	= JFactory::getUser();
	$datos	= json_decode(JRequest::getVar("datos"),true);
	$datos["view"]	= "modify";
	$back	= json_decode(JRequest::getVar("back"),true);
	$thirds  = new JThirds($datos);
	$thirdData = $thirds->getThirds();
    $thirdData["accion"]	= (isset($datos["accion"])?$datos["accion"]:"add");

	$sql = "Select desc_id\n";
	$sql.= "	  ,desc_text\n";
	$sql.= "From mod_note_desc\n";
	$sql.= "Where desc_id is not null\n";
	$db->setQuery( $sql );
	$dataDescNote = $db->loadObjectList();
?>
<script>
	objDataBack   	    = <?php echo json_encode($back); ?>;
	objDataModify 	    = <?php echo json_encode($thirdData); ?>;
	dataDescNote 	    = <?php echo json_encode($dataDescNote); ?>;
    var representTablaList;
    var arrayDataRepresentTabla 	= <?php echo json_encode($thirds->getThirdRepresents($thirdData["third_id"])); ?>;
</script>
<div class="col-xs-2">
	<ul class="nav nav-tabs tabs-left">
		<li class="active">
			<a href="#thirdsData" data-toggle="tab">Datos</a>
		</li>
		<li>
			<a href="#thirdsRepresent" data-toggle="tab">Representantes</a>
		</li>
		<li>
			<a href="#thirdsCurso" data-toggle="tab">Notas</a>
		</li>
	</ul>
	<div class="profile-header-container">
		<div class="profile-header-img">
			<?php
				if(!empty($thirdData["third_img"]) && file_exists("front/images/profile/" . $thirdData["third_path"] . "/profile/" . $thirdData["third_img"])) {
					echo "			<span class=\"img-circle img-circle-bg\" style=\"background-image: url('front/images/profile/" . $thirdData["third_path"] . "/profile/" . $thirdData["third_img"] . "')\"></span>\n";
				} else {
					echo "			<span class=\"img-circle glyphicon glyphicon-user\"></span>\n";
				}
			?>
		</div>
	</div>
</div>
<div class="col-xs-10">
	<form id="thirdModify" data-id="third_id" data-module="third" data-vista="studentsUp" class="form-horizontal formModify" method="POST" target="<?PHP echo $target; ?>">
        <div class="alertDiv oculto"></div>
        <div class="tab-content">
			<div class="tab-pane active" id="thirdsData">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 id="panel-title" class="panel-title">Datos</h3>
					</div>
					<div class="panel-body">
						<input type="hidden" id="third_id" name="third_id">
						<input type="hidden" id="accion" name="accion">
						<input type="hidden" id="third_sta_description" name="third_sta_description">
						<div class="form-group">
							<div class="container-fluid">
								<div class="col-xs-5">
									<div class="form-group">
										<div class="input-group">
											<span for="third_name" class="input-group-addon">Nombres</span>
											<input type="text" class="form-control" placeholder="Nombres" id="third_name" name="third_name" aria-required="true" data-rule-required="true" data-msg-required="Ingrese un Nombre al Estudiante" data-rule-maxlength="150" data-msg-maxlength="No ingrese mas de 150 Caracteres">
										</div>
									</div>
									<div class="form-group">
										<div class="input-group">
											<span for="third_lastname" class="input-group-addon">Apellidos</span>
											<input type="text" class="form-control" placeholder="Apellidos" id="third_lastname" name="third_lastname" aria-required="true" data-rule-required="true" data-msg-required="Ingrese un Apellidos al Estudiante" data-rule-maxlength="150" data-msg-maxlength="No ingrese mas de 150 Caracteres">
										</div>
									</div>
									<div class="form-group">
										<div class="input-group">
											<span for="third_rif" class="input-group-addon">Cedula Escolar</span>
											<input type="text" class="form-control" placeholder="Cedula Escolar" id="third_rif" name="third_rif" aria-required="true" data-rule-required="true" data-msg-required="Ingrese una Cedula" data-rule-maxlength="8" data-msg-maxlength="No ingrese mas de 8 Caracteres">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-barcode third_rif"></span></span>
										</div>
									</div>
									<div class="form-group">
										<div class="input-group">
											<span for="third_birth" class="input-group-addon">Fecha de Nacimiento</span>
											<input type="text" class="form-control" placeholder="DD/MM/YYYY" id="third_birth" name="third_birth" aria-required="true" data-rule-required="true" data-msg-required="Ingrese Fecha Nacimiento" data-rule-maxlength="10" data-msg-maxlength="No ingrese mas de 10 Caracteres">
										</div>
									</div>
									<div class="form-group">
										<div class="input-group" style="z-index: 7;">
											<span class="input-group-addon">Sexo</span>
											<select class="form-control selectpicker" title='Selecione Sexo' name="third_sex" id="third_sex" data-rule-required="true" data-rule-email="true" data-msg-required="Ingrese El Sexo">
												<option value="M">Masculino</option>
												<option value="F">Femenino</option>
											</select>
										</div>
									</div>
								<?php if($mod->getStatus()) { ?>
									<div class="form-group">
										<div class="input-group" style="z-index: 6;">
											<span class="input-group-addon">Estatus</span>
											<select class="form-control selectpicker" title='Selecione Estatus' valOld='<?php echo $thirdData["third_sta"]; ?>' name="third_sta" id="third_sta" style="z-index: 1 !important;" data-rule-required="true" data-rule-email="true" data-msg-required="Ingrese el Estatus">
												<?php
												$sql = "Select sta_id\n";
												$sql.= "	  ,sta_name\n";
												$sql.= "From mod_third_status\n";
												$sql.= "Where sta_id is not null\n";
												$db->setQuery( $sql );
												$rows = $db->loadObjectList();
												for($i=0; $i < count($rows); $i++) {
													echo "<option value=\"" . $rows[$i]->sta_id . "\" >" .  $rows[$i]->sta_name . "</option>\n";
												}
												?>
											</select>
										</div>
									</div>
								<?php } ?>
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon">Nacionalidad</span>
											<select class="form-control selectpicker" title='Selecione Nacionalidad' name="third_nationallity" id="third_nationallity" data-rule-required="true" data-rule-email="true" data-msg-required="Ingrese La Nacionalidad">
												<option value="V">Venezolano</option>
												<option value="E">Extranjero</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-xs-5 col-md-offset-1">							
									<div class="form-group">
										<div class="input-group">
											<span for="third_email" class="input-group-addon">Correo</span>
											<input type="email" class="form-control" placeholder="ejemplo@ejemplo.com" id="third_email" name="third_email" aria-required="true" data-rule-required="true" data-rule-email="true" data-msg-required="Ingrese un correo" data-msg-email="Ingrese un correo Valido" data-rule-maxlength="150" data-msg-maxlength="No ingrese mas de 150 Caracteres">
										</div>
									</div>
									<div class="form-group">
										<div class="input-group">
											<span for="third_phone" class="input-group-addon">Teléfono</span>
											<input type="text" class="form-control" placeholder="Teléfono" id="third_phone" name="third_phone" data-rule-number="true" data-msg-number="Solo Números" aria-required="true" data-rule-required="true" data-msg-required="Ingrese un Número de Teléfono" data-rule-maxlength="12" data-msg-maxlength="No ingrese mas de 12 Caracteres">
										</div>
									</div>
									<div class="form-group">
										<div class="input-group">
											<span for="third_address" class="input-group-addon">Dirección</span>
											<textarea class="form-control" placeholder="Dirección" id="third_address" name="third_address" data-rule-maxlength="255" data-msg-maxlength="No ingrese mas de 200 Caracteres"></textarea>
										</div>
									</div>
									<div class="form-group">
										<div class="input-group">
											<span for="third_description" class="input-group-addon">Descripción</span>
											<textarea class="form-control" placeholder="Descripción" id="third_description" name="third_description" data-rule-maxlength="255" data-msg-maxlength="No ingrese mas de 200 Caracteres"></textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="tab-pane" id="thirdsRepresent">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 id="panel-title" class="panel-title">Representantes</h3>
					</div>
					<div class="panel-body">
						<div class="panel-group" id="represent">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="container-fluid">
                                        <div class="btn-group" id="btnRepresent" style="display:<?php echo (empty($thirdData["third_id"])?"none":"inline-block");?>;">
                                            <?php if($mod->getInsert()) { ?>
                                                <button id="addRepresent" type="button" class="btn btn-primary" title="Nuevo Representante" onclick="addNewRepresent()">
                                                    <i class="glyphicon glyphicon-plus"></i>
                                                    Nuevo
                                                </button>
                                                <button id="listRepresent" type="button" class="btn btn-success" title="Lista Representantes" onclick="upSetRepresent()">
                                                    <i class="glyphicon glyphicon-list"></i>
                                                    Representantes
                                                </button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="container-fluid">
                                        <div class="table-responsive">
                                            <table id="representTabla" class="table table-bordered" data-update="<?php echo $mod->getUpdate(); ?>" data-delete="<?php echo $mod->getDelete(); ?>">
                                                <thead>
                                                <tr>
                                                    <th data-visible="false" data-type="num" data-class="third_id" data-bSearchable="false" data-bSortable="false">#</th>
                                                    <th data-type="string" data-class="third_names" data-bSearchable="false" data-bSortable="false">Nombre</th>
                                                    <th data-type="string" data-class="third_rif" data-bSearchable="false" data-bSortable="false">CI/RIF</th>
                                                    <th data-type="string" data-class="third_relName" data-bSearchable="false" data-bSortable="false">Relacion</th>
                                                    <th data-type="string" data-class="third_phone" data-bSearchable="false" data-bSortable="false">Telefono</th>
                                                    <th data-type="string" data-class="third_email" data-bSearchable="false" data-bSortable="false">E-Mail</th>
                                                    <th data-type="string" data-class="accion" data-bSearchable="false" data-bSortable="false">Accion</th>
                                                </tr>
                                                </thead>
                                                <tbody></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>					
					</div>
				</div>
			</div>
			<div class="tab-pane" id="thirdsCurso">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 id="panel-title" class="panel-title">Notas</h3>
					</div>
					<div class="panel-body">
						<div class="panel panel-default">
							<div class="panel-body">
                                <div class="panel-group" id="perdiodElective" role="tablist" aria-multiselectable="true">
                                    <?php
                                    $sql = "Select p.period_id\n";
                                    $sql.= "      ,p.period_code\n";
                                    $sql.= "      ,p.period_sta\n";
                                    $sql.= "      ,DATE_FORMAT(p.period_start,'%d/%m/%Y') as period_start\n";
                                    $sql.= "      ,DATE_FORMAT(p.period_end,'%d/%m/%Y') as period_end\n";
                                    $sql.= "      ,DATE_FORMAT(p.user_fec,'%d/%m/%Y %H:%i:%s') as user_fec\n";
                                    $sql.= "      ,concat(p.period_code,': ' ,DATE_FORMAT(p.period_start,'%d/%m/%Y'),' - ',DATE_FORMAT(p.period_end,'%d/%m/%Y')) as period_name\n";
                                    $sql.= "      ,p.user_id\n";
                                    $sql.= "      ,if(p.period_sta = 1,'Activo','Culminado') as period_status\n";
                                    $sql.= "      ,if(e.enroll_id is null,'No Inscrito','Inscrito') as enroll_status\n";
                                    $sql.= "      ,if(p.period_sta = 1,'info',if(e.enroll_id is not null,'success','danger')) as color_status\n";
                                    $sql.= "      ,if(e.enroll_id is null, 'NO','YES') as enroll\n";
                                    $sql.= "From mod_period p\n";
                                    $sql.= "Left Join mod_enroll e on e.period_id = p.period_id and e.third_id = '" . $thirdData["third_id"] . "'\n";
                                    $sql.= "Where p.period_id is not null\n";
                                    $sql.= "Order by p.period_start desc\n";
                                    $db->setQuery( $sql );
                                    $rows = $db->loadObjectList();
                                    $arrayAccion = Array();
                                    for($i = 0; $i < count($rows) ; $i++) {
                                        echo "<div class=\"panel panel-" . $rows[$i]->color_status . "\">\n";
                                        echo "	<div class=\"panel-heading\" role=\"tab\" id=\"headingPerdiodElective_" . $rows[$i]->period_id . "\">\n";
                                        echo "		<h4 class=\"panel-title\">\n";
                                        echo "			<a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#perdiodElective\" href=\"#collapseAccionModule_" . $rows[$i]->period_id . "\" aria-expanded=\"false\" aria-controls=\"collapsePerdiodElective_" . $rows[$i]->period_id . "\">\n";
                                        echo "			" . $rows[$i]->period_code . ": " . $rows[$i]->period_start . " - " . $rows[$i]->period_end . " : " . $rows[$i]->period_status . " - " . $rows[$i]->enroll_status . "\n";
                                        echo "			</a>\n";
                                        echo "		</h4>\n";
                                        echo "	</div>\n";
                                        echo "	<div id=\"collapseAccionModule_" . $rows[$i]->period_id . "\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-expanded=\"false\" aria-labelledby=\"headingPerdiodElective_" . $rows[$i]->period_id . "\">\n";
                                        echo "		<div class=\"panel-body\">\n";
                                        echo "		    <div class=\"panel panel-default\">\n";
                                        echo "		        <div class=\"panel-body\">\n";
                                        echo "		            <div class=\"container-fluid\">\n";
                                        echo "		                <div class=\"btn-group\">\n";
                                        if($rows[$i]->period_sta == "1" && $rows[$i]->enroll == "NO") {
                                            echo "		                    <button type=\"button\" id=\"addEnroll\" third_id=\"" . $thirdData["third_id"] . "\" period_id=\"" . $rows[$i]->period_id . "\" class=\"btn btn-primary\">\n";
                                            echo "		                        <i class=\"glyphicon glyphicon-book\"></i>\n";
                                            echo "		                        Inscribir\n";
										    echo "		                    </button>\n";
                                            echo "		                    <button type=\"button\" id=\"addEnrollRight\" style=\"height:34px;\" class=\"btn btn-primary dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n";
                                            echo "		                        <span class=\"caret\"></span>\n";
                                            echo "		                        <span class=\"sr-only\">Toggle Dropdown</span>\n";
                                            echo "		                    </button>\n";
                                            echo "		                    <ul class=\"dropdown-menu\">\n";
                                            $sqll = "Select l.level_id\n";
                                            $sqll.= "	   ,l.level_name\n";
                                            $sqll.= "	   ,group_concat(concat(m.matter_id,'@Z@',m.matter_name,'@Z@',m.matter_code) separator '@X@') as matters\n";
                                            $sqll.= "From mod_level l\n";
                                            $sqll.= "Inner Join mod_matter m on m.matter_level = l.level_id\n";
                                            $sqll.= "Where l.level_type = 2\n";
                                            $sqll.= "  and l.level_id not in (Select x.level_id\n";
                                            $sqll.= "                         From mod_enroll x\n";
                                            $sqll.= "                         Where x.period_id = '" . $rows[$i]->period_id . "'\n";
                                            $sqll.= "                           and x.third_id = '" . $thirdData["third_id"] . "')\n";
                                            $sqll.= "Group by l.level_id\n";
                                            $db->setQuery( $sqll );
                                            $rowl = $db->loadObjectList();
                                            for($l=0; $l < count($rowl); $l++) {
                                                if(!empty($rowl[$l]->matters)) {
                                                    $matters = explode("@X@",$rowl[$l]->matters);
                                                    $arrayAccion = array();
                                                    for($m = 0; $m < count($matters); $m++) {
                                                        $mattersItem = explode("@Z@",$matters[$m]);
                                                        $arrayAccionItem = array();
                                                        $arrayAccionItem["matter_id"]   = $mattersItem[0];
                                                        $arrayAccionItem["matter_name"] = $mattersItem[1];
                                                        $arrayAccionItem["matter_code"] = $mattersItem[2];
                                                        array_push($arrayAccion, $arrayAccionItem);
                                                    }
                                                    echo "<script>var matters_" . $rowl[$l]->level_id . " = " . json_encode($arrayAccion) . ";</script>";
                                                }
                                                echo "		                    <li><a third_id=\"" . $thirdData["third_id"] . "\" period_id=\"" . $rows[$i]->period_id . "\" period_name=\"" . $rows[$i]->period_name . "\"  level_id=\"" . $rowl[$l]->level_id . "\" onclick=\"enrollThird(\$(this))\">" .  $rowl[$l]->level_name . "</a></li>\n";
                                            }
                                            echo "		                    </ul>\n";
                                        } else if($rows[$i]->enroll != "NO" && $mod->getPrint()) {
                                            echo "		                    <button id=\"printNotes_" . $rows[$i]->period_id . "\" third_id=\"" . $thirdData["third_id"] . "\" period_id=\"" . $rows[$i]->period_id . "\" type=\"button\" class=\"btn btn-warning pdf printNotes\" data-module=\"third\" data-view=\"studentsNotesdf\" data-title=\"Imprimir Notas\">\n";
                                            echo "		                        <i class=\"glyphicon glyphicon-print\"></i>\n";
                                            echo "		                        Notas\n";
                                            echo "		                    </button>\n";
                                        }
										if($rows[$i]->period_sta == "1") {
											echo "<input type=\"hidden\" id=\"period_id\" name=\"period_id\" value=\"" . $rows[$i]->period_id . "\">\n";
											echo "<input type=\"hidden\" id=\"period_sta\" name=\"period_sta\" value=\"" . $rows[$i]->period_sta . "\">\n";
										}
                                        echo "		                </div>\n";
                                        echo "		            </div>\n";
                                        echo "		        </div>\n";
                                        echo "		   </div>\n";
                                        echo "		   <div id=\"periodMatterContent_" . $rows[$i]->period_id . "\">\n";
                                        echo "		   <table id=\"periodMatter_" . $rows[$i]->period_id . "\" class=\"table table-bordered\">\n";
                                        echo "		       <thead>\n";
                                        echo "		           <tr>\n";
                                        echo "		               <th>Detalle</th>\n";
                                        echo "		               <th>Nivel</th>\n";
                                        echo "		               <th>Materias</th>\n";
                                        echo "		               <th>Seccion</th>\n";
                                        echo "		               <th>Asistencia</th>\n";
                                        echo "		               <th>Nota</th>\n";
                                        echo "		           </tr>\n";
                                        echo "		       </thead>\n";
                                        echo "		      <tbody>\n";
                                        $sqm = "Select tm.item_id\n";
                                        $sqm.= "      ,tm.third_id\n";
                                        $sqm.= "      ,tm.period_id\n";
                                        $sqm.= "      ,tm.sec_id\n";
                                        $sqm.= "      ,tm.matter_id\n";
                                        $sqm.= "      ,tm.item_sta\n";
                                        $sqm.= "      ,tm.item_note\n";
                                        $sqm.= "      ,DATE_FORMAT(tm.item_start,'%d/%m/%Y') as item_start\n";
                                        $sqm.= "      ,DATE_FORMAT(tm.item_end,'%d/%m/%Y') as item_end\n";
                                        $sqm.= "      ,DATE_FORMAT(tm.user_fec,'%d/%m/%Y %H:%i:%s') as user_fec\n";
                                        $sqm.= "      ,tm.user_id\n";
                                        $sqm.= "      ,l.level_id\n";
                                        $sqm.= "      ,l.level_name\n";
                                        $sqm.= "      ,m.matter_name\n";
                                        $sqm.= "      ,s.sec_code\n";
                                        $sqm.= "      ,n.note_class\n";
                                        $sqm.= "      ,s.sec_code\n";
                                        $sqm.= "      ,pm.item_id as teacher_third\n";
                                        $sqm.= "      ,ifnull(a.tassits,0) as tassits\n";
                                        $sqm.= "      ,ifnull(pm.item_assistance,0) as passits\n";
                                        $sqm.= "      ,FLOOR(ifnull((ifnull(a.tassits,0)*100)/ifnull(pm.item_assistance,0),0)) as assist\n";
                                        $sqm.= "From mod_third_matter tm\n";
                                        $sqm.= "Inner Join mod_matter m on m.matter_id = tm.matter_id\n";
                                        $sqm.= "Inner Join mod_level l on l.level_id = m.matter_level\n";
                                        $sqm.= "Left Join mod_section s on s.sec_id = tm.sec_id\n";
                                        $sqm.= "Left Join mod_note n on n.note_code = tm.item_note\n";
                                        $sqm.= "Left Join mod_teacher_matter pm on pm.matter_id = tm.matter_id and pm.sec_id = tm.sec_id\n";
                                        $sqm.= "Left Join (Select  xt.matter_third\n";
                                        $sqm.= "       			  ,sum(ifnull(xt.court_assists,0)) as tassits\n";
                                        $sqm.= "       	   From mod_third_matter_court xt\n";
                                        $sqm.= "       	   Where xt.matter_third is not null\n";
                                        $sqm.= "       	   Group by xt.matter_third) a on a.matter_third = tm.item_id\n";
                                        $sqm.= "Where tm.period_id = '" . $rows[$i]->period_id . "'\n";
                                        $sqm.= "  and tm.third_id = '" . $thirdData["third_id"] . "'\n";
                                        $sqm.= "Order by tm.item_end desc\n";
                                        //echo $sqm;
                                        $db->setQuery( $sqm );
                                        $rowm = $db->loadObjectList();
                                        for($m = 0; $m < count($rowm); $m++) {
                                            echo "		      <tr item=\"" . $rowm[$m]->item_id . "\">\n";
                                            echo "		      <td ref=\"det\" scope=\"row\">\n";
                                            echo "		        <div class=\"btn-group\">\n";
                                            echo "		            <button type=\"button\" plus=\"0\" id=\"plusNote_" . $rowm[$m]->item_id . "\" item_id=\"" . $rowm[$m]->item_id . "\" period_id=\"" . $rows[$i]->period_id . "\" onclick=\"plusItemdetaill(\$(this))\" class=\"btn btn-info\">\n";
                                            echo "		                <i class=\"glyphicon glyphicon-plus\"></i>\n";
                                            echo "		            </button>\n";
                                            echo "		        </div>\n";
                                            echo "		      </td>\n";
                                            echo "		      <td ref=\"level\">" . $rowm[$m]->level_name . "</td>\n";
                                            echo "		      <td ref=\"matter\">" . $rowm[$m]->matter_name . "</td>\n";
                                            echo "		      <td ref=\"section\">\n";
                                            echo "		        <div class=\"btn-group\" style=\"z-index: " . (count($rowm)-$m) . ";\">\n";
                                            if($rows[$i]->period_sta == "1") {
                                                echo "              <select class=\"form-control col-xs-12 selectpicker\" title=\"Selecione Sección\" item_matter=\"" . $rowm[$m]->item_id . "\" name=\"sec_id\" id=\"sec_id_" . $rows[$i]->period_id . "\">\n";
                                                $sqs = "Select s.sec_id\n";
                                                $sqs.= "      ,s.sec_code\n";
                                                $sqs.= "From mod_section s\n";
                                                $sqs.= "Where s.sec_sta = '1'\n";
                                                $sqs.= "  and s.sec_level = '" . $rowm[$m]->level_id . "'\n";
                                                $db->setQuery( $sqs );
                                                $rowsc = $db->loadObjectList();
                                                for($sc = 0; $sc < count($rowsc) ; $sc++) {
                                                    echo "              <option value=\"" . $rowsc[$sc]->sec_id . "\" " . ($rowsc[$sc]->sec_id == $rowm[$m]->sec_id?"selected":"") . " data-content=\"<kbd>" . $rowsc[$sc]->sec_code . "</kbd>\">" . $rowsc[$sc]->sec_code . "</option>\n";
                                                }
                                                echo "              </select>\n";
                                            } else {
                                                echo "		      <kbd>" . $rowm[$m]->sec_code . "</kbd>\n";
                                            }
                                            echo "		      	</div>\n";
                                            echo "		      </td>\n";
                                            echo "		      <td ref=\"assist\">\n";
                                            echo "              <div class=\"chart\" data-percent=\"" . $rowm[$m]->assist . "\" style=\"position: relative; display: inline-block;\"><span class=\"spanChart\" data-contentX=\"#chartContentPopoverFatther_" . $rowm[$m]->item_id . "\" style=\"cursor:pointer; position: absolute; top: 20%; left: 115%;color: rgb(0, 149, 255);\">" . $rowm[$m]->assist . "%</span></div>\n";
                                            echo "              <div id=\"chartContentPopoverFatther_" . $rowm[$m]->item_id . "\" style=\"display: none\">\n";
                                            echo "                  <table id=\"chartTableContentPopoverFatther_" . $rowm[$m]->item_id . "\" class=\"table table-bordered\">\n";
                                            echo "		                <thead>\n";
                                            echo "		                    <tr>\n";
                                            echo "		                        <th>Cantidad Asistencia</th>\n";
                                            echo "		                        <th>Cantidad Clases</th>\n";
                                            echo "                          </tr>\n";
                                            echo "                      </thead>\n";
                                            echo "                      <tbody>\n";
                                            echo "		                    <tr>\n";
                                            echo "		                        <td>" . $rowm[$m]->tassits . "</td>\n";
                                            echo "		                        <td>" . $rowm[$m]->passits . "</td>\n";
                                            echo "                          </tr>\n";
                                            echo "                      </tbody>\n";
                                            echo "                  </table>\n";
                                            echo "              </div>\n";
                                            echo "            </td>\n";
                                            echo "		      <td ref=\"notes\">\n";
                                            echo "		        <div class=\"btn-group\" style=\"z-index: " . (count($rowm)-$m) . ";\">\n";
                                            if($rows[$i]->period_sta == "1") {
                                                echo "              <select class=\"form-control col-xs-12 selectpicker\" title=\"Selecione Nota\" item_matter=\"" . $rowm[$m]->item_id . "\"  name=\"item_note\" id=\"note_id_" . $rowm[$m]->item_id . "\">\n";
                                                $sqn = "Select n.note_id\n";
                                                $sqn.= "      ,n.note_code\n";
                                                $sqn.= "      ,n.note_class\n";
                                                $sqn.= "From mod_note n\n";
                                                $db->setQuery( $sqn );
                                                $rown = $db->loadObjectList();
                                                for($n = 0; $n < count($rown) ; $n++) {
                                                    echo "              <option value=\"" . $rown[$n]->note_code . "\" " . ($rown[$n]->note_code == $rowm[$m]->item_note?"selected":"") . " data-content=\"<kbd class='" . $rown[$n]->note_class . "'>" . $rown[$n]->note_code . "</kbd>\">" . $rown[$n]->note_code . "</option>\n";
                                                }
                                                echo "              </select>\n";
                                            } else {
                                                echo "		      <kbd class=\"" . $rowm[$m]->note_class . "\">" . $rowm[$m]->item_note . "</kbd>\n";
                                            }
                                            echo "		      	</div>\n";
                                            echo "		      </td>\n";
                                            echo "		      </tr>\n";
                                            echo "		      <tr item=\"detaill_" . $rowm[$m]->item_id . "\" style=\"display:none;\">\n";
                                            echo "		      <td ref=\"det\" scope=\"row\" colspan=\"7\">\n";
                                            echo "		        <table id=\"detaillMatter_" . $rows[$i]->period_id . "_" . $rowm[$m]->item_id . "\" class=\"table table-bordered\">\n";
                                            echo "		            <thead>\n";
                                            echo "		                <tr>\n";
                                            echo "		                    <th style=\"width: 5%;\">Lapso</th>\n";
                                            echo "		                    <th style=\"width: 16%;\">Estatus</th>\n";
                                            echo "		                    <th style=\"width: 10%;\">Asistencias</th>\n";
                                            echo "		                    <th style=\"width: 10%;\">Nota</th>\n";
                                            echo "		                    <th style=\"width: 19%;\">Inicio</th>\n";
                                            echo "		                    <th style=\"width: 19%;\">Fin</th>\n";
                                            echo "		                    <th style=\"width: 18%;\">Accion</th>\n";
                                            echo "		                </tr>\n";
                                            echo "		            </thead>\n";
                                            echo "		            <tbody>\n";
                                                $sqc = "Select c.court_id\n";
                                                $sqc.= "      ,c.court_period\n";
                                                $sqc.= "      ,c.court_assists\n";
                                                $sqc.= "      ,c.matter_third\n";
                                                $sqc.= "      ,c.court_sta\n";
                                                $sqc.= "      ,c.court_note\n";
                                                $sqc.= "      ,c.court_desc\n";
                                                $sqc.= "      ,DATE_FORMAT(c.court_start,'%d/%m/%Y') as court_start\n";
                                                $sqc.= "      ,DATE_FORMAT(c.court_end,'%d/%m/%Y') as court_end\n";
                                                // $sqc.= "      ,c.court_start\n";
                                                // $sqc.= "      ,c.court_end\n";
                                                $sqc.= "      ,DATE_FORMAT(c.user_fec,'%d/%m/%Y %H:%i:%s') as user_fec\n";
                                                $sqc.= "      ,if(c.court_sta = 1, 'Nuevo',if(c.court_sta = 2,'En Curso','Culminado')) as court_status\n";
                                                $sqc.= "      ,n.note_class\n";
                                                $sqc.= "      ,ifnull(pm.court_assists,0) as passits\n";
                                                $sqc.= "      ,FLOOR(ifnull((ifnull(c.court_assists,0)*100)/ifnull(pm.court_assists,0),0)) as assist\n";
                                                $sqc.= "From mod_third_matter_court c\n";
                                                $sqc.= "Left Join mod_teacher_matter_court pm on pm.teacher_third = '" . $rowm[$m]->teacher_third . "' and pm.court_period = c.court_period\n";
                                                $sqc.= "Left Join mod_note n on n.note_code = c.court_note\n";
                                                $sqc.= "Where c.matter_third = '" . $rowm[$m]->item_id . "'\n";
                                                $sqc.= "Order by c.court_period desc\n";
                                                $db->setQuery( $sqc );
                                                $rowc = $db->loadObjectList();
                                                for($c = 0; $c < count($rowc) ; $c++) {
                                                    echo "		      <tr court=\"" . $rowc[$c]->court_id . "\">\n";
                                                    echo "		      <td ref= \"court_period\" scope=\"row\"><kbd>" . $rowc[$c]->court_period . "</kbd></td>\n";
                                                    echo "		      <td ref=\"status\">\n";
													echo "		        <div class=\"btn-group\" style=\"z-index: " . (count($rowc)-$c) . ";\">\n";
                                                    if($rows[$i]->period_sta == "1") {
                                                        echo "              <select class=\"form-control col-xs-12 selectpicker\" title=\"Selecione Estatus\" name=\"court_sta\" id=\"sta_id_" . $rowc[$c]->court_id . "\" court_id=\"" . $rowc[$c]->court_id . "\">\n";
                                                        echo "                  <option value=\"1\" " . ($rowc[$c]->court_sta == "1"?"selected":"") . ">Activo</option>\n";
                                                        echo "                  <option value=\"2\" " . ($rowc[$c]->court_sta == "2"?"selected":"") . ">Culminado</option>\n";
                                                        echo "              </select>\n";
                                                    } else {
                                                        echo "		      " .  $rowc[$c]->court_status . "\n";
                                                    }
                                                    echo "		      	</div>\n";
                                                    echo "		      </td>\n";
                                                    echo "		      <td ref=\"assist\">\n";
                                                    if($rows[$i]->period_sta == "1") {
                                                        echo "              <div class=\"input-group\">\n";
                                                        echo "                  <span class=\"input-group-addon\">&#35;</span>\n";
                                                        echo "                  <input type=\"text\" class=\"form-control col-xs-12\" placeholder=\"Asistencia\" id=\"assist_" . $rowc[$c]->court_id . "\" name=\"court_assists\" court_id=\"" . $rowc[$c]->court_id . "\" value=\"" .  $rowc[$c]->court_assists . "\">\n";
                                                       echo "               </div>\n";
                                                    } else {
                                                        echo "              <div class=\"chart\" data-percent=\"" . $rowc[$c]->assist . "\" style=\"position: relative; display: inline-block;\"><span class=\"spanChart\" data-contentX=\"#chartContentPopover_" . $rowc[$c]->court_id . "\" style=\"cursor:pointer; position: absolute; top: 20%; left: 115%;color: rgb(0, 149, 255);\">" . $rowc[$c]->assist . "%</span></div>\n";
                                                        echo "              <div id=\"chartContentPopover_" . $rowc[$c]->court_id . "\" style=\"display: none\">\n";
                                                        echo "                  <table id=\"chartTableContentPopover_" . $rowc[$c]->court_id . "\" class=\"table table-bordered\">\n";
                                                        echo "		                <thead>\n";
                                                        echo "		                    <tr>\n";
                                                        echo "		                        <th>Cantidad Asistencia</th>\n";
                                                        echo "		                        <th>Cantidad Clases</th>\n";
                                                        echo "                          </tr>\n";
                                                        echo "                      </thead>\n";
                                                        echo "                      <tbody>\n";
                                                        echo "		                    <tr>\n";
                                                        echo "		                        <td>" . $rowc[$c]->court_assists . "</td>\n";
                                                        echo "		                        <td>" . $rowc[$c]->passits . "</td>\n";
                                                        echo "                          </tr>\n";
                                                        echo "                      </tbody>\n";
                                                        echo "                  </table>\n";
                                                        echo "              </div>\n";
                                                    }
                                                    echo "            </td>\n";
                                                    echo "		      <td ref=\"notes\">\n";
													echo "		        <div class=\"btn-group\" style=\"z-index: " . (count($rowc)-$c) . ";\">\n";
                                                    if($rows[$i]->period_sta == "1") {
                                                        echo "              <select class=\"form-control col-xs-12 selectpicker\" title=\"Selecione Nota\" court_id=\"" . $rowc[$c]->court_id . "\" name=\"court_note\" id=\"note_id_" . $rowc[$c]->court_id . "\">\n";
                                                        $sqn = "Select n.note_id\n";
                                                        $sqn.= "      ,n.note_code\n";
                                                        $sqn.= "      ,n.note_class\n";
                                                        $sqn.= "From mod_note n\n";
                                                        $db->setQuery( $sqn );
                                                        $rown = $db->loadObjectList();
                                                        for($n = 0; $n < count($rown) ; $n++) {
                                                            echo "              <option value=\"" . $rown[$n]->note_code . "\" " . ($rown[$n]->note_code == $rowc[$c]->court_note?"selected":"") . " data-content=\"<kbd class='" . $rown[$n]->note_class . "'>" . $rown[$n]->note_code . "</kbd>\">" . $rown[$n]->note_code . "</option>\n";
                                                        }
                                                        echo "              </select>\n";
                                                    } else {
                                                        echo "		      <kbd class=\"" . $rowc[$c]->note_class . "\">" . $rowc[$c]->court_note . "</kbd>\n";
                                                    }
                                                    echo "		      	</div>\n";
                                                    echo "		      </td>\n";
                                                    echo "		      <td ref=\"start\">\n";
                                                    if($rows[$i]->period_sta == "1") {
                                                        echo "              <div class=\"input-group\">\n";
                                                        echo "                  <span class=\"input-group-addon\"><i class=\"glyphicon glyphicon-calendar\"></i></span>\n";
                                                        echo "                  <input type=\"text\" class=\"form-control court_start\" placeholder=\"d/m/Y\" id=\"court_start_" . $rowc[$c]->court_id . "\" name=\"court_start\" court_id=\"" . $rowc[$c]->court_id . "\" value=\"" .  $rowc[$c]->court_start . "\">\n";
                                                        echo "               </div>\n";
                                                    } else {
                                                        echo "		      " .  $rowc[$c]->court_start . "\n";
                                                    }
                                                        echo "		      </td>\n";
                                                        echo "		      <td ref=\"end\">\n";
                                                    if($rows[$i]->period_sta == "1") {
                                                        echo "              <div class=\"input-group\">\n";
                                                        echo "                  <span class=\"input-group-addon\"><i class=\"glyphicon glyphicon-calendar\"></i></span>\n";
                                                        echo "                  <input type=\"text\" class=\"form-control col-xs-12 court_end\" placeholder=\"d/m/Y\" id=\"court_end_" . $rowc[$c]->court_id . "\" name=\"court_end\" court_id=\"" . $rowc[$c]->court_id . "\" value=\"" .  $rowc[$c]->court_end . "\">\n";
                                                        echo "               </div>\n";
                                                    } else {
                                                        echo "		      " .  $rowc[$c]->court_start . "\n";
                                                    }
                                                    echo "		      </td>\n";
                                                        echo "		      <td ref=\"accion\">\n";
                                                        echo "		        <div class=\"btn-group\">\n";
                                                    if($rows[$i]->period_sta != "2") {
                                                        echo "		            <button type=\"button\" title=\"Editar\" id=\"editNotes_" . $rowc[$c]->court_id . "\" court_id=\"" . $rowc[$c]->court_id . "\" court_desc=\"" . $rowc[$c]->court_desc . "\" court_period=\"" . $rowc[$c]->court_period . "\" class=\"btn btn-primary btn-sm\" onclick=\"upNoteDesc(\$(this))\">\n";
                                                        echo "		                <i class=\"glyphicon glyphicon-edit\"></i>\n";
                                                        echo "		                Boleta\n";
                                                        echo "		            </button>\n";
                                                        echo "		            <button type=\"button\" title=\"Imprimir\" style=\"height: 30px;\" id=\"printNotesCourt_" . $rowc[$c]->court_id . "\" third_id=\"" . $thirdData["third_id"] . "\" period_id=\"" . $rows[$i]->period_id . "\" court_id=\"" . $rowc[$c]->court_id . "\" matter_id=\"" . $rowm[$m]->matter_id . "\" class=\"btn btn-primary btn-sm printNotesCourt\">\n";
                                                        echo "		                <i class=\"glyphicon glyphicon-print\"></i>\n";
                                                        echo "		            </button>\n";
                                                    } else {
                                                        echo "		            <button type=\"button\" title=\"Imprimir\" id=\"printNotesCourt_" . $rowc[$c]->court_id . "\" third_id=\"" . $thirdData["third_id"] . "\" period_id=\"" . $rows[$i]->period_id . "\" court_id=\"" . $rowc[$c]->court_id . "\" matter_id=\"" . $rowm[$m]->matter_id . "\" class=\"btn btn-success btn-sm printNotesCourt\">\n";
                                                        echo "		                <i class=\"glyphicon glyphicon-print\"></i>\n";
                                                        echo "		                Boleta\n";
                                                        echo "		            </button>\n";
                                                    }
                                                    echo "		        </div>\n";
                                                    echo "		      </td>\n";
                                                    echo "		      </tr>\n";
                                                }
                                            echo "		            </tbody>\n";
                                            echo "		        </table>\n";
                                            echo "		      </td>\n";
                                            echo "		      </tr>\n";
                                        }
                                        echo "		      </tbody>\n";
                                        echo "		   </table>\n";
                                        echo "		   </div>\n";
                                        echo "		</div>\n";
                                        echo "	</div>\n";
                                        echo "</div>\n";
                                    }
                                    ?>
                                </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-default">
		  <div class="panel-body">
			<div class="container-fluid">
				<div class="btn-group">
					<button type="submit" class="btn btn-success">
						<i class="glyphicon glyphicon-ok"></i>
						Guardar
					</button>
					<button type="button" id="thirdList" class="btn btn-info">
						<i class="glyphicon glyphicon-th-list"></i>
						Lista
					</button>
				</div>
			</div>
		  </div>
		</div>
	</form>
</div>