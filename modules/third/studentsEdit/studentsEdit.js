function listThirds() {
	var url = "index.php?token=" + targetCode + "&module=third&vista=studentsList&back=" + JSON.stringify(objDataBack);
		$(location).attr('href',url);
}
function callBackFormSend(resp) {
    if(resp.id != "") {
        $("#btnRepresent").show();
        $("#btnInscription").show();
    }
}
function enrollThird(oBj) {
    var datos = {};
        datos.third_id  = oBj.attr("third_id");
        datos.period_id = oBj.attr("period_id");
        datos.level_id  = oBj.attr("level_id");
        datos.accion    = "add";
    var dataMatter;
        dataMatter = window["matters_" + datos.level_id];
    confirm = "<div id=\"confirmModal\" class=\"modal fade\">\n";
    confirm+= "     <div class=\"modal-dialog\">\n";
    confirm+= "     <div class=\"modal-content\">\n";
    confirm+= "     <div class=\"modal-header\">\n";
    confirm+= "        Confirmar Inscripcion : <kbd>" + $("input#third_name").val() + " " + $("input#third_lastname").val() + "</kbd> " + oBj.attr("period_name") + "\n";
    confirm+= "     </div>\n";
    confirm+= "     <div class=\"modal-body\">\n";
    confirm+= "        <h1>Materias a Inscribir en <kbd>" + oBj.html() + "</kbd></h1>\n";
    confirm+= "        <ul class=\"list-group\">\n";
    for(m = 0; m < dataMatter.length; m++) {
        confirm += "            <li class=\"list-group-item\">" + dataMatter[m].matter_name + "<span class=\"badge\">" + dataMatter[m].matter_code + "</span></li>\n";
    }
    confirm+= "        </ul>\n";
    confirm+= "     </div>\n";
    confirm+= "     <div class=\"modal-footer\">\n";
    confirm+= "        <button type=\"button\" id=\"sendEnroll\" class=\"btn btn-success\">\n";
    confirm+= "		            <i class=\"glyphicon glyphicon-ok\"></i>\n";
    confirm+= "		            Inscribir\n";
    confirm+= "		   </button>\n";
    confirm+= "        <button type=\"button\" id=\"closeEnroll\" data-dismiss=\"modal\" class=\"btn btn-danger\">\n";
    confirm+= "		        <i class=\"glyphicon glyphicon-remove\"></i>\n";
    confirm+= "		        Cancelar\n";
    confirm+= "		   </button>\n";
    confirm+= "    </div>\n";
    confirm+= "    </div>\n";
    confirm+= "    </div>\n";
    confirm+= "</div>\n";
    $(confirm).modal({
        backdrop: false,
        keyboard:false
    }).on('hidden.bs.modal', function () {
        $(this).data('bs.modal', null);
    }).on('click','button#sendEnroll', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $_selb = $(this);
        $_selb.attr({"disabled":"disabled"}).find("i").removeClass("glyphicon-ok").addClass("throbber-loader").addClass("disabled").css({"font-size":"9px"});
        $("button#closeEnroll").attr({"disabled":"disabled"}).addClass("disabled");
        $.ajax({
             async: true,
             type: "POST",
             dataType: "json",
             url: "json.php",
             data: {
                 module: "enroll",
                 vista: "enrollsUp",
                 datos: JSON.stringify(datos)
             },
             success: function(respJson) {
				$_selb.removeAttr("disabled").removeClass("disabled").find("i").removeAttr("style").removeClass("throbber-loader").addClass("glyphicon-ok");
				$("button#closeEnroll").removeAttr("disabled").removeClass("disabled");
				$("#confirmModal").modal("hide");
				if(respJson.status != "ERROR") {
					$("button#addEnroll").remove();
					$("button#addEnrollRight").remove();
					refreshEnrolls();
				} else {
					$("#myModal" + targetCode + " div.modal-header > h4.modal-title").html(respJson.status);
					$("#myModal" + targetCode + " div.modal-body").html(respJson.text);
					$("#myModal" + targetCode + "").modal("show");
				}
             },
             error: function(jqXHR, textStatus, errorThrown) {
                 $_selb.removeAttr("disabled").find("i").removeAttr("style").removeClass("throbber-loader").addClass("glyphicon-ok");
                 $("#confirmModal").modal("hide");
                 $("#myModal" + targetCode + " div.modal-header > h4.modal-title").html(textStatus);
                 $("#myModal" + targetCode + " div.modal-body").html("<p>" + errorThrown + "</p>");
                 $("#myModal" + targetCode + "").modal("show");
             }
         });
    });
}
function plusItemdetaill(oBj) {
    var plus        = oBj.attr("plus");
    var itemId      = oBj.attr("item_id");
    var periodId    = oBj.attr("period_id");
        if(parseFloat(plus)) {
            $("table#periodMatter_" + periodId + " > tbody > tr[item=detaill_" + itemId + "]").hide();
            oBj.attr("plus",0).find("i").removeClass("glyphicon-minus").addClass("glyphicon-plus");
        } else {
            $("table#periodMatter_" + periodId + " > tbody > tr[item=detaill_" + itemId + "]").show();
            oBj.attr("plus",1).find("i").removeClass("glyphicon-plus").addClass("glyphicon-minus");
        }
}
function enrollsNoteUp(Obj) {
    var datos = {};
    datos["item_id"] = Obj.attr("item_matter");
    datos[Obj.attr("name")] = Obj.val();
    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        url: "json.php",
        data: {
            module: "enroll",
            vista: "enrollsNoteUp",
            datos: JSON.stringify(datos)
        },
        success: function(respJson) {
            if(respJson.status == "ERROR") {
                $("#myModal" + targetCode + " div.modal-header > h4.modal-title").html(respJson.status);
                $("#myModal" + targetCode + " div.modal-body").html(respJson.text);
                $("#myModal" + targetCode + "").modal("show");
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            $("#myModal" + targetCode + " div.modal-header > h4.modal-title").html(textStatus);
            $("#myModal" + targetCode + " div.modal-body").html("<p>" + errorThrown + "</p>");
            $("#myModal" + targetCode + "").modal("show");
        }
    });
}
function enrollsCourtUp(Obj) {
    var datos = {};
        datos["court_id"] = Obj.attr("court_id");
        datos[Obj.attr("name")] = Obj.val();
    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        url: "json.php",
        data: {
            module: "enroll",
            vista: "enrollsCourtUp",
            datos: JSON.stringify(datos)
        },
        success: function(respJson) {
            if(respJson.status == "ERROR") {
                $("#myModal" + targetCode + " div.modal-header > h4.modal-title").html(respJson.status);
                $("#myModal" + targetCode + " div.modal-body").html(respJson.text);
                $("#myModal" + targetCode + "").modal("show");
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            $("#myModal" + targetCode + " div.modal-header > h4.modal-title").html(textStatus);
            $("#myModal" + targetCode + " div.modal-body").html("<p>" + errorThrown + "</p>");
            $("#myModal" + targetCode + "").modal("show");
        }
    });
}
function refreshEnrolls() {
    var data = {};
        data.third_id  	= $("#third_id").val();
        data.period_id  = $("#period_id").val();
        data.period_sta	= $("#period_sta").val();
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        url: "hide.php",
        data: {
            module: "third",
            vista: "enrollsRefresh",
            datos: JSON.stringify(data)
        },
        success: function(respHtml) {
			$("#periodMatterContent_" + data.period_id +"").html(respHtml);
			$("#periodMatterContent_" + data.period_id +"").find("select").selectpicker();
			$('.chart').easyPieChart({
				animate: 2000,
				scaleColor: false,
				lineWidth: 5,
				lineCap: 'square',
				size: 30,
				trackColor: '#e5e5e5',
				barColor: '#3da0ea'
			});
			$(".spanChart").popover({
				html : true,
				placement : "right",
				title : "Asistencia",
				content: function() {
					return $($(this).data("contentx")).html();
				}
			}).tooltip({title:"Click Detalles", placement : "bottom"});
			$('input.court_start').datetimepicker({
				format: 'DD/MM/YYYY'
			}).on('dp.change',function() {
				enrollsCourtUp($(this));
			});
			$('input.court_end').datetimepicker({
				format: 'DD/MM/YYYY'
			}).on('dp.change',function() {
				enrollsCourtUp($(this));
			});
			$("input.court_start").on("dp.change", function (e) {
				$("input#court_end_" + $(this).attr("court_id") + "").data("DateTimePicker").minDate(e.date);
			});
			$("input[court_id], select[court_id]").on('change', function(){
				enrollsCourtUp($(this));
			});
			$("input[item_matter], select[item_matter]").on('change', function(){
				enrollsNoteUp($(this));
			});
        },
        error: function(jqXHR, textStatus, errorThrown) {
            $("#myModal" + targetCode + " div.modal-header > h4.modal-title").html(textStatus);
            $("#myModal" + targetCode + " div.modal-body").html("<p>" + errorThrown + "</p>");
            $("#myModal" + targetCode + "").modal("show");
        }
    });
}
function upNoteDesc(oBj) {
    confirm = "<div id=\"confirmModal\" class=\"modal fade\">\n";
    confirm+= "     <div class=\"modal-dialog\">\n";
    confirm+= "     <div class=\"modal-content\">\n";
    confirm+= "     <div class=\"modal-header\">\n";
    confirm+= "        Boleta : <kbd>" + $("input#third_name").val() + " " + $("input#third_lastname").val() + "</kbd> Lapso " + oBj.attr("court_period") + "\n";
    confirm+= "     </div>\n";
    confirm+= "     <div class=\"modal-body\">\n";
    confirm+= "\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n";
    confirm+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">\n";
    confirm+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\">Notas</span>\n";
    confirm+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control selectpicker\" title=\"Selecione Descripcion\" name=\"desc_note\" id=\"desc_note\">\n";
    confirm+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">Seleccione</option>\n";
    for(n = 0; n < dataDescNote.length; n++) {
        confirm+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"" + dataDescNote[n].desc_id + "\">" + dataDescNote[n].desc_text + "</option>\n";
    }
    confirm+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</select>\n";
    confirm+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n";
    confirm+= "\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n";
    confirm+= "     <div class=\"input-group\">\n";
    confirm+= "         <span class=\"input-group-addon\">Descripcion</span>\n";
    confirm+= "         <textarea class=\"form-control\" placeholder=\"Descripcion\" id=\"court_desc\" name=\"court_desc\" data-rule-maxlength=\"2000\" data-msg-maxlength=\"No ingrese mas de 2000 Caracteres\" style=\"width: 461px; height: 306px;\">" + oBj.attr("court_desc") + "</textarea>\n";
    confirm+= "     </div>\n";
    confirm+= "     </div>\n";
    confirm+= "     <div class=\"modal-footer\">\n";
    confirm+= "        <button type=\"button\" id=\"sendEnrollNote\" class=\"btn btn-success\">\n";
    confirm+= "		            <i class=\"glyphicon glyphicon-ok\"></i>\n";
    confirm+= "		            Guardar\n";
    confirm+= "		   </button>\n";
    confirm+= "        <button type=\"button\" id=\"closeEnrollNote\" data-dismiss=\"modal\" class=\"btn btn-danger\">\n";
    confirm+= "		        <i class=\"glyphicon glyphicon-remove\"></i>\n";
    confirm+= "		        Cancelar\n";
    confirm+= "		   </button>\n";
    confirm+= "    </div>\n";
    confirm+= "    </div>\n";
    confirm+= "    </div>\n";
    confirm+= "</div>\n";
    $(confirm).modal({
        backdrop: false,
        keyboard:false
    }).on('shown.bs.modal', function(){
        $("#desc_note").selectpicker().on('change', function() {
            $('#court_desc').html($(this).find('option:selected').html());
        });
    }).on('hidden.bs.modal', function () {
        $("#confirmModal").data('bs.modal', null);
    }).on('click','button#sendEnrollNote', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $_selb = $(this);
        $_selb.attr({"disabled":"disabled"}).find("i").removeClass("glyphicon-ok").addClass("throbber-loader").addClass("disabled").css({"font-size":"9px"});
        $("button#closeEnrollNote").attr({"disabled":"disabled"}).addClass("disabled");
        var datos = {};
        datos["court_id"]   = oBj.attr("court_id");
        datos["court_desc"] = $("#confirmModal").find("textarea#court_desc").val();
        $.ajax({
            async: true,
            type: "POST",
            dataType: "json",
            url: "json.php",
            data: {
                module: "enroll",
                vista: "enrollsCourtUp",
                datos: JSON.stringify(datos)
            },
            success: function(respJson) {
                $_selb.removeAttr("disabled").removeClass("disabled").find("i").removeAttr("style").removeClass("throbber-loader").addClass("glyphicon-ok");
                $("button#closeEnrollNote").removeAttr("disabled").removeClass("disabled");
                if(respJson.status != "ERROR") {
                    oBj.attr("court_desc",$("#confirmModal").find("textarea#court_desc").val());
					$("#confirmModal").modal("hide");
                } else {
					$("#confirmModal").modal("hide");
                    $("#myModal" + targetCode + " div.modal-header > h4.modal-title").html(respJson.status);
                    $("#myModal" + targetCode + " div.modal-body").html(respJson.text);
                    $("#myModal" + targetCode + "").modal("show");
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $_selb.removeAttr("disabled").find("i").removeAttr("style").removeClass("throbber-loader").addClass("glyphicon-ok");
                $("#confirmModal").modal("hide");
                $("#myModal" + targetCode + " div.modal-header > h4.modal-title").html(textStatus);
                $("#myModal" + targetCode + " div.modal-body").html("<p>" + errorThrown + "</p>");
                $("#myModal" + targetCode + "").modal("show");
            }
        });
    });
}
function refreshRepresent() {
    var data = {};
        data.third_id  = $("#third_id").val();
    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        url: "json.php",
        data: {
            module: "third",
            vista: "representsData",
            datos: JSON.stringify(data)
        },
        success: function(respJson) {
            representTablaList.fnClearTable();
            if(respJson.length > 0) {
                representTablaList.fnAddData(respJson);
                representTablaList.fnDraw();
                arrayDataRepresentTabla = respJson;
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            $("#myModal" + targetCode + " div.modal-header > h4.modal-title").html(textStatus);
            $("#myModal" + targetCode + " div.modal-body").html("<p>" + errorThrown + "</p>");
            $("#myModal" + targetCode + "").modal("show");
        }
    });
}
function addNewRepresent() {
    modalRepresent({accion:"add"});
}
function delRepresent(data,oBj) {
    oBj.attr({"disabled":"disabled"}).find("i").removeClass("glyphicon-trash").addClass("throbber-loader").addClass("disabled").css({"font-size":"9px"});
    $("button#editRepresent_" + data.third_id + "_").attr({"disabled":"disabled"}).addClass("disabled");
    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        url: "json.php",
        data: {
            module: "third",
            vista: "representsUp",
            datos: JSON.stringify(data)
        },
        success: function(respJson) {
            oBj.removeAttr("disabled").removeClass("disabled").find("i").removeAttr("style").removeClass("throbber-loader").addClass("glyphicon-trash");
            $("button#editRepresent_" + data.third_id + "_").removeAttr("disabled").removeClass("disabled");
            if(respJson.status != "ERROR") {
                $("table#representTabla > tbody > tr[ref=" + data.third_id + "]").remove();
            } else {
                $("#myModal" + targetCode + " div.modal-header > h4.modal-title").html(respJson.status);
                $("#myModal" + targetCode + " div.modal-body").html(respJson.text);
                $("#myModal" + targetCode + "").modal("show");
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            oBj.removeAttr("disabled").find("i").removeAttr("style").removeClass("throbber-loader").addClass("glyphicon-trash");
            $("#myModal" + targetCode + " div.modal-header > h4.modal-title").html(textStatus);
            $("#myModal" + targetCode + " div.modal-body").html("<p>" + errorThrown + "</p>");
            $("#myModal" + targetCode + "").modal("show");
        }
    });
}
function modalRepresent(datax) {
    formH = "\t<div id=\"representModal\" class=\"modal fade\">\n";
    formH+= "\t\t<div class=\"modal-dialog\">\n";
    formH+= "\t\t\t<div class=\"modal-content\">\n";
    formH+= "\t\t\t\t<div class=\"modal-header\">\n";
    formH+= "\t\t\t\t\tRepresentante del Alumno : <kbd>" + $("input#third_name").val() + " " + $("input#third_lastname").val() + "</kbd>\n";
    formH+= "\t\t\t\t</div>\n";
    formH+= "\t\t\t\t<div class=\"modal-body\">\n";
    formH+= "\t\t\t\t\t<form id=\"representModify\" data-id=\"third_id\" data-module=\"third\" data-vista=\"representsUp\" class=\"form-horizontal\" method=\"POST\" target=\"" + targetCode + "\">\n";
    formH+= "\t\t\t\t\t\t<div class=\"tab-content\">\n";
    formH+= "\t\t\t\t\t\t\t<div class=\"tab-pane active\" id=\"thirdsData\">\n";
    formH+= "\t\t\t\t\t\t\t\t<div class=\"panel panel-primary\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t<div class=\"panel-heading\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t<h3 id=\"panel-title\" class=\"panel-title\">Datos Representante</h3>\n";
    formH+= "\t\t\t\t\t\t\t\t</div>\n";
    formH+= "\t\t\t\t\t\t\t\t<div class=\"panel-body\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t<input value=\"\" id=\"third_id\" name=\"third_id\" type=\"hidden\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t<input value=\"" + $("#third_id").val()  + "\" id=\"third_son\" name=\"third_son\" type=\"hidden\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t<input value=\"1\" id=\"third_rel\" name=\"third_rel\" type=\"hidden\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t<input value=\"\" id=\"accion\" name=\"accion\" type=\"hidden\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t<div class=\"container-fluid\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-xs-5\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group success\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span for=\"third_name\" class=\"input-group-addon\">Nombres</span>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input aria-invalid=\"false\" class=\"form-control\" placeholder=\"Nombres\" id=\"third_name\" name=\"third_name\" aria-required=\"true\" data-rule-required=\"true\" data-msg-required=\"Ingrese un Nombre al Estudiante\" data-rule-maxlength=\"150\" data-msg-maxlength=\"No ingrese mas de 150 Caracteres\" type=\"text\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span for=\"third_lastname\" class=\"input-group-addon\">Apellidos</span>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input class=\"form-control\" placeholder=\"Apellidos\" id=\"third_lastname\" name=\"third_lastname\" aria-required=\"true\" data-rule-required=\"true\" data-msg-required=\"Ingrese un Apellidos al Estudiante\" data-rule-maxlength=\"150\" data-msg-maxlength=\"No ingrese mas de 150 Caracteres\" type=\"text\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span for=\"third_rif\" class=\"input-group-addon\">Cedula</span>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input class=\"form-control\" placeholder=\"Cedula/Rif\" id=\"third_rif\" name=\"third_rif\" aria-required=\"true\" data-rule-required=\"true\" data-msg-required=\"Ingrese una Cedula\" data-rule-maxlength=\"8\" data-msg-maxlength=\"No ingrese mas de 8 Caracteres\" type=\"text\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">\n";
	formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span for=\"third_birth\" class=\"input-group-addon\">Fecha de Nacimiento</span>\n";
	formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control third_birth\" placeholder=\"DD/MM/YYYY\" id=\"third_birth\" name=\"third_birth\" aria-required=\"true\" data-rule-required=\"true\" data-msg-required=\"Ingrese Fecha Nacimiento\" data-rule-maxlength=\"10\" data-msg-maxlength=\"No ingrese mas de 10 Caracteres\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\" style=\"z-index: 7;\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\">Sexo</span>\n";
	formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control selectpicker\" title='Selecione Sexo' name=\"third_sex\" id=\"third_sex\" data-rule-required=\"true\" data-rule-email=\"true\" data-msg-required=\"Ingrese El Sexo\">\n";
	formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"M\">Masculino</option>\n";
	formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"F\">Femenino</option>\n";
	formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</select>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\">Nacionalidad</span>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control selectpicker\" title=\"Selecione Nacionalidad\" name=\"third_nationallity\" id=\"third_nationallity\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"V\">Venezolano</option>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"E\">Extranjero</option>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</select>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t</div>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-xs-5 col-md-offset-1\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span for=\"third_email\" class=\"input-group-addon\">Correo</span>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input class=\"form-control\" placeholder=\"ejemplo@ejemplo.com\" id=\"third_email\" name=\"third_email\" aria-required=\"true\" data-rule-required=\"true\" data-rule-email=\"true\" data-msg-required=\"Ingrese un correo\" data-msg-email=\"Ingrese un correo Valido\" data-rule-maxlength=\"150\" data-msg-maxlength=\"No ingrese mas de 150 Caracteres\" type=\"email\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span for=\"third_phone\" class=\"input-group-addon\">Teléfono</span>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input class=\"form-control\" placeholder=\"Telefono\" id=\"third_phone\" name=\"third_phone\"data-rule-number=\"true\" data-msg-number=\"Solo Números\" aria-required=\"true\" data-rule-required=\"true\" data-msg-required=\"Ingrese un Número de Teléfono\" data-rule-maxlength=\"12\" data-msg-maxlength=\"No ingrese mas de 12 Caracteres\" type=\"text\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span for=\"third_address\" class=\"input-group-addon\">Dirección</span>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<textarea class=\"form-control\" placeholder=\"Dirección\" id=\"third_address\" name=\"third_address\" data-rule-maxlength=\"255\" data-msg-maxlength=\"No ingrese mas de 200 Caracteres\"></textarea>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span for=\"third_description\" class=\"input-group-addon\">Descripción</span>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<textarea class=\"form-control\" placeholder=\"Descripción\" id=\"third_description\" name=\"third_description\" data-rule-maxlength=\"255\" data-msg-maxlength=\"No ingrese mas de 200 Caracteres\"></textarea>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t\t</div>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t\t</div>\n";
    formH+= "\t\t\t\t\t\t\t\t\t\t</div>\n";
    formH+= "\t\t\t\t\t\t\t\t\t</div>\n";
    formH+= "\t\t\t\t\t\t\t\t</div>\n";
    formH+= "\t\t\t\t\t\t\t</div>\n";
    formH+= "\t\t\t\t\t\t</div>\n";
    formH+= "\t\t\t\t\t</form>\n";
    formH+= "\t\t\t\t</div>\n";
    formH+= "\t\t\t\t<div class=\"modal-footer\">\n";
    formH+= "\t\t\t\t\t<button type=\"button\" id=\"sendRepresent\" class=\"btn btn-success\">\n";
    formH+= "\t\t\t\t\t\t<i class=\"glyphicon glyphicon-ok\"></i>\n";
    formH+= "\t\t\t\t\t\tGuardar\n";
    formH+= "\t\t\t\t\t</button>\n";
    formH+= "\t\t\t\t\t<button type=\"button\" id=\"closeRepresent\" data-dismiss=\"modal\" class=\"btn btn-danger\">\n";
    formH+= "\t\t\t\t\t\t<i class=\"glyphicon glyphicon-remove\"></i>\n";
    formH+= "\t\t\t\t\t\tCancelar\n";
    formH+= "\t\t\t\t\t</button>\n";
    formH+= "\t\t\t\t</div>\n";
    formH+= "\t\t\t</div>\n";
    formH+= "\t\t</div>\n";
    formH+= "\t</div>\n";
    $(formH).modal({
        backdrop: false,
        keyboard:false
    }).on('shown.bs.modal', function(){
		$("form#representModify").find("input.third_birth").datetimepicker({
            format: 'DD/MM/YYYY',
            maxDate: moment().subtract(20, 'years')
        });
		$("form#representModify").find("input[type=checkbox], input[type=radio]").bootstrapSwitch();
		$("form#representModify").find("select").selectpicker();
        $("form#representModify").autofill(datax);
        $("form#representModify").validate({
            submitHandler: function(form) {
                $(form).submit(function() {
                    $_self = $(this);
                    $(this).ajaxSubmit({
                        async: true,
                        type: "POST",
                        dataType: "json",
                        url: "json.php",
                        data: {
                            module: $_self.data("module"),
                            vista: $_self.data("vista"),
                            datos: JSON.stringify($_self.serializeObject())
                        },
                        beforeSubmit: function(formData, jqForm, options) {
                            $("button#sendRepresent").attr({"disabled":"disabled"}).find("i").removeClass("glyphicon-ok").addClass("throbber-loader").addClass("disabled").css({"font-size":"9px"});
                            $("button#closeRepresent").attr({"disabled":"disabled"}).addClass("disabled");
                            return true;
                        },
                        success: function(respJson, statusText, xhr, $form) {
                            $("button#sendRepresent").removeAttr("disabled").removeClass("disabled").find("i").removeAttr("style").removeClass("throbber-loader").addClass("glyphicon-ok");
                            $("button#closeRepresent").removeAttr("disabled").removeClass("disabled");
                            $("#representModal").modal("hide");
                            if(respJson.status == "ERROR") {
                                $("#myModal" + targetCode + " div.modal-header > h4.modal-title").html(respJson.status);
                                $("#myModal" + targetCode + " div.modal-body").html(respJson.text);
                                $("#myModal" + targetCode + "").modal("show");
                            } else {
                                refreshRepresent();
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            $("button#sendRepresent").removeAttr("disabled").removeClass("disabled").find("i").removeAttr("style").removeClass("throbber-loader").addClass("glyphicon-ok");
                            $("button#closeRepresent").removeAttr("disabled").removeClass("disabled");
                            $("#representModal").modal("hide");
                            $("#myModal" + targetCode + " div.modal-header > h4.modal-title").html(textStatus);
                            $("#myModal" + targetCode + " div.modal-body").html(errorThrown);
                            $("#myModal" + targetCode + "").modal("show");
                        }
                    });
                    return false;
                });
            },
            highlight: function (element) {
                $(element).closest('.form-group').removeClass('success').addClass('error');
            },
            success: function(element) {
                element.text('').addClass('valid').closest('.form-group').removeClass('error').addClass('success');
            }
        });
    }).on('hidden.bs.modal', function () {
        $("form#representModify").unbind();
        $("button#sendRepresent").unbind();
        $(this).data('bs.modal', null);
    }).on('click','button#sendRepresent', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $("form#representModify").submit();
    });
}

function confirmStatus(title) {
    confirm = "<div id=\"confirmModal\" class=\"modal fade\">\n";
    confirm+= "     <div class=\"modal-dialog\">\n";
    confirm+= "     <div class=\"modal-content\">\n";
    confirm+= "     <div class=\"modal-header\">\n";
    confirm+= "        Indique Por Que <kbd><strong>" + title + "</strong></kbd> <kbd>" + $("input#third_name").val() + " " + $("input#third_lastname").val() + "</kbd>\n";
    confirm+= "     </div>\n";
    confirm+= "     <div class=\"modal-body\">\n";
    confirm+= "     <div class=\"input-group\">\n";
    confirm+= "         <span class=\"input-group-addon\">Descripcion</span>\n";
    confirm+= "         <textarea class=\"form-control\" placeholder=\"Descripcion\" id=\"sta_desc\" name=\"sta_desc\" data-rule-maxlength=\"255\" data-msg-maxlength=\"No ingrese mas de 255 Caracteres\" style=\"width: 461px; height: 306px;\">" + $("input#third_sta_description").val() + "</textarea>\n";
    confirm+= "     </div>\n";
    confirm+= "     </div>\n";
    confirm+= "     <div class=\"modal-footer\">\n";
    confirm+= "        <button type=\"button\" id=\"sendDescStatus\" class=\"btn btn-success\">\n";
    confirm+= "                 <i class=\"glyphicon glyphicon-ok\"></i>\n";
    confirm+= "                 Guardar\n";
    confirm+= "        </button>\n";
    confirm+= "        <button type=\"button\" id=\"closeDescStatus\" data-dismiss=\"modal\" class=\"btn btn-danger\">\n";
    confirm+= "             <i class=\"glyphicon glyphicon-remove\"></i>\n";
    confirm+= "             Cancelar\n";
    confirm+= "        </button>\n";
    confirm+= "    </div>\n";
    confirm+= "    </div>\n";
    confirm+= "    </div>\n";
    confirm+= "</div>\n";
    $(confirm).modal({
        backdrop: false,
        keyboard:false
    }).on('shown.bs.modal', function(){
        //
    }).on('hidden.bs.modal', function () {
        $("#confirmModal").data('bs.modal', null);
    }).on('click','button#sendDescStatus', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $("select#third_sta").attr({"valOld":$("select#third_sta").val()});
        $("input#third_sta_description").val($("#confirmModal").find("textarea#sta_desc").val());
        $("#confirmModal").modal("hide");
    }).on('click','button#closeDescStatus', function(e) {
        $("select#third_sta").val($("select#third_sta").attr("valOld")).selectpicker('render');
    });
}
$(function () {
    $("input#third_birth").datetimepicker({
        format: 'DD/MM/YYYY',
        maxDate: moment().subtract(5, 'years')
    });
    $("select#third_sta").on('change', function(){
        var val     = $(this).val();
        var title   = $('option:selected',this).html();
        if(val == '1' || val == '5') {
            $("select#third_sta").attr({"valOld":val});
            $("input#third_sta_description").val('');
        } else {
            confirmStatus(title);
        }
    });
    $("input#third_rif").on('change', function(){
        if($(this).val() != ""){
            $("form#thirdModify").find("button[type=submit]").attr({"disabled":"disabled"});
            $("span.third_rif").removeClass("glyphicon-barcode").addClass("three-quarters-loader").css({"padding": "0px", "width": "20px", "height": "20px", "border-width": "5px"});
            $.ajax({
                async: true,
                type: "POST",
                dataType: "json",
                url: "json.php",
                data: {
                    module: "third",
                    vista: "studentsData",
                    datos: JSON.stringify({third_oneRif: $(this).val()})
                },
                success: function(respJson) {
                    $("span.third_rif").removeAttr("style").removeClass("three-quarters-loader").addClass("glyphicon-barcode");
                    if(respJson.length < 1) {
                        $("form#thirdModify").find("button[type=submit]").removeAttr("disabled");
                        $("span.third_rif").popover("hide");
                    } else {
                        dataThird = respJson[0];
                        $("span.third_rif").popover({
                            placement : "right",
                            trigger : "manual",
                            content: "Nombre: " + dataThird.third_names,
                            title: "ALumno Existe"
                        });
                        $("span.third_rif").popover("show");
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $("#myModal" + targetCode + " div.modal-header > h4.modal-title").html(textStatus);
                    $("#myModal" + targetCode + " div.modal-body").html("<p>" + errorThrown + "</p>");
                    $("#myModal" + targetCode + "").modal("show");
                    $("form#thirdModify").find("button[type=submit]").removeAttr("disabled");
                    $("span.third_rif").removeAttr("style").removeClass("three-quarters-loader").addClass("glyphicon-barcode");
                    $("span.third_rif").popover("hide");
                }
            });
        } else {
            $("form#thirdModify").find("button[type=submit]").attr({"disabled":"disabled"});
            $("span.third_rif").removeAttr("style").removeClass("three-quarters-loader").addClass("glyphicon-barcode");
            $("span.third_rif").popover("hide");
        }
    });
	$("button#thirdList").on("click", function() {
		listThirds();
	});
    $('.chart').easyPieChart({
        animate: 2000,
        scaleColor: false,
        lineWidth: 5,
        lineCap: 'square',
        size: 30,
        trackColor: '#e5e5e5',
        barColor: '#3da0ea'
    });
    $(".spanChart").popover({
        html : true,
        placement : "right",
        title : "Asistencia",
        content: function() {
            return $($(this).data("contentx")).html();
        }
    }).tooltip({title:"Click Detalles", placement : "bottom"});
    $('input.court_start').datetimepicker({
        format: 'DD/MM/YYYY'
    }).on('dp.change',function() {
        enrollsCourtUp($(this));
    });
    $('input.court_end').datetimepicker({
        format: 'DD/MM/YYYY'
    }).on('dp.change',function() {
        enrollsCourtUp($(this));
    });
    $("input.court_start").on("dp.change", function (e) {
        $("input#court_end_" + $(this).attr("court_id") + "").data("DateTimePicker").minDate(e.date);
    });
    $("input[court_id], select[court_id]").on('change', function(){
        enrollsCourtUp($(this));
    });
    $("input[item_matter], select[item_matter]").on('change', function(){
        enrollsNoteUp($(this));
    });
    representTablaList = $("table#representTabla").dataTable({
        "aaData": arrayDataRepresentTabla,
        "bPaginate": false,
        "bFilter": false,
        "bInfo": false,
        "scrollX": true,
        "bSortCellsTop": false,
        "bProcessing": true,
        "aoColumns": [
            {"bVisible" : false, "sClass" : "third_id", "bSearchable" : false, "asSorting" : false},
            {"sClass" : "third_names", "bSearchable" : false, "asSorting" : false},
            {"sClass" : "third_rif", "bSearchable" : false, "asSorting" : false},
            {"sClass" : "third_relName", "bSearchable" : false, "asSorting" : false},
            {"sClass" : "third_phone", "bSearchable" : false, "asSorting" : false},
            {"sClass" : "third_email", "bSearchable" : false, "asSorting" : false},
            {"sClass" : "accion", "bSearchable" : false, "asSorting" : false}
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).attr({"ref":aData.third_id});
            $("td",nRow).each(function(idx, tdObj) {
                $(tdObj).html("");
                for(aDataItem in aData) {
                    if($(tdObj).hasClass(aDataItem)) {
                        $(tdObj).html(aData[aDataItem]);
                    }
                }
            });
            $btnDiv = $("<div class=\"btn-group\"></div>");
            if($(this).data("update")) {
                $("<button type=\"button\" id=\"editRepresent_" + aData.third_id + "_\" class=\"btn btn-info\"></button>").tooltip({title: "Editar", placement : "left"}).on("click",function() {
                    aData.accion = "upd";
                    modalRepresent(aData);
                }).append("<i class=\"glyphicon glyphicon-edit\"></i>").appendTo($btnDiv);
            }
            if($(this).data("delete")) {
                $("<button type=\"button\" id=\"trashRepresent_" + aData.third_id + "_\" class=\"btn btn-danger\"></button>").tooltip({title: "Borrar", placement : "right"}).on("click",function() {
                    aData.accion = "del";
                    delRepresent(aData,$(this));
                }).append("<i class=\"glyphicon glyphicon-trash\"></i>").appendTo($btnDiv);
            }
            $("td.accion",nRow).html($btnDiv);
            return nRow;
        }
    });
    $(".dataTables_scrollBody").css({"overflow": "hidden"});
	$("button.printNotes").on("click", function(event) {
		event.preventDefault();
		var datos = $("form#thirdModify").serializeObject();
			datos.period_id = $(this).attr("period_id");
		var url = "pdf.php?rnd=" + targetCode + "&module=third&vista=studentsNotestPdf&datos=" + JSON.stringify(datos); 
		window.open(url, '_blank');
	});
	$("button.printNotesCourt").on("click", function(event) {
		event.preventDefault();
		var datos = $("form#thirdModify").serializeObject();
			datos.period_id = $(this).attr("period_id");
			datos.court_id 	= $(this).attr("court_id");
			datos.matter_id	= $(this).attr("matter_id");
		var url = "pdf.php?rnd=" + targetCode + "&module=third&vista=studentsNotestPdf&datos=" + JSON.stringify(datos); 
		window.open(url, '_blank');
	});
});