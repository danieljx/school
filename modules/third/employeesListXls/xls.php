<?php
$target	= JFunc::FTarget();
$mod	= JFactory::getModulo();    
$db		= JFactory::getDBO();
$user	= JFactory::getUser();
$datos	= json_decode(JRequest::getVar("datos"),true);
$sql = "Select t.third_id\n";
$sql.= "      ,t.third_name\n";
$sql.= "      ,t.third_lastname\n";
$sql.= "      ,concat(ifnull(t.third_name,''),' ',ifnull(t.third_lastname,'')) as third_names\n";
$sql.= "      ,t.third_rif\n";
$sql.= "      ,t.third_type\n";
$sql.= "      ,t.third_email\n";
$sql.= "      ,t.third_father\n";
$sql.= "      ,t.third_nationallity\n";
$sql.= "      ,t.third_website\n";
$sql.= "      ,t.third_description\n";
$sql.= "      ,t.third_phone\n";
$sql.= "      ,t.third_address\n";
$sql.= "      ,t.third_facebook\n";
$sql.= "      ,t.third_twitter\n";
$sql.= "      ,t.third_email\n";
$sql.= "      ,t.third_path\n";
$sql.= "      ,t.third_img\n";
$sql.= "      ,t.third_sta\n";
$sql.= "      ,t.third_sex\n";
$sql.= "      ,DATE_FORMAT(t.third_birth,'%d/%m/%Y') as third_birth\n";
$sql.= "      ,DATE_FORMAT(t.date_start,'%d/%m/%Y') as date_start\n";
$sql.= "      ,DATE_FORMAT(t.user_fec,'%d/%m/%Y %H:%i:%s') as user_fec\n";
$sql.= "      ,s.sta_name as third_status\n";
$sql.= "      ,tp.type_name as third_typeName\n";
$sql.= "From mod_third t\n";
$sql.= "Inner Join mod_third_status s on s.sta_id = t.third_sta\n";
$sql.= "Inner Join mod_third_type tp on tp.type_id = t.third_type\n";
$sql.= "Where t.third_id is not null\n";
if(!is_null($datos["ids"]) && !empty($datos["ids"])) {
	$sql.= "and t.third_id in (" . $datos["ids"] . ")\n";
} else {
	$sql.= "and t.third_id is null\n";
}
$db  = JFactory::getDBO();
$db->setQuery($sql);
$rows = $db->loadObjectList();
$datos = Array();
if(count($rows) > 0) {
	for($i=0; $i < count($rows); $i++) {
		$arrayItemDatos = Array();
		foreach($rows[$i] as $key => $value) {
			$arrayItemDatos[$key] 	= $value;
		}
		array_push($datos,$arrayItemDatos);
	}
}
echo "<table width=\"641\" border=\"1\">\n";
echo "<tr>\n";
echo "<th width=\"50%\" style=\"background-color:#006; text-align:center; color:#FFF\"><strong>Nombre</strong></th>\n";
echo "<th width=\"50%\" style=\"background-color:#006; text-align:center; color:#FFF\"><strong>CI/RIF</strong></th>\n";
echo "<th width=\"50%\" style=\"background-color:#006; text-align:center; color:#FFF\"><strong>Estatus</strong></th>\n";
echo "<th width=\"50%\" style=\"background-color:#006; text-align:center; color:#FFF\"><strong>Tipo</strong></th>\n";
echo "<th width=\"50%\" style=\"background-color:#006; text-align:center; color:#FFF\"><strong>E-Mail</strong></th>\n";
echo "</tr>\n";
for($i = 0; $i < count($datos); $i++) {
	echo "<tr>\n";
	echo "<td bgcolor=\"#ededed\" align=\"center\">" . $datos[$i]["third_names"]  ."</td>\n";
	echo "<td bgcolor=\"#ededed\" align=\"center\">" . $datos[$i]["third_rif"]  ."</td>\n";
	echo "<td bgcolor=\"#ededed\" align=\"center\">" . $datos[$i]["third_status"]  ."</td>\n";
	echo "<td bgcolor=\"#ededed\" align=\"center\">" . $datos[$i]["third_typeName"]  ."</td>\n";
	echo "<td bgcolor=\"#ededed\" align=\"center\">" . $datos[$i]["third_email"]  ."</td>\n";
	echo "</tr>\n";
}
echo "</table>\n";