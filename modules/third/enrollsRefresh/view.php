<?php
	$mod  = JFactory::getModulo();
	$db   = JFactory::getDBO();
	$datos = json_decode(JRequest::getVar("datos"),true);

	echo "		   <table id=\"periodMatter_" . $datos["period_id"] . "\" class=\"table table-bordered\">\n";
	echo "		       <thead>\n";
	echo "		           <tr>\n";
	echo "		               <th>Detalle</th>\n";
	echo "		               <th>Nivel</th>\n";
	echo "		               <th>Materias</th>\n";
	echo "		               <th>Seccion</th>\n";
	echo "		               <th>Asistencia</th>\n";
	echo "		               <th>Nota</th>\n";
	echo "		           </tr>\n";
	echo "		       </thead>\n";
	echo "		      <tbody>\n";
	$sqm = "Select tm.item_id\n";
	$sqm.= "      ,tm.third_id\n";
	$sqm.= "      ,tm.period_id\n";
	$sqm.= "      ,tm.sec_id\n";
	$sqm.= "      ,tm.matter_id\n";
	$sqm.= "      ,tm.item_sta\n";
	$sqm.= "      ,tm.item_note\n";
	$sqm.= "      ,DATE_FORMAT(tm.item_start,'%d/%m/%Y') as item_start\n";
	$sqm.= "      ,DATE_FORMAT(tm.item_end,'%d/%m/%Y') as item_end\n";
	$sqm.= "      ,DATE_FORMAT(tm.user_fec,'%d/%m/%Y %H:%i:%s') as user_fec\n";
	$sqm.= "      ,tm.user_id\n";
	$sqm.= "      ,l.level_id\n";
	$sqm.= "      ,l.level_name\n";
	$sqm.= "      ,m.matter_name\n";
	$sqm.= "      ,s.sec_code\n";
	$sqm.= "      ,n.note_class\n";
	$sqm.= "      ,s.sec_code\n";
	$sqm.= "      ,pm.item_id as teacher_third\n";
	$sqm.= "      ,ifnull(a.tassits,0) as tassits\n";
	$sqm.= "      ,ifnull(pm.item_assistance,0) as passits\n";
	$sqm.= "      ,FLOOR(ifnull((ifnull(a.tassits,0)*100)/ifnull(pm.item_assistance,0),0)) as assist\n";
	$sqm.= "From mod_third_matter tm\n";
	$sqm.= "Inner Join mod_matter m on m.matter_id = tm.matter_id\n";
	$sqm.= "Inner Join mod_level l on l.level_id = m.matter_level\n";
	$sqm.= "Left Join mod_section s on s.sec_id = tm.sec_id\n";
	$sqm.= "Left Join mod_note n on n.note_code = tm.item_note\n";
	$sqm.= "Left Join mod_teacher_matter pm on pm.matter_id = tm.matter_id and pm.sec_id = tm.sec_id\n";
	$sqm.= "Left Join (Select  xt.matter_third\n";
	$sqm.= "       			  ,sum(ifnull(xt.court_assists,0)) as tassits\n";
	$sqm.= "       	   From mod_third_matter_court xt\n";
	$sqm.= "       	   Where xt.matter_third is not null\n";
	$sqm.= "       	   Group by xt.matter_third) a on a.matter_third = tm.item_id\n";
	$sqm.= "Where tm.period_id = '" .$datos["period_id"] . "'\n";
	$sqm.= "  and tm.third_id = '" . $datos["third_id"] . "'\n";
	$sqm.= "Order by tm.item_end desc\n";
	//echo $sqm;
	$db->setQuery( $sqm );
	$rowm = $db->loadObjectList();
	for($m = 0; $m < count($rowm) ; $m++) {
		echo "		      <tr item=\"" . $rowm[$m]->item_id . "\">\n";
		echo "		      <td ref=\"det\" scope=\"row\">\n";
		echo "		        <div class=\"btn-group\">\n";
		echo "		            <button type=\"button\" plus=\"0\" id=\"plusNote_" . $rowm[$m]->item_id . "\" item_id=\"" . $rowm[$m]->item_id . "\" period_id=\"" .$datos["period_id"] . "\" onclick=\"plusItemdetaill(\$(this))\" class=\"btn btn-info\">\n";
		echo "		                <i class=\"glyphicon glyphicon-plus\"></i>\n";
		echo "		            </button>\n";
		echo "		        </div>\n";
		echo "		      </td>\n";
		echo "		      <td ref=\"level\">" . $rowm[$m]->level_name . "</td>\n";
		echo "		      <td ref=\"matter\">" . $rowm[$m]->matter_name . "</td>\n";
		echo "		      <td ref=\"section\">\n";
		if($datos["period_sta"] == "1") {
			echo "              <select class=\"form-control col-xs-12 selectpicker\" title=\"Selecione Secci�n\" item_matter=\"" . $rowm[$m]->item_id . "\" name=\"sec_id\" id=\"sec_id_" .$datos["period_id"] . "\">\n";
			$sqs = "Select s.sec_id\n";
			$sqs.= "      ,s.sec_code\n";
			$sqs.= "From mod_section s\n";
			$sqs.= "Where s.sec_sta = '1'\n";
			$sqs.= "  and s.sec_level = '" . $rowm[$m]->level_id . "'\n";
			$db->setQuery( $sqs );
			$rowsc = $db->loadObjectList();
			for($sc = 0; $sc < count($rowsc) ; $sc++) {
				echo "              <option value=\"" . $rowsc[$sc]->sec_id . "\" " . ($rowsc[$sc]->sec_id == $rowm[$m]->sec_id?"selected":"") . " data-content=\"<kbd>" . $rowsc[$sc]->sec_code . "</kbd>\">" . $rowsc[$sc]->sec_code . "</option>\n";
			}
			echo "              </select>\n";
		} else {
			echo "		      <kbd>" . $rowm[$m]->sec_code . "</kbd>\n";
		}
		echo "		      </td>\n";
		echo "		      <td ref=\"assist\">\n";
		echo "              <div class=\"chart\" data-percent=\"" . $rowm[$m]->assist . "\" style=\"position: relative; display: inline-block;\"><span class=\"spanChart\" data-contentX=\"#chartContentPopoverFatther_" . $rowm[$m]->item_id . "\" style=\"cursor:pointer; position: absolute; top: 20%; left: 115%;color: rgb(0, 149, 255);\">" . $rowm[$m]->assist . "%</span></div>\n";
		echo "              <div id=\"chartContentPopoverFatther_" . $rowm[$m]->item_id . "\" style=\"display: none\">\n";
		echo "                  <table id=\"chartTableContentPopoverFatther_" . $rowm[$m]->item_id . "\" class=\"table table-bordered\">\n";
		echo "		                <thead>\n";
		echo "		                    <tr>\n";
		echo "		                        <th>Cantidad Asistencia</th>\n";
		echo "		                        <th>Cantidad Clases</th>\n";
		echo "                          </tr>\n";
		echo "                      </thead>\n";
		echo "                      <tbody>\n";
		echo "		                    <tr>\n";
		echo "		                        <td>" . $rowm[$m]->tassits . "</td>\n";
		echo "		                        <td>" . $rowm[$m]->passits . "</td>\n";
		echo "                          </tr>\n";
		echo "                      </tbody>\n";
		echo "                  </table>\n";
		echo "              </div>\n";
		echo "            </td>\n";
		echo "		      <td ref=\"notes\">\n";
		if($datos["period_sta"] == "1") {
			echo "              <select class=\"form-control col-xs-12 selectpicker\" title=\"Selecione Nota\" item_matter=\"" . $rowm[$m]->item_id . "\"  name=\"item_note\" id=\"note_id_" . $rowm[$m]->item_id . "\">\n";
			$sqn = "Select n.note_id\n";
			$sqn.= "      ,n.note_code\n";
			$sqn.= "      ,n.note_class\n";
			$sqn.= "From mod_note n\n";
			$db->setQuery( $sqn );
			$rown = $db->loadObjectList();
			for($n = 0; $n < count($rown) ; $n++) {
				echo "              <option value=\"" . $rown[$n]->note_code . "\" " . ($rown[$n]->note_code == $rowm[$m]->item_note?"selected":"") . " data-content=\"<kbd class='" . $rown[$n]->note_class . "'>" . $rown[$n]->note_code . "</kbd>\">" . $rown[$n]->note_code . "</option>\n";
			}
			echo "              </select>\n";
		} else {
			echo "		      <kbd class=\"" . $rowm[$m]->note_class . "\">" . $rowm[$m]->item_note . "</kbd>\n";
		}
		echo "		      </td>\n";
		echo "		      </tr>\n";
		echo "		      <tr item=\"detaill_" . $rowm[$m]->item_id . "\" style=\"display:none;\">\n";
		echo "		      <td ref=\"det\" scope=\"row\" colspan=\"7\">\n";
		echo "		        <table id=\"detaillMatter_" .$datos["period_id"] . "_" . $rowm[$m]->item_id . "\" class=\"table table-bordered\">\n";
		echo "		            <thead>\n";
		echo "		                <tr>\n";
		echo "		                    <th style=\"width: 5%;\">Lapso</th>\n";
		echo "		                    <th style=\"width: 16%;\">Estatus</th>\n";
		echo "		                    <th style=\"width: 10%;\">Asistencias</th>\n";
		echo "		                    <th style=\"width: 10%;\">Nota</th>\n";
		echo "		                    <th style=\"width: 19%;\">Inicio</th>\n";
		echo "		                    <th style=\"width: 19%;\">Fin</th>\n";
		echo "		                    <th style=\"width: 18%;\">Accion</th>\n";
		echo "		                </tr>\n";
		echo "		            </thead>\n";
		echo "		            <tbody>\n";
			$sqc = "Select c.court_id\n";
			$sqc.= "      ,c.court_period\n";
			$sqc.= "      ,c.court_assists\n";
			$sqc.= "      ,c.matter_third\n";
			$sqc.= "      ,c.court_sta\n";
			$sqc.= "      ,c.court_note\n";
			$sqc.= "      ,c.court_desc\n";
			$sqc.= "      ,DATE_FORMAT(c.court_start,'%d/%m/%Y') as court_start\n";
			$sqc.= "      ,DATE_FORMAT(c.court_end,'%d/%m/%Y') as court_end\n";
			$sqc.= "      ,DATE_FORMAT(c.user_fec,'%d/%m/%Y %H:%i:%s') as user_fec\n";
			$sqc.= "      ,if(c.court_sta = 1, 'Nuevo',if(c.court_sta = 2,'En Curso','Culminado')) as court_status\n";
			$sqc.= "      ,n.note_class\n";
			$sqc.= "      ,ifnull(pm.court_assists,0) as passits\n";
			$sqc.= "      ,FLOOR(ifnull((ifnull(c.court_assists,0)*100)/ifnull(pm.court_assists,0),0)) as assist\n";
			$sqc.= "From mod_third_matter_court c\n";
			$sqc.= "Left Join mod_teacher_matter_court pm on pm.teacher_third = '" . $rowm[$m]->teacher_third . "' and pm.court_period = c.court_period\n";
			$sqc.= "Left Join mod_note n on n.note_code = c.court_note\n";
			$sqc.= "Where c.matter_third = '" . $rowm[$m]->item_id . "'\n";
			$sqc.= "Order by c.court_period desc\n";
			$db->setQuery( $sqc );
			$rowc = $db->loadObjectList();
			for($c = 0; $c < count($rowc) ; $c++) {
				echo "		      <tr court=\"" . $rowc[$c]->court_id . "\">\n";
				echo "		      <td ref= \"court_period\" scope=\"row\"><kbd>" . $rowc[$c]->court_period . "</kbd></td>\n";
				echo "		      <td ref=\"status\">\n";
				if($datos["period_sta"] == "1") {
					echo "              <select class=\"form-control col-xs-12 selectpicker\" title=\"Selecione Estatus\" name=\"court_sta\" id=\"sta_id_" . $rowc[$c]->court_id . "\" court_id=\"" . $rowc[$c]->court_id . "\">\n";
					echo "                  <option value=\"1\" " . ($rowc[$c]->court_sta == "1"?"selected":"") . ">Activo</option>\n";
					echo "                  <option value=\"2\" " . ($rowc[$c]->court_sta == "2"?"selected":"") . ">Culminado</option>\n";
					echo "              </select>\n";
				} else {
					echo "		      " .  $rowc[$c]->court_status . "\n";
				}
				echo "		      </td>\n";
				echo "		      <td ref=\"assist\">\n";
				if($datos["period_sta"] == "1") {
					echo "              <div class=\"input-group\">\n";
					echo "                  <span class=\"input-group-addon\">&#35;</span>\n";
					echo "                  <input type=\"text\" class=\"form-control col-xs-12\" placeholder=\"Asistencia\" id=\"assist_" . $rowc[$c]->court_id . "\" name=\"court_assists\" court_id=\"" . $rowc[$c]->court_id . "\" value=\"" .  $rowc[$c]->court_assists . "\">\n";
				   echo "               </div>\n";
				} else {
					echo "              <div class=\"chart\" data-percent=\"" . $rowc[$c]->assist . "\" style=\"position: relative; display: inline-block;\"><span class=\"spanChart\" data-contentX=\"#chartContentPopover_" . $rowc[$c]->court_id . "\" style=\"cursor:pointer; position: absolute; top: 20%; left: 115%;color: rgb(0, 149, 255);\">" . $rowc[$c]->assist . "%</span></div>\n";
					echo "              <div id=\"chartContentPopover_" . $rowc[$c]->court_id . "\" style=\"display: none\">\n";
					echo "                  <table id=\"chartTableContentPopover_" . $rowc[$c]->court_id . "\" class=\"table table-bordered\">\n";
					echo "		                <thead>\n";
					echo "		                    <tr>\n";
					echo "		                        <th>Cantidad Asistencia</th>\n";
					echo "		                        <th>Cantidad Clases</th>\n";
					echo "                          </tr>\n";
					echo "                      </thead>\n";
					echo "                      <tbody>\n";
					echo "		                    <tr>\n";
					echo "		                        <td>" . $rowc[$c]->court_assists . "</td>\n";
					echo "		                        <td>" . $rowc[$c]->passits . "</td>\n";
					echo "                          </tr>\n";
					echo "                      </tbody>\n";
					echo "                  </table>\n";
					echo "              </div>\n";
				}
				echo "            </td>\n";
				echo "		      <td ref=\"notes\">\n";
				if($datos["period_sta"] == "1") {
					echo "              <select class=\"form-control col-xs-12 selectpicker\" title=\"Selecione Nota\" court_id=\"" . $rowc[$c]->court_id . "\" name=\"court_note\" id=\"note_id_" . $rowc[$c]->court_id . "\">\n";
					$sqn = "Select n.note_id\n";
					$sqn.= "      ,n.note_code\n";
					$sqn.= "      ,n.note_class\n";
					$sqn.= "From mod_note n\n";
					$db->setQuery( $sqn );
					$rown = $db->loadObjectList();
					for($n = 0; $n < count($rown) ; $n++) {
						echo "              <option value=\"" . $rown[$n]->note_code . "\" " . ($rown[$n]->note_code == $rowc[$c]->court_note?"selected":"") . " data-content=\"<kbd class='" . $rown[$n]->note_class . "'>" . $rown[$n]->note_code . "</kbd>\">" . $rown[$n]->note_code . "</option>\n";
					}
					echo "              </select>\n";
				} else {
					echo "		      <kbd class=\"" . $rowc[$c]->note_class . "\">" . $rowc[$c]->court_note . "</kbd>\n";
				}
				echo "		      </td>\n";
				echo "		      <td ref=\"start\">\n";
				if($datos["period_sta"] == "1") {
					echo "              <div class=\"input-group\">\n";
					echo "                  <span class=\"input-group-addon\"><i class=\"glyphicon glyphicon-calendar\"></i></span>\n";
					echo "                  <input type=\"text\" class=\"form-control court_start\" placeholder=\"d/m/Y\" id=\"court_start_" . $rowc[$c]->court_id . "\" name=\"court_start\" court_id=\"" . $rowc[$c]->court_id . "\" value=\"" .  $rowc[$c]->court_start . "\">\n";
					echo "               </div>\n";
				} else {
					echo "		      " .  $rowc[$c]->court_start . "\n";
				}
					echo "		      </td>\n";
					echo "		      <td ref=\"end\">\n";
				if($datos["period_sta"] == "1") {
					echo "              <div class=\"input-group\">\n";
					echo "                  <span class=\"input-group-addon\"><i class=\"glyphicon glyphicon-calendar\"></i></span>\n";
					echo "                  <input type=\"text\" class=\"form-control col-xs-12 court_end\" placeholder=\"d/m/Y\" id=\"court_end_" . $rowc[$c]->court_id . "\" name=\"court_end\" court_id=\"" . $rowc[$c]->court_id . "\" value=\"" .  $rowc[$c]->court_end . "\">\n";
					echo "               </div>\n";
				} else {
					echo "		      " .  $rowc[$c]->court_start . "\n";
				}
				echo "		      </td>\n";
					echo "		      <td ref=\"accion\">\n";
					echo "		        <div class=\"btn-group\">\n";
				if($datos["period_sta"] != "2") {
					echo "		            <button type=\"button\" title=\"Editar\" id=\"editNotes_" . $rowc[$c]->court_id . "\" court_id=\"" . $rowc[$c]->court_id . "\" court_desc=\"" . $rowc[$c]->court_desc . "\" court_period=\"" . $rowc[$c]->court_period . "\" class=\"btn btn-primary btn-sm\" onclick=\"upNoteDesc(\$(this))\">\n";
					echo "		                <i class=\"glyphicon glyphicon-edit\"></i>\n";
					echo "		                Boleta\n";
					echo "		            </button>\n";
					echo "		            <button type=\"button\" title=\"Imprimir\" style=\"height: 30px;\" id=\"printNotes_" . $rowc[$c]->court_id . "\" court_id=\"" . $rowc[$c]->court_id . "\" class=\"btn btn-primary btn-sm\">\n";
					echo "		                <i class=\"glyphicon glyphicon-print\"></i>\n";
					echo "		            </button>\n";
				} else {
					echo "		            <button type=\"button\" title=\"Imprimir\" id=\"printNotes_" . $rowc[$c]->court_id . "\" court_id=\"" . $rowc[$c]->court_id . "\" class=\"btn btn-success btn-sm\">\n";
					echo "		                <i class=\"glyphicon glyphicon-print\"></i>\n";
					echo "		                Boleta\n";
					echo "		            </button>\n";
				}
				echo "		        </div>\n";
				echo "		      </td>\n";
				echo "		      </tr>\n";
			}
		echo "		            </tbody>\n";
		echo "		        </table>\n";
		echo "		      </td>\n";
		echo "		      </tr>\n";
	}
	echo "		      </tbody>\n";
	echo "		   </table>\n";