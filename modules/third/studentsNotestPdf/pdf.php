<?php
$target	= JFunc::FTarget();
$mod	= JFactory::getModulo();    
$db		= JFactory::getDBO();
$user	= JFactory::getUser();
$datos	= json_decode(JRequest::getVar("datos"),true);
$sql = "Select p.period_id\n";
$sql.= "      ,p.period_code\n";
$sql.= "      ,p.period_sta\n";
$sql.= "      ,DATE_FORMAT(p.period_start,'%d/%m/%Y') as period_start\n";
$sql.= "      ,DATE_FORMAT(p.period_end,'%d/%m/%Y') as period_end\n";
$sql.= "      ,DATE_FORMAT(p.user_fec,'%d/%m/%Y %H:%i:%s') as user_fec\n";
$sql.= "      ,concat(p.period_code,': ' ,DATE_FORMAT(p.period_start,'%d/%m/%Y'),' - ',DATE_FORMAT(p.period_end,'%d/%m/%Y')) as period_name\n";
$sql.= "      ,p.user_id\n";
$sql.= "      ,if(p.period_sta = 1,'Activo','Culminado') as period_status\n";
$sql.= "      ,if(e.enroll_id is null,'No Inscrito','Inscrito') as enroll_status\n";
$sql.= "      ,if(p.period_sta = 1,'info',if(e.enroll_id is not null,'success','danger')) as color_status\n";
$sql.= "      ,if(e.enroll_id is null, 'NO','YES') as enroll\n";
$sql.= "From mod_period p\n";
$sql.= "Left Join mod_enroll e on e.period_id = p.period_id and e.third_id = '" . $datos["third_id"] . "'\n";
$sql.= "Where p.period_id = '" . $datos["period_id"] . "'\n";
$sql.= "Order by p.period_start desc\n";
$db->setQuery( $sql );
$rows = $db->loadObjectList();
if(count($rows) > 0) {
	$datos["period_id"] 	= $rows[0]->period_id;
	$datos["period_code"] 	= $rows[0]->period_code;
	$datos["period_start"] 	= $rows[0]->period_start;
	$datos["period_end"] 	= $rows[0]->period_end;
}
$pdf 	= new PDFDoc();
$pdf->setTitle("INFORME DESCRIPTIVO DE LA ACTUACIÓN DEL ESTUDIANTE");
$pdf->setInicio();
$pdf->SetXY(10, 45);
$pdf->SetTextColor(0, 0, 10);
$pdf->SetFont('helvetica', 'B', 9);
$pdf->Cell(45, 5, html_entity_decode("NOMBRES Y APELLIDOS:"), 0, 0, 'L');
$pdf->SetFont('helvetica', '', 9);
$pdf->Cell(50, 5, html_entity_decode($datos["third_name"] . " " . $datos["third_lastname"]), 0, 2, 'L');
$pdf->SetXY(10, 50);
$pdf->SetFont('helvetica', 'B', 9);
$pdf->Cell(45, 5, html_entity_decode("PERÍODO ACADÉMICO:"), 0, 0, 'L');
$pdf->SetFont('helvetica', '', 9);
$pdf->Cell(50, 5, html_entity_decode($datos["period_code"] . " - DESDE: " . $datos["period_start"] . " HASTA: " . $datos["period_end"] . ""), 0, 2, 'L');
$sqm = "Select tm.item_id\n";
$sqm.= "      ,tm.third_id\n";
$sqm.= "      ,tm.period_id\n";
$sqm.= "      ,tm.sec_id\n";
$sqm.= "      ,tm.matter_id\n";
$sqm.= "      ,tm.item_sta\n";
$sqm.= "      ,tm.item_note\n";
$sqm.= "      ,DATE_FORMAT(tm.item_start,'%d/%m/%Y') as item_start\n";
$sqm.= "      ,DATE_FORMAT(tm.item_end,'%d/%m/%Y') as item_end\n";
$sqm.= "      ,DATE_FORMAT(tm.user_fec,'%d/%m/%Y %H:%i:%s') as user_fec\n";
$sqm.= "      ,tm.user_id\n";
$sqm.= "      ,l.level_id\n";
$sqm.= "      ,l.level_name\n";
$sqm.= "      ,m.matter_name\n";
$sqm.= "      ,s.sec_code\n";
$sqm.= "      ,n.note_class\n";
$sqm.= "      ,s.sec_code\n";
$sqm.= "      ,pm.item_id as teacher_third\n";
$sqm.= "      ,ifnull(a.tassits,0) as tassits\n";
$sqm.= "      ,ifnull(pm.item_assistance,0) as passits\n";
$sqm.= "      ,FLOOR(ifnull((ifnull(a.tassits,0)*100)/ifnull(pm.item_assistance,0),0)) as assist\n";
$sqm.= "From mod_third_matter tm\n";
$sqm.= "Inner Join mod_matter m on m.matter_id = tm.matter_id\n";
$sqm.= "Inner Join mod_level l on l.level_id = m.matter_level\n";
$sqm.= "Left Join mod_section s on s.sec_id = tm.sec_id\n";
$sqm.= "Left Join mod_note n on n.note_code = tm.item_note\n";
$sqm.= "Left Join mod_teacher_matter pm on pm.matter_id = tm.matter_id and pm.sec_id = tm.sec_id\n";
$sqm.= "Left Join (Select  xt.matter_third\n";
$sqm.= "       			  ,sum(ifnull(xt.court_assists,0)) as tassits\n";
$sqm.= "       	   From mod_third_matter_court xt\n";
$sqm.= "       	   Where xt.matter_third is not null\n";
$sqm.= "       	   Group by xt.matter_third) a on a.matter_third = tm.item_id\n";
$sqm.= "Where tm.period_id = '" . $datos["period_id"] . "'\n";
$sqm.= "  and tm.third_id = '" . $datos["third_id"] . "'\n";
if(isset($datos["matter_id"]) && !empty($datos["matter_id"])) {
	$sqm.= "  and tm.matter_id = '" . $datos["matter_id"] . "'\n";
}
$sqm.= "Order by tm.item_end desc\n";
//echo $sqm;
$db->setQuery( $sqm );
$rowm = $db->loadObjectList();
for($m = 0; $m < count($rowm) ; $m++) {
	$pdf->SetXY(10,55);
	$pdf->SetFont('helvetica', 'B', 9);
	$pdf->Cell(45, 5, html_entity_decode("MATERIA:"), 0, 0, 'L');
	$pdf->SetFont('helvetica', '', 9);
	$pdf->Cell(50, 5, html_entity_decode($rowm[$m]->matter_name), 0, 2, 'L');
	$pdf->SetXY(10, 60);
	$pdf->SetFont('helvetica', 'B', 9);
	$pdf->Cell(45, 5, html_entity_decode("NIVEL:"), 0, 0, 'L');
	$pdf->SetFont('helvetica', '', 9);
	$pdf->Cell(50, 5, html_entity_decode($rowm[$m]->level_name), 0, 2, 'L');
	$sqc = "Select c.court_id\n";
	$sqc.= "      ,c.court_period\n";
	$sqc.= "      ,c.court_assists\n";
	$sqc.= "      ,c.matter_third\n";
	$sqc.= "      ,c.court_sta\n";
	$sqc.= "      ,c.court_note\n";
	$sqc.= "      ,c.court_desc\n";
	$sqc.= "      ,DATE_FORMAT(c.court_start,'%d/%m/%Y') as court_start\n";
	$sqc.= "      ,DATE_FORMAT(c.court_end,'%d/%m/%Y') as court_end\n";
	$sqc.= "      ,DATE_FORMAT(c.user_fec,'%d/%m/%Y %H:%i:%s') as user_fec\n";
	$sqc.= "      ,if(c.court_sta = 1, 'Nuevo',if(c.court_sta = 2,'En Curso','Culminado')) as court_status\n";
	$sqc.= "      ,n.note_class\n";
	$sqc.= "      ,ifnull(pm.court_assists,0) as passits\n";
	$sqc.= "      ,FLOOR(ifnull((ifnull(c.court_assists,0)*100)/ifnull(pm.court_assists,0),0)) as assist\n";
	$sqc.= "From mod_third_matter_court c\n";
	$sqc.= "Left Join mod_teacher_matter_court pm on pm.teacher_third = '" . $rowm[$m]->teacher_third . "' and pm.court_period = c.court_period\n";
	$sqc.= "Left Join mod_note n on n.note_code = c.court_note\n";
	$sqc.= "Where c.matter_third = '" . $rowm[$m]->item_id . "'\n";
	if(isset($datos["court_id"]) && !empty($datos["court_id"])) {
		$sqc.= "  and c.court_id = '" . $datos["court_id"] . "'\n";
	}
	$sqc.= "Order by c.court_period desc\n";
	$db->setQuery( $sqc );
	$rowc = $db->loadObjectList();
	for($c = 0; $c < count($rowc) ; $c++) {
		$pdf->SetXY(10, 65);
		$pdf->SetFont('helvetica', 'B', 9);
		$pdf->Cell(45, 5, html_entity_decode("LAPSO:"), 0, 0, 'L');
		$pdf->SetFont('helvetica', '', 9);
		if($rowc[$c]->court_period == 1) {
			$pdf->Cell(50, 5, html_entity_decode("1RO (X) 2DO (__) 3RO (__) "), 0, 2, 'L');
		} else if($rowc[$c]->court_period == 2) {
			$pdf->Cell(50, 5, html_entity_decode("1RO (__) 2DO (X) 3RO (__) "), 0, 2, 'L');
		} else if($rowc[$c]->court_period == 3) {
			$pdf->Cell(50, 5, html_entity_decode("1RO (__) 2DO (__) 3RO (X) "), 0, 2, 'L');
		}
		$pdf->SetXY(10, 80);
		$pdf->SetFont('helvetica', '', 12);
		// echo $rowc[$c]->court_desc;
		$pdf->MultiCell(190, 190, html_entity_decode($rowc[$c]->court_desc), 0, 'L');
		// $pdf->MultiCell(190, 190, html_entity_decode("Es una niña creativa y de muestra compañerismo. Coopera en la realización de trabajos en el aula. Le gusta intervenir en la discusión de temas que le causan interés. Identifica palabras que pertenecen a la misma familia.  Lee oraciones completas y  toma dictado. Buena caligrafía. Resuelve adiciones y sustracciones de una cifra, oralmente y escritas. Escribe los números del 1 al 100.Realiza dibujos sobre Simón Bolívar y le gusta escuchar historias relacionadas con su vida. Describe elementos del ambiente. Relaciona el reciclaje como medio para mantener un ambiente limpio y sano. Realiza manualidades y maquetas con material reciclable. Ejecuta buenas exposiciones sobre el proyecto."), 0, 'L');
		$pdf->SetFont('helvetica', 'B', 10);
		$pdf->SetXY(10, 190);
		$pdf->MultiCell(190, 20, html_entity_decode("Observaciones del Representante: _______________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________"), 0, 'L');
		$pdf->SetXY(45, 215);
		$pdf->SetFont('helvetica', 'B', 9);
		$pdf->Cell(120, 8, html_entity_decode("REGISTRO DE ASISTENCIA E INASISTENCIA"), 1, 2, 'C');
		$pdf->Cell(30, 8, html_entity_decode("DÍAS HÁBILES"), 1, 0, 'C');
		$pdf->Cell(30, 8, html_entity_decode("ASISTENCIA"), 1, 0, 'C');
		$pdf->Cell(30, 8, html_entity_decode("INASISTENCIA"), 1, 0, 'C');
		$pdf->Cell(30, 8, html_entity_decode("PORCENTAJE"), 1, 0, 'C');
		$pdf->SetXY(45, 231);
		$pdf->Cell(30, 8, html_entity_decode($rowc[$c]->passits), 1, 0, 'C');
		$pdf->Cell(30, 8, html_entity_decode($rowc[$c]->court_assists), 1, 0, 'C');
		$pdf->Cell(30, 8, html_entity_decode(($rowc[$c]->passits - $rowc[$c]->court_assists)), 1, 0, 'C');
		$pdf->Cell(30, 8, html_entity_decode($rowc[$c]->assist . "%"), 1, 0, 'C');
		
		$pdf->SetXY(10, 240);
		$pdf->SetFont('helvetica', 'B', 9);
		$pdf->Cell(45, 5, html_entity_decode("FIRMAS:"), 0, 0, 'L');
		
		$pdf->SetXY(10, 255);
		$pdf->SetFont('helvetica', 'B', 9);
		$pdf->Cell(45, 5, html_entity_decode("_________________________"), 0, 2, 'C');
		$pdf->Cell(45, 5, html_entity_decode("REPRESENTANTE"), 0, 0, 'C');
		
		$pdf->SetXY(60, 255);
		$pdf->SetFont('helvetica', 'B', 9);
		$pdf->Cell(45, 5, html_entity_decode("_________________________"), 0, 2, 'C');
		$pdf->Cell(45, 5, html_entity_decode("DIRECTOR (A)"), 0, 0, 'C');
		
		$pdf->SetXY(110, 255);
		$pdf->SetFont('helvetica', 'B', 9);
		$pdf->Cell(45, 5, html_entity_decode(""), 0, 2, 'C');
		$pdf->Cell(45, 5, html_entity_decode("SELLO"), 0, 0, 'C');
		
		$pdf->SetXY(160, 255);
		$pdf->SetFont('helvetica', 'B', 9);
		$pdf->Cell(45, 5, html_entity_decode("_________________________"), 0, 2, 'C');
		$pdf->Cell(45, 5, html_entity_decode("DOCENTE"), 0, 0, 'C');
		
		$pdf->SetFont('helvetica', '', 9);
		if(($c+1) < count($rowc)) {
			$pdf->addPage();
			$pdf->SetXY(10, 45);
			$pdf->SetTextColor(0, 0, 10);
			$pdf->SetFont('helvetica', 'B', 9);
			$pdf->Cell(45, 5, html_entity_decode("NOMBRES Y APELLIDOS:"), 0, 0, 'L');
			$pdf->SetFont('helvetica', '', 9);
			$pdf->Cell(50, 5, html_entity_decode($datos["third_name"] . " " . $datos["third_lastname"]), 0, 2, 'L');
			$pdf->SetXY(10, 50);
			$pdf->SetFont('helvetica', 'B', 9);
			$pdf->Cell(45, 5, html_entity_decode("PERÍODO ACADÉMICO:"), 0, 0, 'L');
			$pdf->SetFont('helvetica', '', 9);
			$pdf->Cell(50, 5, html_entity_decode($datos["period_code"] . " - DESDE: " . $datos["period_start"] . " HASTA: " . $datos["period_end"] . ""), 0, 2, 'L');
			$pdf->SetXY(10,55);
			$pdf->SetFont('helvetica', 'B', 9);
			$pdf->Cell(45, 5, html_entity_decode("MATERIA:"), 0, 0, 'L');
			$pdf->SetFont('helvetica', '', 9);
			$pdf->Cell(50, 5, html_entity_decode($rowm[$m]->matter_name), 0, 2, 'L');
			$pdf->SetXY(10, 60);
			$pdf->SetFont('helvetica', 'B', 9);
			$pdf->Cell(45, 5, html_entity_decode("NIVEL:"), 0, 0, 'L');
			$pdf->SetFont('helvetica', '', 9);
			$pdf->Cell(50, 5, html_entity_decode($rowm[$m]->level_name), 0, 2, 'L');
		}
	}
	if(($m+1) < count($rowm)) {
		$pdf->addPage();
		$pdf->SetXY(10, 45);
		$pdf->SetTextColor(0, 0, 10);
		$pdf->SetFont('helvetica', 'B', 9);
		$pdf->Cell(45, 5, html_entity_decode("NOMBRES Y APELLIDOS:"), 0, 0, 'L');
		$pdf->SetFont('helvetica', '', 9);
		$pdf->Cell(50, 5, html_entity_decode($datos["third_name"] . " " . $datos["third_lastname"]), 0, 2, 'L');
		$pdf->SetXY(10, 50);
		$pdf->SetFont('helvetica', 'B', 9);
		$pdf->Cell(45, 5, html_entity_decode("PERÍODO ACADÉMICO:"), 0, 0, 'L');
		$pdf->SetFont('helvetica', '', 9);
		$pdf->Cell(50, 5, html_entity_decode($datos["period_code"] . " - DESDE: " . $datos["period_start"] . " HASTA: " . $datos["period_end"] . ""), 0, 2, 'L');
	}
}
$pdf->Output('Notas-'. date("d-m-Y-H-i-s"), 'I');