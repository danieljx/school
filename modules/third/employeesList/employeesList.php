<?php
	$target = JFunc::FTarget();
	$mod	= JFactory::getModulo();
	$db		= JFactory::getDBO();
	$user	= JFactory::getUser();
	$back	= JRequest::getVar("back");
	if(!empty($back)) {
		$back	= json_decode($back,true);
	} else {
		$back	= json_decode("[]",true);
	}
	$back["third_type"] = ["not in" => ["2","3"]];
	$back["view"]		= "list";
	$third 	= new JThirds($back);
?>
<script>
	objDataBack   = <?php echo json_encode($back); ?>;
	arrayDataList = <?php echo json_encode($third->getThirds()); ?>;
</script>
<h2 class="page-header">Empleado</h2>
<div class="panel panel-info">
	<div class="panel-heading">
		<h3 id="panel-title" class="panel-title">Filtro de Empleados</h3>
	</div>
	<div class="panel-body">
		<form id="employeesFilter" data-module="third" data-vista="employeesData" class="form-horizontal formList" method="POST" target="<?PHP echo $target; ?>">
			<div class="form-group">
				<div class="col-xs-4">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
						<input type="text" class="form-control" placeholder="Nombres del Empleado" id="third_name" name="third_name">
					</div>
				</div>
				<div class="col-xs-4">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
						<input type="text" class="form-control" placeholder="ejemplo@ejemplo.com" id="third_email" name="third_email">
					</div>
				</div>
				<div class="col-xs-4">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-stats"></span></span>
						<select class="form-control selectpicker" title='Selecione Estatus' name="third_sta" id="third_sta" multiple>
							<?php
							$sql = "Select sta_id\n";
							$sql.= "	  ,sta_name\n";
							$sql.= "From mod_third_status\n";
							$sql.= "Where sta_id is not null\n";
							$db->setQuery( $sql );
							$rows = $db->loadObjectList();
							for($i=0; $i < count($rows); $i++) {
								echo "<option value=\"" . $rows[$i]->sta_id . "\" >" .  $rows[$i]->sta_name . "</option>\n";
							}
							?>
						</select>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-4">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-tasks"></span></span>
						<select class="form-control selectpicker" title='Selecione Tipo empleados' name="third_type" id="third_type" multiple>
							<?php
							$sql = "Select type_id\n";
							$sql.= "	  ,type_name\n";
							$sql.= "From mod_third_type\n";
							$sql.= "Where type_id not in (" . implode($back["third_type"]["not in"]) . ")\n";
							$db->setQuery( $sql );
							$rows = $db->loadObjectList();
							for($i=0; $i < count($rows); $i++) {
								echo "<option value=\"" . $rows[$i]->type_id . "\" >" .  $rows[$i]->type_name . "</option>\n";
							}
							?>
						</select>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="container-fluid">
					<div class="btn-group">
						<button type="submit" class="btn btn-primary">
							<i class="glyphicon glyphicon-filter"></i>
							Filtrar
						</button>
						<button type="reset" class="btn btn-warning">
							<i class="glyphicon glyphicon-erase"></i>
							Limpiar
						</button>
						<?php if($mod->getInsert()) { ?>
						<button id="newEmployees" type="button" class="btn btn-info">
							<i class="glyphicon glyphicon-edit"></i>
							Nuevo
						</button>
						<?php } ?>
						<?php if($mod->getPrint()) { ?>
						<button id="pdfListTeachers" type="button" class="btn btn-danger pdf" data-module="third" data-view="employeesListPdf" data-title="Listado Empleados">
							<i class="glyphicon glyphicon-print"></i>
							Pdf
						</button>
						<?php } ?>
						<?php if($mod->getExport()) { ?>
						<button id="newTeachers" data-module="third" type="button" class="btn btn-success excel" data-module="third" data-view="employeesListXls" data-title="Listado Empleados">
							<i class="glyphicon glyphicon-share"></i>
							Excel
						</button>
						<?php } ?>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<div class="container-fluid">
	<div class="table-responsive">
		<table id="employeesTabla" class="table dataTableList table-bordered" data-module="third" data-del-view="employeesUp" data-update="<?php echo $mod->getUpdate(); ?>" data-delete="<?php echo $mod->getDelete(); ?>">
			<thead>
				<tr>
					<th data-visible="false" data-type="num" data-class="third_id" data-bSearchable="false" data-bSortable="false">#</th>
					<th data-type="string" data-class="third_names" data-bSearchable="true" data-bSortable="true">Nombre</th>
					<th data-type="string" data-class="third_rif" data-bSearchable="true" data-bSortable="true">CI/RIF</th>
					<th data-type="string" data-class="third_status" data-bSearchable="true" data-bSortable="true">Estatus</th>
					<th data-type="string" data-class="third_typeName" data-bSearchable="true" data-bSortable="true">Tipo</th>
					<th data-type="string" data-class="third_email" data-bSearchable="true" data-bSortable="true">E-Mail</th>
					<th data-type="string" data-class="accion" data-bSearchable="false" data-bSortable="false">Accion</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
</div>