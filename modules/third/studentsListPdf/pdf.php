<?php
$target	= JFunc::FTarget();
$mod	= JFactory::getModulo();    
$db		= JFactory::getDBO();
$user	= JFactory::getUser();
$datos	= json_decode(JRequest::getVar("datos"),true);
$title	= JRequest::getVar("title");
$sql = "Select t.third_id\n";
$sql.= "      ,t.third_name\n";
$sql.= "      ,t.third_lastname\n";
$sql.= "      ,concat(ifnull(t.third_name,''),' ',ifnull(t.third_lastname,'')) as third_names\n";
$sql.= "      ,t.third_rif\n";
$sql.= "      ,t.third_type\n";
$sql.= "      ,t.third_email\n";
$sql.= "      ,t.third_father\n";
$sql.= "      ,t.third_nationallity\n";
$sql.= "      ,t.third_website\n";
$sql.= "      ,t.third_description\n";
$sql.= "      ,t.third_phone\n";
$sql.= "      ,t.third_address\n";
$sql.= "      ,t.third_facebook\n";
$sql.= "      ,t.third_twitter\n";
$sql.= "      ,t.third_email\n";
$sql.= "      ,t.third_path\n";
$sql.= "      ,t.third_img\n";
$sql.= "      ,t.third_sta\n";
$sql.= "      ,t.third_sex\n";
$sql.= "      ,DATE_FORMAT(t.third_birth,'%d/%m/%Y') as third_birth\n";
$sql.= "      ,DATE_FORMAT(t.date_start,'%d/%m/%Y') as date_start\n";
$sql.= "      ,DATE_FORMAT(t.user_fec,'%d/%m/%Y %H:%i:%s') as user_fec\n";
$sql.= "      ,s.sta_name as third_status\n";
$sql.= "      ,tp.type_name as third_typeName\n";
$sql.= "From mod_third t\n";
$sql.= "Inner Join mod_third_status s on s.sta_id = t.third_sta\n";
$sql.= "Inner Join mod_third_type tp on tp.type_id = t.third_type\n";
$sql.= "Where t.third_id is not null\n";
if(!is_null($datos["ids"]) && !empty($datos["ids"])) {
	$sql.= "and t.third_id in (" . $datos["ids"] . ")\n";
} else {
	$sql.= "and t.third_id is null\n";
}
$db  = JFactory::getDBO();
$db->setQuery($sql);
$rows = $db->loadObjectList();
$datos = Array();
if(count($rows) > 0) {
	for($i=0; $i < count($rows); $i++) {
		$arrayItemDatos = Array();
		foreach($rows[$i] as $key => $value) {
			$arrayItemDatos[$key] 	= $value;
		}
		array_push($datos,$arrayItemDatos);
	}
}
$pdf 	= new PDFList();
$pdf->setTitle($title);
$pdf->setInicio();
$pdf->SetXY(10, 45);
$pdf->SetTextColor(0, 0, 10);
$pdf->SetFont('helvetica', 'B', 8);
$pdf->Cell(40, 5, html_entity_decode("Nombre"), 1, 0, 'C');
$pdf->Cell(30, 5, html_entity_decode("CI/RIF"), 1, 0, 'C');
$pdf->Cell(20, 5, html_entity_decode("Estatus"), 1, 0, 'C');
$pdf->Cell(20, 5, html_entity_decode("E-Mail"), 1, 0, 'C');
$pdf->SetXY(10,50);
for($i = 0; $i < count($datos); $i++) {
	$pdf->SetX(10);
	$pdf->SetFont('helvetica', '', 8);
	$CebColor = array();
	$CebColor[0] = (($i%2==0)?"255":"225");
	$CebColor[1] = (($i%2==0)?"255":"225");
	$CebColor[2] = (($i%2==0)?"255":"225");
	$pdf->SetFillColorArray($CebColor);
	$pdf->Cell(40, 5, html_entity_decode($datos[$i]["third_names"]), 1, 0, 'C', true, '', 1);
	$pdf->Cell(30, 5, html_entity_decode($datos[$i]["third_rif"]), 1, 0, 'C', true, '', 1);
	$StaColor = array();
	if($datos[$i]["third_sta"] == "1") {
		$StaColor[0] = (($i%2==0)?"162":"197");
		$StaColor[1] = (($i%2==0)?"232":"232");
		$StaColor[2] = (($i%2==0)?"162":"197");
	} else {
		$StaColor[0] = (($i%2==0)?"240":"240");
		$StaColor[1] = (($i%2==0)?"168":"204");
		$StaColor[2] = (($i%2==0)?"168":"204");
	}
	$pdf->SetFillColorArray($StaColor);
	$pdf->Cell(20, 5, html_entity_decode($datos[$i]["third_status"]), 1, 0, 'C', true, '', 1);
	$pdf->SetFillColorArray($CebColor);
	$pdf->SetFont('helvetica', 'B', 8);
	$pdf->Cell(20, 5, html_entity_decode($datos[$i]["third_email"]), 1, 0, 'C', true, '', 1);
	$pdf->Ln();
}
$pdf->Output('Listado-'. date("d-m-Y-H-i-s"), 'I');