<?php
class JSections {
    private $sec_id		= null;
    private $sec_sta	= null;
    private $sec_code	= null;
    private $sec_level	= null;
    private $user_id	= null;
    private $user_fec	= null;
    private $accion		= null;
    private $view		= "list";

    function __construct($datos = array()) {
		$this->requestSections($datos);
	}
    public function getDisable($tipo = "campo") {
    	$mod  = JFactory::getModulo();
    	if($this->accion == "add" && !$mod->getInsert()) {
			return "disabled";
		} else if($this->accion == "upd" && !$mod->getUpdate()) {
			return "disabled";
		} else if($this->accion == "del" && $tipo=="campo") {
			return "disabled";
		} else if($this->accion == "del" && !$mod->getDelete()) {
			return "disabled";
		}
    	return "";
	}
	public function getClassAtribute() {
        return get_object_vars($this);
    }
    public function requestSections($datos = array()) {
		if(count($datos) > 0) {
			foreach($datos as $key => $value) {
                if(array_key_exists($key,$this->getClassAtribute())) {
                    $this->$key =  $value;
                }
			}
		}
    }
    public function setSections() {
    	$user = JFactory::getUser();
    	$mod  = JFactory::getModulo();
    	$db   = JFactory::getDBO();
		$temp = array("status" => "ERROR", "type" => "alert-danger", "text" => "NO TIENE ACCESO", "id" => "");
    	if($this->accion != "del") {
			$obj 	= new stdClass();
			$datos	= $this->getClassAtribute();
			if(count($datos) > 0) {
				foreach($datos as $key => $value) {
                    if($key != "accion" && $key != "view" && !is_null($value) && !empty($value)) {
						$obj->$key =  $value;
					}
				}
			}
			$obj->user_id	= $user->getId();
			$obj->user_fec  = 'NOW_AHORA';
    	  	if($this->accion == "add" && $mod->getInsert()) {
				if(!$db->insertObject('mod_section', $obj,'sec_id')) {
                    $temp["id"]  	= "";
                    $temp["text"]  	= $db->getErrorMsg();
				} else {
					$this->sec_id = $obj->sec_id;
					$temp["id"]		= $obj->sec_id;
					$temp["status"]	= "OK";
					$temp["text"]  	= "Operacion Satisfactoria";
					$temp["type"]  	= "alert-info";
				}   	  	  	
			} else if($mod->getUpdate()) {
				$obj->sec_id   = (int)$this->sec_id;
				if(!$db->updateObject('mod_section', $obj,"sec_id")) {
					$temp["id"]  	= "";
					$temp["text"]  	= $db->getErrorMsg();
				} else {
					$temp["id"]  	= $obj->sec_id;
					$temp["status"]	= "OK";
					$temp["text"]  	= "Operacion Satisfactoria";
					$temp["type"]  	= "alert-info";
				}
			}
		} else {
    	  	$sql = "DELETE FROM mod_section WHERE sec_id = " . $this->sec_id;
    	  	if(!$db->Execute($sql)) {					
				$temp["id"]  	= "";
				$temp["text"]  	= $db->getErrorMsg();
			} else {
				$temp["status"]	= "DELETE";
				$temp["text"]  	= "Operacion Satisfactoria";
				$temp["type"]  	= "alert-info";
			}
		}
    	return  $temp;
    }
	public function getSections() {
    	$user = JFactory::getUser();
		$sql = "Select s.sec_id\n";
		$sql.= "      ,s.sec_id as id\n";
		$sql.= "      ,s.sec_code\n";
		$sql.= "      ,s.sec_sta\n";
		$sql.= "      ,s.sec_level\n";
		$sql.= "      ,DATE_FORMAT(s.user_fec,'%d/%m/%Y %H:%i:%s') as user_fec\n";
		$sql.= "      ,s.user_id\n";
		$sql.= "      ,if(s.sec_sta = 1,'Activo','Inactivo') as sec_status\n";
		$sql.= "      ,l.level_name as sec_levels\n";
		$sql.= "From mod_section s\n";
		$sql.= "Inner Join mod_level l on l.level_id = s.sec_level\n";
		$sql.= "Where s.sec_id is not null\n";
		if(!is_null($this->sec_id) && !empty($this->sec_id)) {
			$sql.= "and s.sec_id = " . $this->sec_id . "\n";
		}
		if(!is_null($this->sec_code) && !empty($this->sec_code)) {
			$sql.= "and s.sec_code like '%" . $this->sec_code . "%'\n";
		}
        if(!is_null($this->sec_level)) {
            if(is_array($this->sec_level)) {
                $sql.= "and s.sec_level in (" . implode($this->sec_level,",") . ")\n";
            } else if(!empty($this->sec_level)){
                $sql.= "and s.sec_level = '" . $this->sec_level . "'\n";
            }
        }
		if(!is_null($this->sec_sta)) {
			if(is_array($this->sec_sta)) {
				$sql.= "and s.sec_sta in (" . implode($this->sec_sta,",") . ")\n";
			} else if(!empty($this->sec_sta)){
				$sql.= "and s.sec_sta = '" . $this->sec_sta . "'\n";
			}
		}
		// echo $sql;
		$db  = JFactory::getDBO();
		$db->setQuery($sql);
		$rows = $db->loadObjectList();
		$arrayDatos = array();
		if($this->view == "modify") {
			$arrayDatos = $this->getClassAtribute();
			if(count($rows) == 1 && $this->accion != "add") {
				for($i=0; $i < count($rows); $i++) {
					$iT = 0;
					foreach($rows[$i] as $key => $value) {
						$arrayDatos[$iT] 	= $value;
						$arrayDatos[$key] 	= $value;
						$iT++;
					}
				}
			}
		} else if($this->view == "list"){
			if(count($rows) > 0) {
				for($i=0; $i < count($rows); $i++) {
					$iT = 0;
					$arrayItemDatos = Array();
					foreach($rows[$i] as $key => $value) {
						$arrayItemDatos[$iT] 	= $value;
						$arrayItemDatos[$key] 	= $value;
						$iT++;
					}
					array_push($arrayDatos,$arrayItemDatos);
				}
			}
		}
		return $arrayDatos;
	}
}