<?php
$target	= JFunc::FTarget();
$mod	= JFactory::getModulo();    
$db		= JFactory::getDBO();
$user	= JFactory::getUser();
$datos	= json_decode(JRequest::getVar("datos"),true);
$title	= JRequest::getVar("title");
$sql = "Select s.sec_id\n";
$sql.= "      ,s.sec_code\n";
$sql.= "      ,s.sec_sta\n";
$sql.= "      ,s.sec_level\n";
$sql.= "      ,DATE_FORMAT(s.user_fec,'%d/%m/%Y %H:%i:%s') as user_fec\n";
$sql.= "      ,s.user_id\n";
$sql.= "      ,if(s.sec_sta = 1,'Activo','Inactivo') as sec_status\n";
$sql.= "      ,l.level_name as sec_levels\n";
$sql.= "From mod_section s\n";
$sql.= "Inner Join mod_level l on l.level_id = s.sec_level\n";
$sql.= "Where s.sec_id is not null\n";
if(!is_null($datos["ids"]) && !empty($datos["ids"])) {
	$sql.= "and s.sec_id in (" . $datos["ids"] . ")\n";
} else {
	$sql.= "and s.sec_id is null\n";
}
$db  = JFactory::getDBO();
$db->setQuery($sql);
$rows = $db->loadObjectList();
$datos = Array();
if(count($rows) > 0) {
	for($i=0; $i < count($rows); $i++) {
		$arrayItemDatos = Array();
		foreach($rows[$i] as $key => $value) {
			$arrayItemDatos[$key] 	= $value;
		}
		array_push($datos,$arrayItemDatos);
	}
}
$pdf 	= new PDFList();
$pdf->setTitle($title);
$pdf->setInicio();
$pdf->SetXY(10, 45);
$pdf->SetTextColor(0, 0, 10);
$pdf->SetFont('helvetica', 'B', 8);
$pdf->Cell(30, 5, html_entity_decode("Codigo"), 1, 0, 'C');
$pdf->Cell(20, 5, html_entity_decode("Estatus"), 1, 0, 'C');
$pdf->Cell(20, 5, html_entity_decode("Nivel"), 1, 0, 'C');
$pdf->SetXY(10,50);
for($i = 0; $i < count($datos); $i++) {
	$pdf->SetX(10);
	$pdf->SetFont('helvetica', '', 8);
	$CebColor = array();
	$CebColor[0] = (($i%2==0)?"255":"225");
	$CebColor[1] = (($i%2==0)?"255":"225");
	$CebColor[2] = (($i%2==0)?"255":"225");
	$pdf->SetFillColorArray($CebColor);
	$pdf->Cell(30, 5, html_entity_decode($datos[$i]["sec_code"]), 1, 0, 'C', true, '', 1);
	$StaColor = array();
	if($datos[$i]["sec_sta"] == "1") {
		$StaColor[0] = (($i%2==0)?"162":"197");
		$StaColor[1] = (($i%2==0)?"232":"232");
		$StaColor[2] = (($i%2==0)?"162":"197");
	} else {
		$StaColor[0] = (($i%2==0)?"240":"240");
		$StaColor[1] = (($i%2==0)?"168":"204");
		$StaColor[2] = (($i%2==0)?"168":"204");
	}
	$pdf->SetFillColorArray($StaColor);
	$pdf->Cell(20, 5, html_entity_decode($datos[$i]["sec_status"]), 1, 0, 'C', true, '', 1);
    $pdf->Cell(20, 5, html_entity_decode($datos[$i]["sec_levels"]), 1, 0, 'C', true, '', 1);
	$pdf->Ln();
}
$pdf->Output('Listado-'. date("d-m-Y-H-i-s"), 'I');