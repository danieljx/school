<?php
	$target = JFunc::FTarget();
	$mod	= JFactory::getModulo();
	$db		= JFactory::getDBO();
	$user	= JFactory::getUser();
	$back	= JRequest::getVar("back");
	if(!empty($back)) {
		$back	= json_decode($back,true);
	} else {
		$back	= json_decode("[]",true);
	}
	$back["view"]	= "list";
	$sec = new JSections($back);
?>
<script>
	objDataBack   = <?php echo json_encode($back); ?>;
	arrayDataList = <?php echo json_encode($sec->getSections()); ?>;
</script>
<h2 class="page-header">Secciones</h2>
<div class="panel panel-info">
	<div class="panel-heading">
		<h3 id="panel-title" class="panel-title">Filtro de Secciones</h3>
	</div>
	<div class="panel-body">
		<form id="sectionsFilter" data-module="section" data-vista="sectionsData" class="form-horizontal formList" method="POST" target="<?PHP echo $target; ?>">
			<div class="form-group">
				<div class="col-xs-4">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-barcode"></span></span>
						<input type="text" class="form-control" placeholder="Codigo de la Sección" name="sec_code">
					</div>
				</div>
				<div class="col-xs-4">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-signal"></span></span>
						<select class="form-control selectpicker" title='Selecione Nivel' name="sec_level" id="sec_level" multiple>
							<?php
							$sql = "Select level_id\n";
							$sql.= "	  ,level_name\n";
							$sql.= "From mod_level\n";
							$sql.= "Where level_type = 2\n";
							$db->setQuery( $sql );
							$rows = $db->loadObjectList();
							for($i=0; $i < count($rows); $i++) {
								echo "<option value=\"" . $rows[$i]->level_id . "\" >" .  $rows[$i]->level_name . "</option>\n";
							}
							?>
						</select>
					</div>
				</div>
				<div class="col-xs-4">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-stats"></span></span>
						<select class="form-control selectpicker" title='Selecione Estatus' name="sec_sta" id="sec_sta" multiple>
							<option value="1">Activo</option>
							<option value="2">Inactivo</option>
						</select>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="container-fluid">
					<div class="btn-group">
						<button type="submit" class="btn btn-primary">
							<i class="glyphicon glyphicon-filter"></i>
							Filtrar
						</button>
						<button type="reset" class="btn btn-warning">
							<i class="glyphicon glyphicon-erase"></i>
							Limpiar
						</button>
						<?php if($mod->getInsert()) { ?>
						<button id="newSection" type="button" class="btn btn-info">
							<i class="glyphicon glyphicon-edit"></i>
							Nuevo
						</button>
						<?php } ?>
						<?php if($mod->getPrint()) { ?>
						<button id="pdfListSections" type="button" class="btn btn-danger pdf" data-module="section" data-view="sectionsListPdf" data-title="Listado Secciónes">
							<i class="glyphicon glyphicon-print"></i>
							Pdf
						</button>
						<?php } ?>
						<?php if($mod->getExport()) { ?>
						<button id="xlsListSections" type="button" class="btn btn-success excel" data-module="section" data-view="sectionsListXls" data-title="Listado Secciónes">
							<i class="glyphicon glyphicon-share"></i>
							Excel
						</button>
						<?php } ?>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<div class="container-fluid">
	<div class="table-responsive">
		<table id="sectionsTabla" class="table dataTableList table-bordered" data-module="section" data-del-view="sectionsUp" data-update="<?php echo $mod->getUpdate(); ?>" data-delete="<?php echo $mod->getDelete(); ?>">
			<thead>
				<tr>
					<th data-visible="false" data-type="num" data-class="sec_id" data-bSearchable="false" data-bSortable="false">#</th>
					<th data-type="string" data-class="sec_code" data-bSearchable="true" data-bSortable="true">Codigo</th>
					<th data-type="string" data-class="sec_levels" data-bSearchable="true" data-bSortable="true">Nivel</th>
					<th data-type="string" data-class="sec_status" data-bSearchable="true" data-bSortable="true">Estatus</th>
					<th data-type="string" data-class="accion" data-bSearchable="false" data-bSortable="false">Accion</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
</div>