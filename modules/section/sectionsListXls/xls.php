<?php
$target	= JFunc::FTarget();
$mod	= JFactory::getModulo();    
$db		= JFactory::getDBO();
$user	= JFactory::getUser();
$datos	= json_decode(JRequest::getVar("datos"),true);
$sql = "Select s.sec_id\n";
$sql.= "      ,s.sec_code\n";
$sql.= "      ,s.sec_sta\n";
$sql.= "      ,s.sec_level\n";
$sql.= "      ,DATE_FORMAT(s.user_fec,'%d/%m/%Y %H:%i:%s') as user_fec\n";
$sql.= "      ,s.user_id\n";
$sql.= "      ,if(s.sec_sta = 1,'Activo','Inactivo') as sec_status\n";
$sql.= "      ,l.level_name as sec_levels\n";
$sql.= "From mod_section s\n";
$sql.= "Inner Join mod_level l on l.level_id = s.sec_level\n";
$sql.= "Where s.sec_id is not null\n";
if(!is_null($datos["ids"]) && !empty($datos["ids"])) {
	$sql.= "and s.sec_id in (" . $datos["ids"] . ")\n";
} else {
	$sql.= "and s.sec_id is null\n";
}
$db  = JFactory::getDBO();
$db->setQuery($sql);
$rows = $db->loadObjectList();
$datos = Array();
if(count($rows) > 0) {
	for($i=0; $i < count($rows); $i++) {
		$arrayItemDatos = Array();
		foreach($rows[$i] as $key => $value) {
			$arrayItemDatos[$key] 	= $value;
		}
		array_push($datos,$arrayItemDatos);
	}
}
echo "<table width=\"641\" border=\"1\">\n";
echo "<tr>\n";
echo "<th width=\"50%\" style=\"background-color:#006; text-align:center; color:#FFF\"><strong>Coodigo</strong></th>\n";
echo "<th width=\"50%\" style=\"background-color:#006; text-align:center; color:#FFF\"><strong>Estatus</strong></th>\n";
echo "<th width=\"50%\" style=\"background-color:#006; text-align:center; color:#FFF\"><strong>Nivel</strong></th>\n";
echo "</tr>\n";
for($i = 0; $i < count($datos); $i++) {
	echo "<tr>\n";
    echo "<td bgcolor=\"#ededed\" align=\"center\">" . $datos[$i]["sec_code"]  ."</td>\n";
    echo "<td bgcolor=\"#ededed\" align=\"center\">" . $datos[$i]["sec_status"]  ."</td>\n";
    echo "<td bgcolor=\"#ededed\" align=\"center\">" . $datos[$i]["sec_levels"]  ."</td>\n";
	echo "</tr>\n";
}
echo "</table>\n";