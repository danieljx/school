<?php
   $mod  	= JFactory::getModulo();
   $db  	= JFactory::getDBO();
   $user 	= JFactory::getUser(); 								
?>
<h1 class="page-header" xmlns="http://www.w3.org/1999/html">Tablero</h1>
<div class="row placeholders">
    <div class="col-xs-6 col-sm-3 placeholder">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="chart" data-percent="73"><span>73%</span></div>
            </div>
            <div class="panel-footer">Periodo Completado</div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-3 placeholder">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="chart" data-percent="100">100%</div>
            </div>
            <div class="panel-footer">Primer Lapso</div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-3 placeholder">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="chart" data-percent="100">100%</div>
            </div>
            <div class="panel-footer">Segundo Lapso</div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-3 placeholder">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="chart" data-percent="52">52%</div>
            </div>
            <div class="panel-footer">Tercer Lapso</div>
        </div>
    </div>
</div>
<div class="row placeholders">
    <div class="col-xs-6 col-sm-6 placeholder">
        <div class="panel panel-default">
            <div class="panel-body">
                <canvas id="canvasStudent" class="chartjs" width="500" height="300"></canvas>
            </div>
            <div class="panel-footer">Cantidad Alumnos Aprobados por Periodo</div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-6 placeholder">
        <div class="panel panel-default">
            <div class="panel-body">
                <canvas id="canvasEmployee" class="chartjs" width="500" height="300"></canvas>
            </div>
            <div class="panel-footer">Aprobados por Notas</div>
        </div>
    </div>
</div>