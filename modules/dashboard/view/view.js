$(function () {
    $('.chart').easyPieChart({
        animate: 2000,
        scaleColor: false,
        lineWidth: 12,
        lineCap: 'square',
        size: 100,
        trackColor: '#e5e5e5',
        barColor: '#3da0ea'
    });
    var canvas = $('canvas#canvasStudent');
    var ctx = canvas[0].getContext("2d");
    var chart = new Chart(ctx).Line({
        responsive: true,
        labels: "Titulo",
        datasets: [{
            fillColor: "rgba(151,187,205,0.2)",
            strokeColor: "rgba(151,187,205,1)",
            pointColor: "rgba(151,187,205,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(151,187,205,1)",
            data: "70,13,20,90,44,12,30,30,30,10,5,0".split(",")
        },{
            fillColor: "rgba(220,220,220,0.2)",
            strokeColor: "#F7464A",
            pointColor: "#FF5A5E",
            pointStrokeColor: "#FF5A5E",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "red",
            data: "70,13,20,90,44,12,30,30,30,10,5,0".split(",")
        }]
    },{});
    var canvas = $('canvas#canvasEmployee');
    var ctx = canvas[0].getContext("2d");
    var myDoughnutChart = new Chart(ctx).Doughnut([
            {
                value: 300,
                color:"#46BFBD",
                highlight: "#5AD3D1",
                label: "Aprobados con A"
            },
            {
                value: 50,
                color: "#F7464A",
                highlight: "#FF5A5E",
                label: "Aprobados con D"
            },
            {
                value: 100,
                color: "#FDB45C",
                highlight: "#FFC870",
                label: "Aprobados con C"
            },
            {
                value: 200,
                color: "#3da0ea",
                highlight: "#3da0ea",
                label: "Aprobados con B"
            }
        ], {
        animateScale: true
    });
});