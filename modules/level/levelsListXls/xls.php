<?php
$target	= JFunc::FTarget();
$mod	= JFactory::getModulo();    
$db		= JFactory::getDBO();
$user	= JFactory::getUser();
$datos	= json_decode(JRequest::getVar("datos"),true);
$sql = "Select l.level_id\n";
$sql.= "      ,l.level_name\n";
$sql.= "      ,l.level_desc\n";
$sql.= "      ,l.level_code\n";
$sql.= "      ,l.level_father\n";
$sql.= "      ,l.level_type\n";
$sql.= "      ,l.level_sta\n";
$sql.= "      ,DATE_FORMAT(l.user_fec,'%d/%m/%Y %H:%i:%s') as user_fec\n";
$sql.= "      ,l.user_id\n";
$sql.= "      ,if(l.level_sta = 1,'Activo','Inactivo') as level_status\n";
$sql.= "      ,if(l.level_type = 1,'Grupo','Grado') as level_typeName\n";
$sql.= "From mod_level l\n";
$sql.= "Where l.level_id is not null\n";
if(!is_null($datos["ids"]) && !empty($datos["ids"])) {
	$sql.= "and l.level_id in (" . $datos["ids"] . ")\n";
} else {
	$sql.= "and l.level_id is null\n";
}
$db  = JFactory::getDBO();
$db->setQuery($sql);
$rows = $db->loadObjectList();
$datos = Array();
if(count($rows) > 0) {
	for($i=0; $i < count($rows); $i++) {
		$arrayItemDatos = Array();
		foreach($rows[$i] as $key => $value) {
			$arrayItemDatos[$key] 	= $value;
		}
		array_push($datos,$arrayItemDatos);
	}
}
echo "<table width=\"641\" border=\"1\">\n";
echo "<tr>\n";
echo "<th width=\"50%\" style=\"background-color:#006; text-align:center; color:#FFF\"><strong>Nombre</strong></th>\n";
echo "<th width=\"50%\" style=\"background-color:#006; text-align:center; color:#FFF\"><strong>Coodigo</strong></th>\n";
echo "<th width=\"50%\" style=\"background-color:#006; text-align:center; color:#FFF\"><strong>Estatus</strong></th>\n";
echo "</tr>\n";
for($i = 0; $i < count($datos); $i++) {
	echo "<tr>\n";
	echo "<td bgcolor=\"#ededed\" align=\"center\">" . $datos[$i]["level_name"]  ."</td>\n";
	echo "<td bgcolor=\"#ededed\" align=\"center\">" . $datos[$i]["level_code"]  ."</td>\n";
	echo "<td bgcolor=\"#ededed\" align=\"center\">" . $datos[$i]["level_status"]  ."</td>\n";
	echo "</tr>\n";
}
echo "</table>\n";