<?php
class JLevels {
    private $level_id			= null;
    private $level_name			= null;
    private $level_desc			= null;
    private $level_code			= null;
    private $level_type			= null;
    private $level_father		= null;
    private $level_sta			= null;
    private $user_id			= null;
    private $user_fec			= null;
    private $accion				= null;
    private $view    			= "list";

    function __construct($datos = array()) {
		$this->requestLevels($datos);
	}
    public function getDisable($tipo = "campo") {
    	$mod  = JFactory::getModulo();
    	if($this->accion == "add" && !$mod->getInsert()) {
			return "disabled";
		} else if($this->accion == "upd" && !$mod->getUpdate()) {
			return "disabled";
		} else if($this->accion == "del" && $tipo=="campo") {
			return "disabled";
		} else if($this->accion == "del" && !$mod->getDelete()) {
			return "disabled";
		}
    	return "";
	}
	public function getClassAtribute() {
        return get_object_vars($this);
    }
    public function requestLevels($datos = array()) {
		if(count($datos) > 0) {
			foreach($datos as $key => $value) {
                if(array_key_exists($key,$this->getClassAtribute())) {
                    $this->$key =  $value;
                }
			}
		}
    }
    public function setLevels() {
    	$user = JFactory::getUser();
    	$mod  = JFactory::getModulo();
    	$db   = JFactory::getDBO();
		$temp = array("status" => "ERROR", "type" => "alert-danger", "text" => "NO TIENE ACCESO", "id" => "");
    	if($this->accion != "del") {
			$obj 	= new stdClass();
			$datos	= $this->getClassAtribute();
			if(count($datos) > 0) {
				foreach($datos as $key => $value) {
                    if($key != "accion" && $key != "view" && !is_null($value) && !empty($value)) {
						$obj->$key =  $value;
					}
				}
			}
			$obj->user_id	= $user->getId();
			$obj->user_fec  = 'NOW_AHORA';
    	  	if($this->accion == "add" && $mod->getInsert()) {
				if(!$db->insertObject('mod_level', $obj,'level_id')) {
                    $temp["id"]  	= "";
                    $temp["text"]  	= $db->getErrorMsg();
				} else {
					$this->level_id = $obj->level_id;
					$temp["id"]		= $obj->level_id;
					$temp["status"]	= "OK";
					$temp["text"]  	= "Operacion Satisfactoria";
					$temp["type"]  	= "alert-info";
				}   	  	  	
			} else if($mod->getUpdate()) {
				$obj->level_id   = (int)$this->level_id;
				if(!$db->updateObject('mod_level', $obj,"level_id")) {
					$temp["id"]  	= "";
					$temp["text"]  	= $db->getErrorMsg();
				} else {
					$temp["id"]  	= $obj->level_id;
					$temp["status"]	= "OK";
					$temp["text"]  	= "Operacion Satisfactoria";
					$temp["type"]  	= "alert-info";
				}
			}
		} else {
    	  	$sql = "DELETE FROM mod_level WHERE level_id = " . $this->level_id;
    	  	if(!$db->Execute($sql)) {					
				$temp["id"]  	= "";
				$temp["text"]  	= $db->getErrorMsg();
			} else {
				$temp["status"]	= "DELETE";
				$temp["text"]  	= "Operacion Satisfactoria";
				$temp["type"]  	= "alert-info";
			}
		}
    	return  $temp;
    }
	public function getLevels() {
    	$user = JFactory::getUser();
		$sql = "Select l.level_id\n";
		$sql.= "      ,l.level_id as id\n";
		$sql.= "      ,l.level_name\n";
		$sql.= "      ,l.level_desc\n";
		$sql.= "      ,l.level_code\n";
		$sql.= "      ,l.level_father\n";
		$sql.= "      ,l.level_type\n";
		$sql.= "      ,l.level_sta\n";
		$sql.= "      ,DATE_FORMAT(l.user_fec,'%d/%m/%Y %H:%i:%s') as user_fec\n";
		$sql.= "      ,l.user_id\n";
		$sql.= "      ,if(l.level_sta = 1,'Activo','Inactivo') as level_status\n";
		$sql.= "      ,if(l.level_type = 1,'Grupo','Grado') as level_typeName\n";
		$sql.= "From mod_level l\n";
		$sql.= "Where l.level_id is not null\n";
		if(!is_null($this->level_id) && !empty($this->level_id)) {
			$sql.= "and l.level_id = " . $this->level_id . "\n";
		}
		if(!is_null($this->level_name) && !empty($this->level_name)) {
			$sql.= "and l.level_name like '%" . $this->level_name . "%'\n";
		}
		if(!is_null($this->level_code) && !empty($this->level_code)) {
			$sql.= "and l.level_code like '%" . $this->level_code . "%'\n";
		}
		if(!is_null($this->level_type)) {
			if(is_array($this->level_type)) {
				$in = true;
				foreach($this->level_type as $key => $itemData) {
					if($key == "not in") {
						$sql.= "and l.level_type " . $key . " (" . implode($itemData,",") . ")\n";
						$in = false;
					}
				}
				if($in) {
					$sql.= "and l.level_type in (" . implode($this->level_type,",") . ")\n";
				}
			} else if(!empty($this->level_type)){
				$sql.= "and l.level_type = '" . $this->level_type . "'\n";
			}
		}
		if(!is_null($this->level_sta)) {
			if(is_array($this->level_sta)) {
				$sql.= "and l.level_sta in (" . implode($this->level_sta,",") . ")\n";
			} else if(!empty($this->level_sta)){
				$sql.= "and l.level_sta = '" . $this->level_sta . "'\n";
			}
		}
		$db  = JFactory::getDBO();
		$db->setQuery($sql);
		$rows = $db->loadObjectList();
		$arrayDatos = array();
		if($this->view == "modify") {
			$arrayDatos = $this->getClassAtribute();
			if(count($rows) == 1 && $this->accion != "add") {
				for($i=0; $i < count($rows); $i++) {
					$iT = 0;
					foreach($rows[$i] as $key => $value) {
						$arrayDatos[$iT] 	= $value;
						$arrayDatos[$key] 	= $value;
						$iT++;
					}
				}
			}
		} else if($this->view == "list"){
			if(count($rows) > 0) {
				for($i=0; $i < count($rows); $i++) {
					$iT = 0;
					$arrayItemDatos = Array();
					foreach($rows[$i] as $key => $value) {
						$arrayItemDatos[$iT] 	= $value;
						$arrayItemDatos[$key] 	= $value;
						$iT++;
					}
					array_push($arrayDatos,$arrayItemDatos);
				}
			}
		}
		return $arrayDatos;
	}
}