<?php
	$target = JFunc::FTarget();
	$mod	= JFactory::getModulo();
	$db		= JFactory::getDBO();
	$user	= JFactory::getUser();
	$back	= JRequest::getVar("back");
	if(!empty($back)) {
		$back	= json_decode($back,true);
	} else {
		$back	= json_decode("[]",true);
	}
	$back["level_type"] = "2";
	$back["view"]		= "list";
	$level 	= new JLevels($back);
?>
<script>
	objDataBack   = <?php echo json_encode($back); ?>;
	arrayDataList = <?php echo json_encode($level->getLevels()); ?>;
</script>
<h2 class="page-header">Niveles</h2>
<div class="panel panel-info">
	<div class="panel-heading">
		<h3 id="panel-title" class="panel-title">Filtro de Niveles</h3>
	</div>
	<div class="panel-body">
		<form id="levelsFilter" data-module="level" data-vista="levelsData" class="form-horizontal formList" method="POST" target="<?PHP echo $target; ?>">
			<div class="form-group">
				<div class="col-xs-4">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-signal"></span></span>
						<input type="text" class="form-control" placeholder="Nombre del Nivel" name="level_name">
					</div>
				</div>
				<div class="col-xs-4">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-barcode"></span></span>
						<input type="text" class="form-control" placeholder="Codigo Nivel" name="level_code">
					</div>
				</div>
				<div class="col-xs-4">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-stats"></span></span>
						<select class="form-control selectpicker" title='Selecione Estatus' name="level_sta" id="level_sta" multiple>
							<option value="1">Activo</option>
							<option value="2">Inactivo</option>
						</select>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="container-fluid">
					<div class="btn-group">
						<button type="submit" class="btn btn-primary">
							<i class="glyphicon glyphicon-filter"></i>
							Filtrar
						</button>
						<button type="reset" class="btn btn-warning">
							<i class="glyphicon glyphicon-erase"></i>
							Limpiar
						</button>
						<?php if($mod->getInsert()) { ?>
						<button id="newLevel" type="button" class="btn btn-info">
							<i class="glyphicon glyphicon-edit"></i>
							Nuevo
						</button>
						<?php } ?>
						<?php if($mod->getPrint()) { ?>
						<button id="pdfListLevels" type="button" class="btn btn-danger pdf" data-module="level" data-view="levelsListPdf" data-title="Listado Niveles">
							<i class="glyphicon glyphicon-print"></i>
							Pdf
						</button>
						<?php } ?>
						<?php if($mod->getExport()) { ?>
						<button id="xlsListLevels" data-module="level" type="button" class="btn btn-success excel" data-view="levelsListXls" data-title="Listado Niveles">
							<i class="glyphicon glyphicon-share"></i>
							Excel
						</button>
						<?php } ?>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<div class="container-fluid">
	<div class="table-responsive">
		<table id="levelsTabla" class="table dataTableList table-bordered" data-module="level" data-del-view="levelsUp" data-update="<?php echo $mod->getUpdate(); ?>" data-delete="<?php echo $mod->getDelete(); ?>">
			<thead>
				<tr>
					<th data-visible="false" data-type="num" data-class="level_id" data-bSearchable="false" data-bSortable="false">#</th>
					<th data-type="string" data-class="level_name" data-bSearchable="true" data-bSortable="true">Nombre</th>
					<th data-type="string" data-class="level_code" data-bSearchable="true" data-bSortable="true">Codigo</th>
					<th data-type="string" data-class="level_status" data-bSearchable="true" data-bSortable="true">Estatus</th>
					<th data-type="string" data-class="accion" data-bSearchable="false" data-bSortable="false">Accion</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
</div>