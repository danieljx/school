<?php
	$target = JFunc::FTarget();
	$mod	= JFactory::getModulo();
	$db		= JFactory::getDBO();
	$user	= JFactory::getUser();
	$datos	= json_decode(JRequest::getVar("datos"),true);
	$datos["view"]	= "modify";
	$back	= json_decode(JRequest::getVar("back"),true);
	$levels  = new JLevels($datos);
	$levelData = $levels->getLevels();
    $levelData["accion"]	= (isset($datos["accion"])?$datos["accion"]:"add");
?>
<script>
	objDataBack   	= <?php echo json_encode($back); ?>;
	objDataModify 	= <?php echo json_encode($levelData); ?>;
</script>
<div class="col-xs-2">
	<ul class="nav nav-tabs tabs-left">
		<li class="active">
			<a href="#levelsData" data-toggle="tab">Datos</a>
		</li>
	</ul>
</div>
<div class="col-xs-10">
	<form id="levelModify" data-id="level_id" data-module="level" data-vista="levelsUp" class="form-horizontal formModify" method="POST" target="<?PHP echo $target; ?>">
        <div class="alertDiv oculto"></div>
        <div class="tab-content">
			<div class="tab-pane active" id="levelsData">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 id="panel-title" class="panel-title">Datos</h3>
					</div>
					<div class="panel-body">
						<input type="hidden" id="level_id" name="level_id">
						<input type="hidden" id="level_father" name="level_father" value="1">
						<input type="hidden" id="level_type" name="level_type" value="2">
                        <input type="hidden" id="accion" name="accion">
						<div class="form-group">
							<div class="container-fluid">
								<div class="col-xs-5">
									<div class="form-group">
										<div class="input-group">
											<span for="level_name" class="input-group-addon">Nombre</span>
											<input type="text" class="form-control" placeholder="Nombres" id="level_name" name="level_name" aria-required="true" data-level-required="true" data-msg-required="Ingrese un Nombre al Nivel" data-rule-maxlength="150" data-msg-minlength="No ingrese mas de 150 Caracteres">
										</div>
									</div>
									<div class="form-group">
										<div class="input-group">
											<span for="level_code" class="input-group-addon">Codigo</span>
											<input type="text" class="form-control" placeholder="Codigo" id="level_code" name="level_code" aria-required="true" data-level-required="true" data-msg-required="Ingrese un Codigo al Nivel" data-rule-maxlength="150" data-msg-minlength="No ingrese mas de 20 Caracteres">
										</div>
									</div>
								</div>
								<div class="col-xs-5 col-md-offset-1">							
								<?php if($mod->getStatus()) { ?>
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon">Estatus</span>
											<input type="radio" class="form-control" id="level_sta" name="level_sta" value="1" data-on-color="success" data-label-text="Activo">
											<input type="radio" class="form-control" id="level_sta" name="level_sta" value="2" data-on-color="danger" data-label-text="Inactivo">
										</div>
									</div>
								<?php } ?>
									<div class="form-group">
										<div class="input-group">
											<span for="level_desc" class="input-group-addon">Descripción</span>
											<textarea class="form-control" placeholder="Descripción" id="level_desc" name="level_desc" data-rule-maxlength="255" data-msg-minlength="No ingrese mas de 200 Caracteres"></textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-default">
		  <div class="panel-body">
			<div class="container-fluid">
				<div class="btn-group">
					<button type="submit" class="btn btn-success">
						<i class="glyphicon glyphicon-ok"></i>
						Guardar
					</button>
					<button type="button" id="levelList" class="btn btn-info">
						<i class="glyphicon glyphicon-th-list"></i>
						Lista
					</button>
				</div>
			</div>
		  </div>
		</div>
	</form>
</div>