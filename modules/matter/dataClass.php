<?php
class JMatters {
    private $matter_id			= null;
    private $matter_name		= null;
    private $matter_desc		= null;
    private $matter_code		= null;
    private $matter_level		= null;
    private $matter_sta			= null;
    private $user_id			= null;
    private $user_fec			= null;
    private $accion				= null;
    private $view    			= "list";

    function __construct($datos = array()) {
		$this->requestMatters($datos);
	}
    public function getDisable($tipo = "campo") {
    	$mod  = JFactory::getModulo();
    	if($this->accion == "add" && !$mod->getInsert()) {
			return "disabled";
		} else if($this->accion == "upd" && !$mod->getUpdate()) {
			return "disabled";
		} else if($this->accion == "del" && $tipo=="campo") {
			return "disabled";
		} else if($this->accion == "del" && !$mod->getDelete()) {
			return "disabled";
		}
    	return "";
	}
	public function getClassAtribute() {
        return get_object_vars($this);
    }
    public function requestMatters($datos = array()) {
		if(count($datos) > 0) {
			foreach($datos as $key => $value) {
                if(array_key_exists($key,$this->getClassAtribute())) {
                    $this->$key =  $value;
                }
			}
		}
    }
    public function setMatters() {
    	$user = JFactory::getUser();
    	$mod  = JFactory::getModulo();
    	$db   = JFactory::getDBO();
		$temp = array("status" => "ERROR", "type" => "alert-danger", "text" => "NO TIENE ACCESO", "id" => "");
    	if($this->accion != "del") {
			$obj 	= new stdClass();
			$datos	= $this->getClassAtribute();
			if(count($datos) > 0) {
				foreach($datos as $key => $value) {
                    if($key != "accion" && $key != "view") {
						$obj->$key =  $value;
					}
				}
			}
			$obj->user_id	= $user->getId();
			$obj->user_fec  = 'NOW_AHORA';
    	  	if($this->accion == "add" && $mod->getInsert()) {
				if(!$db->insertObject('mod_matter', $obj,'matter_id')) {
                    $temp["id"]  	= "";
                    $temp["status"]	= "ERROR";
                    $temp["text"]  	= $db->getErrorMsg();
				} else {
					$this->matter_id = $obj->matter_id;
					$temp["id"]		= $obj->matter_id;
					$temp["status"]	= "OK";
					$temp["text"]  	= "Operacion Satisfactoria";
					$temp["type"]  	= "alert-info";
				}   	  	  	
			} else if($mod->getUpdate()) {
				$obj->matter_id   = (int)$this->matter_id;
				if(!$db->updateObject('mod_matter', $obj,"matter_id")) {
					$temp["id"]  	= "";
					$temp["text"]  	= $db->getErrorMsg();
				} else {
					$temp["id"]  	= $obj->matter_id;
					$temp["status"]	= "OK";
					$temp["text"]  	= "Operacion Satisfactoria";
					$temp["type"]  	= "alert-info";
				}
			}
		} else {
    	  	$sql = "DELETE FROM mod_matter WHERE matter_id = " . $this->matter_id;
    	  	if(!$db->Execute($sql)) {					
				$temp["id"]  	= "";
				$temp["text"]  	= $db->getErrorMsg();
			} else {
				$temp["status"]	= "DELETE";
				$temp["text"]  	= "Operacion Satisfactoria";
				$temp["type"]  	= "alert-info";
			}
		}
    	return  $temp;
    }
	public function getMatters() {
    	$user = JFactory::getUser();
		$sqm = "Select m.matter_id\n";
		$sqm.= "      ,m.matter_id as id\n";
		$sqm.= "      ,m.matter_name\n";
		$sqm.= "      ,m.matter_desc\n";
		$sqm.= "      ,m.matter_code\n";
		$sqm.= "      ,m.matter_level\n";
		$sqm.= "      ,m.matter_sta\n";
		$sqm.= "      ,DATE_FORMAT(m.user_fec,'%d/%m/%Y %H:%i:%s') as user_fec\n";
		$sqm.= "      ,m.user_id\n";
		$sqm.= "      ,if(m.matter_sta = 1,'Activo','Inactivo') as matter_status\n";
		$sqm.= "      ,l.level_name\n";
		$sqm.= "From mod_matter m\n";
		$sqm.= "Inner Join mod_level l on l.level_id = m.matter_level\n";
		$sqm.= "Where m.matter_id is not null\n";
		if(!is_null($this->matter_id) && !empty($this->matter_id)) {
			$sqm.= "and m.matter_id = " . $this->matter_id . "\n";
		}
		if(!is_null($this->matter_name) && !empty($this->matter_name)) {
			$sqm.= "and m.matter_name like '%" . $this->matter_name . "%'\n";
		}
		if(!is_null($this->matter_code) && !empty($this->matter_code)) {
			$sqm.= "and m.matter_code like '%" . $this->matter_code . "%'\n";
		}
		if(!is_null($this->matter_level)) {
			if(is_array($this->matter_level)) {
				$in = true;
				foreach($this->matter_level as $key => $itemData) {
					if($key == "not in") {
						$sqm.= "and m.matter_level " . $key . " (" . implode($itemData,",") . ")\n";
						$in = false;
					}
				}
				if($in) {
					$sqm.= "and m.matter_level in (" . implode($this->matter_level,",") . ")\n";
				}
			} else if(!empty($this->matter_level)){
				$sqm.= "and m.matter_level = '" . $this->matter_level . "'\n";
			}
		}
		if(!is_null($this->matter_sta)) {
			if(is_array($this->matter_sta)) {
				$sqm.= "and m.matter_sta in (" . implode($this->matter_sta,",") . ")\n";
			} else if(!empty($this->matter_sta)){
				$sqm.= "and m.matter_sta = '" . $this->matter_sta . "'\n";
			}
		}
		// echo $sql;
		$db  = JFactory::getDBO();
		$db->setQuery($sqm);
		$rows = $db->loadObjectList();
		$arrayDatos = array();
		if($this->view == "modify") {
			$arrayDatos = $this->getClassAtribute();
			if(count($rows) == 1 && $this->accion != "add") {
				for($i=0; $i < count($rows); $i++) {
					$iT = 0;
					foreach($rows[$i] as $key => $value) {
						$arrayDatos[$iT] 	= $value;
						$arrayDatos[$key] 	= $value;
						$iT++;
					}
				}
			}
		} else if($this->view == "list"){
			if(count($rows) > 0) {
				for($i=0; $i < count($rows); $i++) {
					$iT = 0;
					$arrayItemDatos = Array();
					foreach($rows[$i] as $key => $value) {
						$arrayItemDatos[$iT] 	= $value;
						$arrayItemDatos[$key] 	= $value;
						$iT++;
					}
					array_push($arrayDatos,$arrayItemDatos);
				}
			}
		}
		return $arrayDatos;
	}
}