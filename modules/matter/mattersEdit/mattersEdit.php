<?php
	$target = JFunc::FTarget();
	$mod	= JFactory::getModulo();
	$db		= JFactory::getDBO();
	$user	= JFactory::getUser();
	$datos	= json_decode(JRequest::getVar("datos"),true);
	$datos["view"]	= "modify";
	$back	= json_decode(JRequest::getVar("back"),true);
	$matters  = new JMatters($datos);
	$matterData = $matters->getMatters();
    $matterData["accion"]	= (isset($datos["accion"])?$datos["accion"]:"add");
?>
<script>
	objDataBack   	= <?php echo json_encode($back); ?>;
	objDataModify 	= <?php echo json_encode($matterData); ?>;
</script>
<div class="col-xs-2">
	<ul class="nav nav-tabs tabs-left">
		<li class="active">
			<a href="#mattersData" data-toggle="tab">Datos</a>
		</li>
	</ul>
</div>
<div class="col-xs-10">
	<form id="matterModify" data-id="matter_id" data-module="matter" data-vista="mattersUp" class="form-horizontal formModify" method="POST" target="<?PHP echo $target; ?>">
        <div class="alertDiv oculto"></div>
        <div class="tab-content">
			<div class="tab-pane active" id="mattersData">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 id="panel-title" class="panel-title">Datos</h3>
					</div>
					<div class="panel-body">
						<input type="hidden" id="matter_id" name="matter_id">
						<input type="hidden" id="matter_type" name="matter_type" value="2">
                        <input type="hidden" id="accion" name="accion">
						<div class="form-group">
							<div class="container-fluid">
								<div class="col-xs-5">
									<div class="form-group">
										<div class="input-group">
											<span for="matter_name" class="input-group-addon">Nombre</span>
											<input type="text" class="form-control" placeholder="Nombres" id="matter_name" name="matter_name" aria-required="true" data-matter-required="true" data-msg-required="Ingrese un Nombre a la Materia" data-rule-maxlength="150" data-msg-minlength="No ingrese mas de 150 Caracteres">
										</div>
									</div>
									<div class="form-group">
										<div class="input-group">
											<span for="matter_code" class="input-group-addon">Codigo</span>
											<input type="text" class="form-control" placeholder="Codigo" id="matter_code" name="matter_code" aria-required="true" data-matter-required="true" data-msg-required="Ingrese un Codigo a la Materia" data-rule-maxlength="20" data-msg-minlength="No ingrese mas de 20 Caracteres">
										</div>
									</div>
									<div class="form-group">
										<div class="input-group">
											<span for="matter_level" class="input-group-addon">Nivel</span>
											<select class="form-control selectpicker" title='Selecione Nivel' id="matter_level" name="matter_level">
												<?php
												$sql = "Select level_id\n";
												$sql.= "	  ,level_name\n";
												$sql.= "From mod_level\n";
												$sql.= "Where level_type = 2\n";
												$db->setQuery( $sql );
												$rows = $db->loadObjectList();
												for($i=0; $i < count($rows); $i++) {
													echo "<option value=\"" . $rows[$i]->level_id . "\" >" .  $rows[$i]->level_name . "</option>\n";
												}
												?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-xs-5 col-md-offset-1">							
								<?php if($mod->getStatus()) { ?>
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon">Estatus</span>
											<input type="radio" class="form-control" id="matter_sta" name="matter_sta" value="1" data-on-color="success" data-label-text="Activo">
											<input type="radio" class="form-control" id="matter_sta" name="matter_sta" value="2" data-on-color="danger" data-label-text="Inactivo">
										</div>
									</div>
								<?php } ?>
									<div class="form-group">
										<div class="input-group">
											<span for="matter_desc" class="input-group-addon">Descripción</span>
											<textarea class="form-control" placeholder="Descripción" id="matter_desc" name="matter_desc" data-rule-maxlength="255" data-msg-minlength="No ingrese mas de 200 Caracteres"></textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-default">
		  <div class="panel-body">
			<div class="container-fluid">
				<div class="btn-group">
					<button type="submit" class="btn btn-success">
						<i class="glyphicon glyphicon-ok"></i>
						Guardar
					</button>
					<button type="button" id="matterList" class="btn btn-info">
						<i class="glyphicon glyphicon-th-list"></i>
						Lista
					</button>
				</div>
			</div>
		  </div>
		</div>
	</form>
</div>