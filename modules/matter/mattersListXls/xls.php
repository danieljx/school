<?php
$target	= JFunc::FTarget();
$mod	= JFactory::getModulo();    
$db		= JFactory::getDBO();
$user	= JFactory::getUser();
$datos	= json_decode(JRequest::getVar("datos"),true);
$sqm = "Select m.matter_id\n";
$sqm.= "      ,m.matter_name\n";
$sqm.= "      ,m.matter_desc\n";
$sqm.= "      ,m.matter_code\n";
$sqm.= "      ,m.matter_level\n";
$sqm.= "      ,m.matter_sta\n";
$sqm.= "      ,DATE_FORMAT(m.user_fec,'%d/%m/%Y %H:%i:%s') as user_fec\n";
$sqm.= "      ,m.user_id\n";
$sqm.= "      ,if(m.matter_sta = 1,'Activo','Inactivo') as matter_status\n";
$sqm.= "      ,l.level_name\n";
$sqm.= "From mod_matter m\n";
$sqm.= "Inner Join mod_level l on l.level_id = m.matter_level\n";
$sqm.= "Where m.matter_id is not null\n";
if(!is_null($datos["ids"]) && !empty($datos["ids"])) {
	$sqm.= "and m.matter_id in (" . $datos["ids"] . ")\n";
} else {
	$sqm.= "and m.matter_id is null\n";
}
$db  = JFactory::getDBO();
$db->setQuery($sqm);
$rows = $db->loadObjectList();
$datos = Array();
if(count($rows) > 0) {
	for($i=0; $i < count($rows); $i++) {
		$arrayItemDatos = Array();
		foreach($rows[$i] as $key => $value) {
			$arrayItemDatos[$key] 	= $value;
		}
		array_push($datos,$arrayItemDatos);
	}
}
echo "<table width=\"641\" border=\"1\">\n";
echo "<tr>\n";
echo "<th width=\"50%\" style=\"background-color:#006; text-align:center; color:#FFF\"><strong>Nombre</strong></th>\n";
echo "<th width=\"50%\" style=\"background-color:#006; text-align:center; color:#FFF\"><strong>Coodigo</strong></th>\n";
echo "<th width=\"50%\" style=\"background-color:#006; text-align:center; color:#FFF\"><strong>Estatus</strong></th>\n";
echo "<th width=\"50%\" style=\"background-color:#006; text-align:center; color:#FFF\"><strong>Nivel</strong></th>\n";
echo "</tr>\n";
for($i = 0; $i < count($datos); $i++) {
	echo "<tr>\n";
	echo "<td bgcolor=\"#ededed\" align=\"center\">" . $datos[$i]["matter_name"]  ."</td>\n";
	echo "<td bgcolor=\"#ededed\" align=\"center\">" . $datos[$i]["matter_code"]  ."</td>\n";
	echo "<td bgcolor=\"#ededed\" align=\"center\">" . $datos[$i]["matter_status"]  ."</td>\n";
	echo "<td bgcolor=\"#ededed\" align=\"center\">" . $datos[$i]["level_name"]  ."</td>\n";
	echo "</tr>\n";
}
echo "</table>\n";