<?php
	$target = JFunc::FTarget();
	$mod	= JFactory::getModulo();
	$db		= JFactory::getDBO();
	$user	= JFactory::getUser();
	$back	= JRequest::getVar("back");
	if(!empty($back)) {
		$back	= json_decode($back,true);
	} else {
		$back	= json_decode("[]",true);
	}
	$back["view"]		= "list";
	$matter	= new JMatters($back);
?>
<script>
	objDataBack   = <?php echo json_encode($back); ?>;
	arrayDataList = <?php echo json_encode($matter->getMatters()); ?>;
</script>
<h2 class="page-header">Materias</h2>
<div class="panel panel-info">
	<div class="panel-heading">
		<h3 id="panel-title" class="panel-title">Filtro de Materias</h3>
	</div>
	<div class="panel-body">
		<form id="mattersFilter" data-module="matter" data-vista="mattersData" class="form-horizontal formList" method="POST" target="<?PHP echo $target; ?>">
			<div class="form-group">
				<div class="col-xs-4">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-book"></span></span>
						<input type="text" class="form-control" placeholder="Nombre Materia" name="matter_name">
					</div>
				</div>
				<div class="col-xs-4">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-barcode"></span></span>
						<input type="text" class="form-control" placeholder="Codigo Materia" name="matter_code">
					</div>
				</div>
				<div class="col-xs-4">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-stats"></span></span>
						<select class="form-control selectpicker" title='Selecione Estatus' name="matter_sta" id="matter_sta" multiple>
							<option value="1">Activo</option>
							<option value="2">Inactivo</option>
						</select>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-4">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-signal"></span></span>
						<select class="form-control selectpicker" title='Selecione Nivel' name="matter_level" id="matter_level" multiple>
							<?php
							$sql = "Select level_id\n";
							$sql.= "	  ,level_name\n";
							$sql.= "From mod_level\n";
							$sql.= "Where level_type = 2\n";
							$db->setQuery( $sql );
							$rows = $db->loadObjectList();
							for($i=0; $i < count($rows); $i++) {
								echo "<option value=\"" . $rows[$i]->level_id . "\" >" .  $rows[$i]->level_name . "</option>\n";
							}
							?>
						</select>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="container-fluid">
					<div class="btn-group">
						<button type="submit" class="btn btn-primary">
							<i class="glyphicon glyphicon-filter"></i>
							Filtrar
						</button>
						<button type="reset" class="btn btn-warning">
							<i class="glyphicon glyphicon-erase"></i>
							Limpiar
						</button>
						<?php if($mod->getInsert()) { ?>
						<button id="newMatter" type="button" class="btn btn-info">
							<i class="glyphicon glyphicon-edit"></i>
							Nuevo
						</button>
						<?php } ?>
						<?php if($mod->getPrint()) { ?>
						<button id="pdfListMatters" type="button" class="btn btn-danger pdf" data-module="matter" data-view="mattersListPdf" data-title="Listado Materias">
							<i class="glyphicon glyphicon-print"></i>
							Pdf
						</button>
						<?php } ?>
						<?php if($mod->getExport()) { ?>
						<button id="xlsListMatters" data-module="matter" type="button" class="btn btn-success excel" data-module="matter" data-view="mattersListXls" data-title="Listado Materias">
							<i class="glyphicon glyphicon-share"></i>
							Excel
						</button>
						<?php } ?>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<div class="container-fluid">
	<div class="table-responsive">
		<table id="mattersTabla" class="table dataTableList table-bordered" data-module="matter" data-del-view="mattersUp" data-update="<?php echo $mod->getUpdate(); ?>" data-delete="<?php echo $mod->getDelete(); ?>">
			<thead>
				<tr>
					<th data-visible="false" data-type="num" data-class="matter_id" data-bSearchable="false" data-bSortable="false">#</th>
					<th data-type="string" data-class="matter_name" data-bSearchable="true" data-bSortable="true">Nombre</th>
					<th data-type="string" data-class="matter_code" data-bSearchable="true" data-bSortable="true">Codigo</th>
					<th data-type="string" data-class="matter_status" data-bSearchable="true" data-bSortable="true">Estatus</th>
					<th data-type="string" data-class="level_name" data-bSearchable="true" data-bSortable="true">Nivel</th>
					<th data-type="string" data-class="accion" data-bSearchable="false" data-bSortable="false">Accion</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
</div>